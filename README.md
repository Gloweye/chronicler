# Chronicler

Chronicler keeps track off and allows the editing/viewing of a Gamer and his stats.

## Add to server:

[Add Chronicler to a server you can add bots on](
https://discordapp.com/oauth2/authorize?&client_id=728224088362975303&scope=bot&permissions=8)

## Specification

### Author

A Discord User who has editing rights. Server Owners can register these.

#### Properties
- Discord User Snowflake
- Discord User identifier
- Server Name
- Channels
- Permissions

#### Access Points
- Add
- List
- Remove
- Permissions (List of Gamers this Author can edit.)
- Channels (Author may only use the bot in these channels)
  - View
  - Add
  - Remove

### Character
A virtual person who has Classes, Skills, Attributes, Perks, and Abilities.

#### Properties / Members
- Classes (List of Added Classes, dynamically inferring their levels from Skills)
- Skills (List of Added skills and their levels)
- Attributes (Dynamically constructed: Strength, Health, Frost Resistance)
- Perks (Dynamically constructed: Misc. Boni stat sticks)
- Abilities (Dynamically constructed: Power Attack, Fireball)
- Name
- Title (May be NULL)
- Bonus Perks (Perks not originating from Skills/classes, such as Quest Rewards.)
- Inventory (NOTE: More details working out.)
- AttributeDamage (Contains damage to attributes, like lost Health or expended Mana.)
- RelationShips
- Divinity (determines which trump-type effect wins)

#### Access Points
Please note that these names will not be representative of the commands given.
- Get
  - by ID
  - List all Gamers (filters?)
- Skills
    - Set Level
    - Add
    - Remove
- Classes (Dynamically constructed based on Skills, as long as the class is added)
    - View List (filters?)
    - Add
    - Remove
- Perks (Dynamically constructed based on Skills, Classes, Bonus Perks)
    - View Specific
    - View List (filters?)
    - View Bonus Perk List
    - Add Bonus Perk
    - Remove Bonus Perk.
- Attributes (Dynamically constructed based on Skills, Classes, Attributes, Perks, Classifications)
    - View List (Values specific to this gamer) (filters?)
    - View Single (Values specific to this gamer)
    - Get Damage Value (this value should be subtracted from the MaxValue)
    - Set Damage Value
- Abilities (Dynamically constructed based on Skills, Classes, Perks, Attributes)
    - View Specific (Cost/effect shown as would be when used by this Gamer)
    - View List (Available to this Gamer) (filters?)
- Relationships
    - List
    - Set (set to 0 to remove)
- Create
- Delete
- Name
  - Set
- Title
  - Get Allowed Titles (Class names, and their unlocked higher level names)
  - Set Title (can set to NULL)
- Divinity
  - Set
 
### Skill
Skills are the core of Chroniclers' character building.
Skills give Attributes-per-level, and may unlock Abilities, Perks and Skills on level thresholds.

#### Properties
- Name
- Attributes (every skill level, the magnitude of the Attribute increases)
- LevelMax (May be null for infinite)
- Unlocks (Which anything else is unlocked at which level)
 
#### Access Points
- Get
  - by ID
  - View List
- Create
- Delete
- Attributes
  - View List
  - View
  - Add
  - Remove
  - Set Magnitude
- LevelMax
  - Set
  - View
- Unlocks  (Mapping of level -> (Class, Skill, Perk, Ability). Three of those must be null.)
  - View List
  - Add
  - Remove
- Name
  - Set

### Class
Classes are entities that manage skills. Levelling up a skill will increase Class level (progress).
Note that not all Skills need a Class.
Class levels generally give basic attributes as rewards, such as Strength, Intelligence, etc.
Class levels may also unlock other classes, skills, perks or abilities.

#### Properties
- Name
- Evolutions (level -> New unlocked names)
- Skills (These skills level up the class)
- LevelMax
- SkillLevelRatio (This many skill levels are required for 1 class level)
- Unlocks

#### Access Points
- Get
  - by ID
  - List
- Create
- Delete
- Skills
  - View List
  - Add
  - Remove
- Attributes
  - View List
  - View
  - Add
  - Remove
  - Set Magnitude
- Unlocks  (Mapping of level -> (Class, Skill, Perk, Ability). Three of those must be null.)
  - View List
  - Add
  - Remove

### Perks
Perks are one-off bonuses that a Gamer can have. These are generally given from Skills or Classes,
but may be quest rewards given to a Gamer. In these cases, they are considered Bonus Perks.

Status effects are temporary perks, like 

#### Properties
- Name
- Description
- Attributes
- Unlocks
 
#### Access Points
- Get
  - by ID
  - List (filters?)
- Create
- Delete
- Description
  - Set
  - View
- Attributes (Mapping of attr -> magnitude)
  - View List
  - Add 
  - Remove
- Unlocks  (list of Ability, Skill, Class, Attribute)
  - View List
  - Add
  - Remove

### Attributes
Attributes are core statistics about a character. These are things like Health, Charisma, 
Frost Resistance, Movement Speed, Mana Regeneration, and Armor Rating.

They may be referenced in formulae in Abilities. 
Some Attributes may raise/lower other Attributes, like Strength increasing Health.
Attributes may not increase Attributes that increase other Attributes.

Most attributes only look at max value. 

#### Properties
- Name (dynamically chosen based on the sign of the value.)
- Value (dynamic, sign not rendered if negative name is present.)
- MaxValue
- Description
- Attributes (mapping of other attributes -> multipliers)

#### Access Points
- Get
  - by ID
  - List (filters?)
- Create
- Delete
- Name
  - View
  - Set (ex: Frost Resistance)
  - View Negative
  - Set Negative (ex: Frost Weakness)
- Description
  - View
  - Set
- Attributes
  - Set (add, remove(value=0) or overwrite)
  - View List

### Abilities
Abilities (and Spells) are actions that a character can make. Examples are Power Attack, Fireball, 
Lockpick and Skin (get hides from animals).

Some of these may have costs and/or magnitudes based on Attributes.

#### Properties
- Name
- Description (format string, uses {} to show either base or calculated values.)
- Cost (mapping of Attribute -> Magnitude, may cost multiple attributes)
- Duration (may be null)
- Magnitude (Magnitude.Base + (Magnitude.Attribute * Magnitude.Multiplier))
- AttackSpeed (1 / SwingDuration)
- Classification
- DivinityMod (effective Divinity is modified by this value)
- Source (May be null, or the Weapon/Enchantment the ability/effect is granted by)
 
#### Access Points
- Get
  - by ID
  - List (filters ?)
- Create
- Delete
- Name
  - Set
- Description 
  - View
  - Set
- Cost
  - View
  - Set (single Attr -> magnitude) (add/update/0-to-remove)
- Duration
  - Set
- Magnitude
  - Set Base 
  - Set Attribute (may be null if Multiplier is zero)
  - Set Multiplier (1.0, 0.8, 1.4, 2.0...)
- SwingDuration
  - Set Base
- Classification
  - Set
- Source
  - Set (overrides: SwingDuration, Magnitude.Base, Magnitude.Multiplier, Duration, Cost)

### Classification
(Ability) Classifications affect (damage/other) calculations. For example, the Fire
Classification would handle increasing Fire Damage for Fire Spells, while a Conjuration Classification
might just handle duration and cost reduction.

Calculations:
- Damage = Ability.Magnitude * (Magnifier / 100) * ((Piercer - Resistor) / 100) - May be fully prevented or even absorbed
- Cost = Ability.Cost * (0.99 ** Reductor) - May NOT become zero
- Duration = Ability.Duration * (Extender / 100) - No upper cap to the duration, damaging this attribute may decrease to 0.
- SwingDuration = Ability.SwingDuration * (0.99 ** Speeder) - May NOT become zero

#### Properties
- Name
- Resistor (ex: Fire Resistance)
- Magnifier (ex: Fire Damage)
- Extender (ex: Burning Duration)
- Piercer (ex: Fire Piercing)
- (Cost) Reductor (ex: Fire Affinity)
- Speeder (ex: Agility (raises attack speed))
 
#### Access Points
- Get
  - by ID
  - List
- Create
- Delete
- Name
  - Set
- Resistor (target.Resistor reduces magnitude, %-based)
  - View
  - Set
  - Set Base (generally 0)
- Piercer (user.Piercer reduces target.Resistor by this much.)
  - View
  - Set
  - Set Base (generally 0)
- Magnifier (user.Magnifier increases magnitude, %-based)
  - View
  - Set
  - Set Base (generally 100)
- Extender (user.Extender increases duration, %-based)
  - View
  - Set
  - Set Default (generally 100)
- Reductor (user.Reductor reduces cost for an ability)
  - View
  - Set
  - Set Default (generally 0)
- Speeder (user.Speeder speeds up how long an ability takes to execute)
  - View
  - Set
  - Set Default (generally 0)

### Relationship
A relationship is a score as to how good your relation ship is with another person or organization.

Typically, new people that do not belong to an organization start with a score of 0.

Typically, new people that do belong to an organization start with a score equal to which you have with the 
organization, if they have heard of you.

A relationship with a someone who could conceivably attracted to you, is Attraction. Otherwise, it is Reputation.

For example, Children will only have Reputation, but as they grow up, somewhere in puberty, it can shift to attraction.

#### Properties
- Type (Sexual/non-Sexual)
- Name
- Value

#### Access Points
(Indirect only)

### Enchantments
Enchantments are spells that are applied on top of weapon strikes.
They spend enchantment charge instead of mana.

#### Properties
- Name
- Description
- Ability
- Charge (amount used, 0 to use attribute costs directly)

#### Access Points
- Name
  - Set
- Description
  - Get
  - Set
- Ability
  - Set Ability (the ability activated on use)
- Charge
  - Set

### Weapon
An item that can be attacked with.

#### Properties
- Name
- Description
- Value
- Enchantments
- Enchantment Charge

#### Access Points
SwingDuration, Magnitude.Base, Magnitude.Multiplier, Duration, Cost
- Abilities
  - Add/Update WeaponAbility
  - Remove
- Name
  - Set
- Description
  - Get
  - Set
- Value
  - Get
  - Set
- Enchantments
  - Add/Update (SwingDuration, Magnitude.Base, Magnitude.Multiplier, Duration, Cost) (just abilities applied on top of others)
  - Remove

### WeaponAbility
Backend Only, describes an ability granted by a weapon.

#### Properties
- Name
- BaseDamage (overrides Ability.Magnitude.Base)
- SwingDuration (overrides Ability.Swingduration)
- DamageMult (overrides Ability.Magnitude.Multiplier)
- Cost (overrides Ability.Cost)
- Ability

#### Access Points
- Name
- BaseDamage
  - Get
  - Set
- SwingDuration
  - Get
  - Set
- DamageMult
  - Get
  - Set
- Cost
  - Get
  - Set
- Ability
  - Get
  - Set
  
### Armor
Armor/clothing can be worn, potentially reduces damage, and can be enchanted.

#### Properties
- Name
- ArmorRatings (Classification -> value mapping.)
- Unlock ()

#### Access Points
- Name
  - Set
- Unlocks  (Mapping of level -> (Attribute, Ability, Skill, Value) 2 out of Attr/Abil/Skill is null, if Abil is not, then 
value is null)
  - View List
  - Add
  - Remove

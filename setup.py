from distutils.core import setup

from setuptools import find_packages

setup(
    name='chronicler',
    version='1.0.2',
    packages=find_packages(),
    url='https://gitlab.com/Gloweye/chronicler',
    license='',
    author='Jacco van Dorp',
    author_email='jaccovandorp@gmail.com',
    description='Chronicler is a bookkeeper for my fanfic "A Dragon\'s Game. It\'s also a discord bot.'
)


import socket

sock = socket.create_connection(('127.0.0.1', 8888))

while True:
    message = input('What should we send?\n')
    data = message.encode()
    print(f'Sending....[{message}]')
    sock.send(data)
    answer = sock.recv(len(data) + 20)
    response = answer.decode()
    print(f'Received: [{response}]')
    if response == 'quit':
        break

sock.close()

# Abilities

## Fire Magic

### Firebolt
- Classification: Fire
- Base Damage: 20
- Scale Attribute: Intelligence
- Scale Factor: 0.2
- Cost: 20 Mana

### Flame Cloak
- Classification: Fire
- Base Damage: 5
- Duration: 30
- Radius: 2
- Cost 100 Mana

### Incinerate
- Classification: Fire
- Base Damage: 50
- Scale Attribute: Intelligence
- Scale Factor: 0.4
- Duration: 10
- Afterburn at 25% damage/second.
- Cost: 200 Mana


## Blunt Damage

### Punch
- Classification: Blunt
- Base Damage: 10
- Scale Attribute: Strength
- Scale Factor: 0.2
- Cost: 5 Stamina

## Slicing Damage

### Claws
- Classification: Slicing Damage
- Base Damage: 15
- Scale Attribute: Dexterity
- Scale Factor: 0.3
- Cost: 5 Stamina


## Mind

### Command Animal (Bosmer, High Survivor?)
- Classification: Illusion
- Base Level 6
- Scale Attribute: Charisma
- Scale Factor: 0.2
- Duration: 10 minutes
- Cost: 50 Mana, 50 Stamina


## Misc

### Night Eye (Khajiit, Vampire, high Alteration)
- Free
- Description only

### Waterbreathing (Argonian, high Alteration)
- Free
- Description only

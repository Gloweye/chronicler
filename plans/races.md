# Races

## Khajiit
### Attributes
- Strength - 8
- Dexterity - 12
- Endurance - 10
- Intelligence - 11
- Wisdom - 8
- Charisma - 11
### Abilities
- Claw Swipe
- Punch
- Night Eye

## Argonian
### Attributes
- Strength - 11
- Dexterity - 8
- Endurance - 12
- Intelligence - 9
- Wisdom - 11
- Charisma - 9
- Frost Resistance - -25
### Abilities
- Claw Swipe
- Punch
- Waterbreathing

## Nord
- Strength - 12
- Dexterity - 10
- Endurance - 12
- Intelligence - 8
- Wisdom - 10
- Charisma - 8
- Frost Resistance - 50
### Abilities
- Punch

## Redguard
### Attributes
- Strength - 12
- Dexterity - 11
- Endurance - 10
- Intelligence - 9
- Wisdom - 9
- Charisma - 9
- Poison Resistance - 50
- Stamina Regeneration - 5
### Abilities
- Punch

## Imperial
### Attributes
- Strength - 10
- Dexterity - 9
- Endurance - 10
- Intelligence - 10
- Wisdom - 11
- Charisma - 12
### Abilities
- Punch

## Breton
### Attributes
- Strength - 8
- Dexterity - 11
- Endurance - 9
- Intelligence - 12
- Wisdom - 12
- Charisma - 10
### Abilities
- Punch

## Altmer
### Attributes
- Strength - 9
- Dexterity - 9
- Endurance - 10
- Intelligence - 11
- Wisdom - 12
- Charisma - 10
- Mana - 50
- Mana Regeneration - 5
### Abilities
- Punch

## Dunmer
### Attributes
- Strength - 11
- Dexterity - 10
- Endurance - 9
- Intelligence - 11
- Wisdom - 10
- Charisma - 8
- Fire Resistance - 50
- Fire Damage - 20
### Abilities
- Punch
- Fire Cloak

## Bosmer
### Attributes
- Strength - 7
- Dexterity - 12
- Endurance - 11
- Intelligence - 9
- Wisdom - 11
- Charisma - 10
- Resist Poison - 50
### Abilities
- Punch
- Command Animal

## Orsimer
### Attributes
- Strength - 13
- Dexterity - 10
- Endurance - 12
- Intelligence - 8
- Wisdom - 9
- Charisma - 8
- Stamina - 50
### Abilities
- Punch






from typing import Optional, Any

from chronicler.discord.mentionable import Mentionable


class Role(Mentionable):
    _MENTION_PREFIX: str = '<@&'
    _MENTION_SUFFIX: str = '>'
    name: str = 'new role'
    permissions: Optional[int] = None
    position: int = None
    color: int = 0
    hoist: bool = False
    mentionable: bool = False
    managed: bool = False
    tags: Optional[Any] = None

    def __str__(self):
        return f'{self.__class__.__name__}({self.name})'


from typing import Optional, List, Union, Any

from chronicler.discord.bases import BaseDataClass
from chronicler.discord.snowflake import Snowflake
from chronicler.discord.user import User
from chronicler.discord.role import Role
from chronicler.discord.channel import Channel
from chronicler.discord.api import api


class Message(BaseDataClass):
    id: Snowflake = None
    channel_id: Snowflake
    guild_id: Optional[Snowflake] = None
    author: Optional[User] = None
    member: dict = None
    content: str
    timestamp: str
    edited_timestamp: Optional[int] = None
    tts: bool
    mention_everyone: bool
    mentions: List[User]
    mention_roles: List[Role]
    mention_channels: Optional[List[Channel]] = None
    attachments: Optional[list] = None  # We're gonna ignore it anyway
    embeds: Optional[list] = None
    reactions: list = None  # Perhaps implement someday?
    nonce: Union[str, int, None] = None
    pinned: bool
    webhook_id: Optional[Snowflake] = None
    type: int
    activity: Any = None
    application: Any = None
    message_reference: Any = None
    flags: int = 0

    async def reply(self, content='', embed=None):
        return await api.send_message(self.channel_id, content, embed)
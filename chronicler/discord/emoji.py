
from typing import Optional

from chronicler.discord.user import User
from chronicler.discord.bases import BaseDataClass
from chronicler.discord.snowflake import Snowflake


class Emoji(BaseDataClass):
    id: Snowflake = None
    name: Optional[str] = None
    roles: Optional[list] = None
    user: Optional[User] = None
    require_colons: Optional[bool] = None
    managed: Optional[bool] = None
    animated: Optional[bool] = None
    available: Optional[bool] = None

    def __format__(self, format_spec):
        if self.animated:
            return object.__format__(f'<a:{self.name}:{self.id!s}>', format_spec)
        else:
            return object.__format__(f'<:{self.name}:{self.id!s}>', format_spec)

    @classmethod
    def from_string(cls, string):
        # Inverse of the __format__ up there.
        return cls(id=string[1:-1].strip('a')[1:])

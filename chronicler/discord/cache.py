"""
Discord requests us to store as much as we can about everything in a cache
"""

from logging import getLogger
from datetime import datetime
from typing import Optional, Type, Tuple

from pydantic import BaseModel

from chronicler.discord.snowflake import Snowflake

logger = getLogger(__name__)


class DiscordCache:

    def __init__(self):
        self.entities = {}

    def __setitem__(self, snowflake: Snowflake, entity):
        self.entities.setdefault(type(entity), {})[snowflake] = entity, datetime.now()

    def __getitem__(self, item: Tuple[Type[BaseModel], Snowflake]):
        cls, snowflake = item
        if not isinstance(snowflake, Snowflake):
            snowflake = Snowflake(snowflake)
        return self.entities[cls][snowflake][0]

    def __delitem__(self, item: Tuple[Type[BaseModel], Snowflake]):
        cls, snowflake = item
        self.entities[cls].pop(snowflake)

    def exists(self, cls: Type[BaseModel], item):
        return item in self.entities.setdefault(cls, {})

    def get_age(self, cls: Type[BaseModel], snowflake: Snowflake):
        return self.entities[cls][snowflake][1]

    def purge(self, before: Optional[datetime] = None):
        if before is None:
            self.entities.clear()
            return

        outdated = list()
        for cls, instances in self.entities.items():
            for flake, tpl in instances.items():
                if tpl[1] < before:
                    outdated.append((cls, flake))
        # outdated = {(klass, flake) for flake, tpl in klass.items() for klass in self.entities.values() if tpl[1] < before}
        for cls, flake in outdated:
            del self.entities[cls][flake]


cache = DiscordCache()

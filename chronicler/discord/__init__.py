
def get_mentions(message_content: str):
    """Not break-safe"""
    ids = []
    while '<@!' in message_content:
        message_content = message_content[:message_content.find('<@!')]
        id_, message_content = message_content.split('>')
        ids.append(int(id_))

    return ids

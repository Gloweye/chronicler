
from typing import Optional, List, Any

from chronicler.discord.snowflake import Snowflake
from chronicler.discord.bases import BaseDataClass
from chronicler.discord.user import User
from chronicler.discord.channel import Channel
from chronicler.discord.emoji import Emoji
from chronicler.discord.role import Role


class GuildMember(BaseDataClass):
    user: Optional[User] = None
    nick: str = None
    roles: List[Snowflake] = None
    joined_at: str = None
    premium_since: str = None
    deaf: bool = None
    mute: bool = None


class Guild(BaseDataClass):
    id: Snowflake = None
    name: str = None
    icon: Optional[str] = None
    splash: Optional[str] = None
    discovery_splash: Optional[str] = None
    owner: bool = False
    owner_id: Snowflake = None
    permissions: Optional[int] = None
    region: str = None
    afk_channel_id: Optional[Snowflake] = None
    afk_timeout: int = None
    embed_enabled: Optional[bool] = None
    embed_channel_id: Optional[Snowflake] = None
    verification_level: int = None
    default_message_notifications: int = None
    explicit_content_filter: int = None
    roles: List[Role] = None
    emojis: List[Emoji] = None
    features: List = None
    mfa_level: int = None
    application_id: Optional[Snowflake] = None
    widget_enabled: Optional[bool] = None
    widget_channel_id: Optional[Snowflake] = None
    system_channel_id: Optional[Snowflake] = None
    system_channel_flags: int = None
    rules_channel_id: Optional[Snowflake] = None
    joined_at: Any = None
    large: Optional[bool] = False
    unavailable: Optional[bool] = False
    member_count: Optional[int] = None
    voice_states: Optional[list] = None
    members: Optional[List[GuildMember]] = None
    channels: Optional[List[Channel]] = None
    presences: Optional[list] = None
    max_presences: Optional[int] = None
    max_members: Optional[int] = None
    vanity_url_code: Optional[str] = None
    description: Optional[str] = None
    banner: Optional[str] = None
    premium_tier: int = None
    premium_subscription_count: Optional[int] = None
    preferred_locale: str = 'en-US'
    public_updates_channel_id: Optional[Snowflake] = None
    max_video_channel_users: Optional[int] = None
    approximate_member_count: Optional[int] = None
    approximate_presence_count: Optional[int] = None

    def __str__(self):
        return f'{self.__class__.__name__}({self.name})'

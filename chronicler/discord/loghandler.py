from logging import Handler, NOTSET, LogRecord
from asyncio import create_task
from asyncio.queues import Queue

from chronicler.discord.api import api


class DiscordHandler(Handler):
    """
    Handler that logs to a discord channel.
    """
    def __init__(self, discord_client, channel_id, level=NOTSET):
        super().__init__(level)
        self.discord_client = discord_client
        self.messages = Queue()
        self.channel_id = channel_id
        self.task = create_task(self.send_messages())

    async def send_messages(self):
        while True:
            message = await self.messages.get()
            await api.send_message(self.channel_id, content=message)

    async def cancel_and_wait(self):
        self.task.cancel()
        await self.task

    def update(self, channel_id, level):
        self.setLevel(level)
        self.channel_id = channel_id

    def emit(self, record: LogRecord):
        self.messages.put_nowait(f'```{record.getMessage()}```')  # Format as code to keep monospaced font.

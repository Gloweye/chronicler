

from typing import List

from pydantic import Field

from chronicler.discord.bases import BaseDataClass


class EmbedFooter(BaseDataClass):
    text: str = None
    icon_url: str = None
    proxy_icon_url: str = None


class EmbedField(BaseDataClass):
    name: str = None
    value: str = None
    inline: bool = True


class EmbedAuthor(BaseDataClass):
    name: str = None
    url: str = None
    icon_url: str = None
    proxy_icon_url: str = None


class EmbedProvider(BaseDataClass):
    name: str = None
    url: str = None


class EmbedImage(BaseDataClass):
    url: str = None
    proxy_url: str = None
    height: int = None
    width: int = None


class EmbedVideo(BaseDataClass):
    url: str = None
    height: int = None
    width: int = None


class EmbedThumbnail(BaseDataClass):
    url: str = None
    proxy_url: str = None
    height: int = None
    width: int = None


class Embed(BaseDataClass):
    title: str = None
    type: str = 'rich'
    description: str = None
    url: str = None
    color: int = None
    footer: EmbedFooter = None
    image: EmbedImage = None
    thumbnail: EmbedThumbnail = None
    video: EmbedVideo = None
    provider: EmbedProvider = None
    author: EmbedAuthor = None
    fields_: List[EmbedField] = Field(default_factory=list, alias='fields')

    class Config:
        allow_population_by_field_name = True

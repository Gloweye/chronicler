
from pydantic import BaseModel

from chronicler.discord.cache import cache


class BaseDataClass(BaseModel):

    def __init__(self, **kwargs):
        BaseModel.__init__(self, **kwargs)
        cls = type(self)
        if hasattr(self, 'id') and self.id is not None:
            if cache.exists(cls, self.id):
                self.update_from(cache[cls, self.id])

            cache[self.id] = self

    def update_from(self, old):
        for name, value in old.dict(skip_defaults=True, exclude_unset=True).items():
            if isinstance(getattr(self, name, None), list):
                setattr(self, name, self._combine_lists(value, getattr(self, name) or ()))
            else:
                setattr(self, name, value)

    @staticmethod
    def _combine_lists(old: list, new: list) -> list:
        if not old:
            return new

        if not hasattr(old[0], 'id'):
            return old + new

        ids = {item.id for item in old}
        for item in new:
            if item.id not in ids:
                old.append(item)

        return old



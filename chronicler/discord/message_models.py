
from typing import List, Optional, Tuple, Dict, Type
from logging import getLogger

from pydantic import BaseModel

from chronicler.discord.user import User
from chronicler.discord.guild import Guild
from chronicler.discord.message import Message
from chronicler.discord.channel import Channel


logger = getLogger(__name__)


class DispatchEvent(BaseModel):
    """Base class for all messages received through the gateway."""
    _dispatch: Dict[str, Type['DispatchEvent']] = {}

    def __init_subclass__(cls, **kwargs):
        cls._dispatch[cls.__name__.upper()] = cls

    @classmethod
    def register(cls, event_name: str, payload_type: Type[BaseModel]):
        cls._dispatch[event_name] = payload_type

    @classmethod
    def create(cls, event_name, data):
        logger.debug(f'Creation Model of type: {cls._dispatch[event_name].__name__} for event {event_name}')
        return cls._dispatch[event_name].parse_obj(data)


DispatchEvent.register('MESSAGE_CREATE', Message)
DispatchEvent.register('CHANNEL_CREATE', Channel)
DispatchEvent.register('CHANNEL_UPDATE', Channel)
DispatchEvent.register('CHANNEL_DELETE', Channel)
DispatchEvent.register('GUILD_CREATE', Guild)


class Hello(BaseModel):
    heartbeat_interval: int


class Ready(DispatchEvent):
    v: int
    user: User
    private_channels: list
    guilds: List[dict]
    session_id: str
    shards: Optional[Tuple[int, int]]


import asyncio
from collections import namedtuple
import json
from logging import getLogger
from typing import Dict, Callable, Union, Coroutine
import time
import random
from datetime import datetime
from pathlib import Path

import websockets

from chronicler.config import config
from chronicler.discord.enums import OpCode, Intent
from chronicler.discord.loghandler import DiscordHandler
from chronicler.discord.message import Message
from chronicler.discord.api import api, Method as HTTPMethod
from chronicler.discord.message_models import Hello, DispatchEvent, Ready
from chronicler.models.author import ServerGlobal
from chronicler.models.base import session


DiscordMessage = namedtuple('DiscordMessage', 'op, d, s, t', defaults=(None, None, None, None))
logger = getLogger(__name__)


class Discord:
    websocket: websockets.WebSocketClientProtocol

    def __init__(self):
        self.running = False
        self.op_codes: Dict[OpCode, Callable[[DiscordMessage], Coroutine]] = {
            OpCode.Hello: self._opcode_hello,
            OpCode.HeartbeatACK: self._opcode_heartbeat_response,
            OpCode.InvalidSession: self._opcode_invalid_session,
            OpCode.Reconnect: self._opcode_reconnect,
            OpCode.Dispatch: self._opcode_dispatch,
        }
        self.events: Dict[str, Callable[[DispatchEvent], Coroutine]] = {
            'READY': self._on_ready
        }
        self.last_message_id = 0
        self.heartbeat_interval = None
        self.last_heartbeat_response = None
        self._session_id = None
        self.bot_id = None
        self.loghandler = None

    async def run(self, attempt=0):
        self.websocket = await websockets.connect(await self.get_gateway_url())
        self.running = True
        logger.info('Starting to listen to websocket...')
        try:
            async for raw in self.websocket:
                if not self.running:
                    break

                attempt = 0  # We're running, so we can still hold all our retries.
                message = self._parse_message(raw)
                logger.debug(f'Received: {message}')
                if message.op in self.op_codes:
                    asyncio.create_task(self.op_codes[message.op](message))
                else:
                    await self._unhandled(message)

        except (ConnectionResetError, ConnectionAbortedError):
            await self.close()
            if attempt < int(config['main']['connectionRetries']) and not logger.isEnabledFor(10):  # 10 == logging.DEBUG
                logger.info(f'Connection died. Retrying...')
                asyncio.create_task(self.run(attempt + 1))
            else:
                logger.critical(f'All retries failed.')
                await self.close()
                raise
        except:
            logger.critical('Unhandled exception')
            await self.close()
            raise
        finally:
            if self.loghandler is not None:
                await self.loghandler.cancel_and_wait()

        logger.critical(f'Somehow exited loop. This is only allowed if self.running is False. It is: {self.running}')

    async def write(self, message: Union[str, dict]):
        logger.debug(f"Sending: {message}")
        if not isinstance(message, str):
            message = json.dumps(message)
        await self.websocket.send(message)

    @staticmethod
    async def _unhandled(message: DiscordMessage):
        logger.info(f'OpCode {message.op} with type {message.t} is unhandled: {message.d}')

    async def close(self):
        if self.running:
            self.running = False
            await self.websocket.close()
            del self.websocket

    def __call__(self, identifier: Union[OpCode, str]):
        """
        Decorates a coroutine. The coroutine is supposed to handle messages with the provided OpCode / message name.
        >>> client = Discord()
        >>> @client('MESSAGE_CREATE')
        >>> def handles_new_messages(event: Message): ...
        """
        if isinstance(identifier, OpCode):
            def decorator(func):
                self.op_codes[identifier] = func
                return func
        elif isinstance(identifier, str):
            def decorator(func):
                self.events[identifier] = func
                return func
        else:
            raise TypeError(f'Cannot register handler for type {identifier.__class__.__name__}')

        return decorator

    @staticmethod
    async def get_gateway_url():
        result = await api.request(method=HTTPMethod.GET, url=config['gateway']['url'])
        if result.status_code != 200:
            logger.error(f'Received status code {result.status_code}, contents: {result.json()}')
            asyncio.get_running_loop().stop()
            raise RuntimeError(f'Received status code {result.status_code}, contents: {result.json()}')

        result = result.json()
        if result.get('shards', 1) > 1:  # We don't plan on running on enough servers to require multiple bot instances.
            logger.warning(f'Shards recommended: {result["shards"]}. The API isn\'t designed to run on this many guilds.')

        session_starts = result['session_start_limit']
        logger.info('Remaining Session Starts: {remaining}/{total}'.format_map(session_starts))
        if session_starts['remaining'] < 100:
            # I seriously can't imagine this stuff going wrong, but if it does, lets error out.
            logger.critical('Aborting connection due to insufficient sessions starts.')
            asyncio.get_running_loop().stop()

        # There's probably an urllib function for this, but I can't be fucked to find it.
        url = result['url'] + '/?' + '&'.join(f'{key}={value}' for key, value in {
            'v': config['gateway']['version'],
            'encoding': config['gateway']['encoding']
        }.items())
        return url

    async def start_logger_if_sensible(self):
        # NOTE: Currently grabs any and errors out if multiple servers have a logger configured. So don't do that.
        chan_setting = session.query(ServerGlobal).filter(ServerGlobal.setting == 'LogChannel').one_or_none()
        chan_loglevel = session.query(ServerGlobal).filter(ServerGlobal.setting == 'LogLevel').one_or_none()
        if chan_loglevel is None or chan_setting is None:
            return

        await self.configure_logger(chan_setting.value, chan_loglevel.value)

    async def configure_logger(self, channel_id, level):
        if self.loghandler is None:
            self.loghandler = DiscordHandler(self, channel_id, level)
            getLogger().addHandler(self.loghandler)
        else:
            self.loghandler.update(channel_id, level)

    def _parse_message(self, data: str):
        message = DiscordMessage(**json.loads(data))
        if message.s is not None:
            self.last_message_id = message.s
        return message

    async def _heartbeat(self):
        logger.debug(f'Starting heartbeat with interval: {self.heartbeat_interval} msec')
        while self.running:
            await self.write({
                'op': OpCode.Heartbeat.value,
                'd': self.last_message_id
            })
            await asyncio.sleep(self.heartbeat_interval / 1000)  # Convert to seconds

    async def _identify(self):
        await self.write({
            'op': OpCode.Identify.value,
            'd': {
                'token': config['auth']['token'],
                'properties': {
                    '$os': 'windows',
                    '$browser': 'chronicler',
                    '$device': 'chronicler'
                },
                'intents': Intent.GUILD_MESSAGES | Intent.GUILDS | Intent.DIRECT_MESSAGES,
                'presence': {
                    'since': None,
                    'game': {
                        'name': 'Reading Gamer fics',
                        'type': 4,
                        'created_at': datetime.now().timestamp(),
                        'emoji': {'name': 'eyeglasses'},
                    },
                    'status': 'online',
                    'afk': False
                }
            }
        })

    async def _resume(self):
        await self.write({
            'op': OpCode.Resume.value,
            'd': {
                'token': config['auth']['token'],
                'session_id': self._session_id,
                'seq': self.last_message_id,
            }
        })

    # OpCode handlers
    async def _opcode_hello(self, message: DiscordMessage):
        hello = Hello.parse_obj(message.d)
        self.heartbeat_interval = hello.heartbeat_interval
        asyncio.create_task(self._heartbeat())
        if self._session_id is None:
            await self._identify()
        else:
            await self._resume()

    async def _opcode_heartbeat_response(self, _: DiscordMessage):
        self.last_heartbeat_response = time.time()

    async def _opcode_invalid_session(self, _: DiscordMessage):
        self._session_id = None
        await asyncio.sleep(random.randint(1, 5))
        await self._identify()

    async def _opcode_reconnect(self, _: DiscordMessage):
        await self.close()
        logger.info('Attempting to reconnect...')
        asyncio.create_task(self.run())

    async def _opcode_dispatch(self, message: DiscordMessage):
        try:
            type_ = message.t
            try:
                event = DispatchEvent.create(type_, message.d)
            except KeyError:
                event = message.d
            logger.debug(f'Handling event {type_} payload: {event.__class__.__name__}({event})')
            if type_ in self.events:
                await self.events[type_](event)
            else:
                logger.debug(f'{list(self.events.keys())} doesn\'t contain {type_}')
                await self._unhandled(message)
        except Exception as err:
            file = Path.home() / config["main"]["debugfile"]
            file.write_text(json.dumps(message.d, indent=4))
            logger.exception(err)
            if logger.isEnabledFor(10):  # 10 == logging.DEBUG
                await self.close()

    # Dispatch functions
    async def _on_ready(self, event: Ready):
        if event.v != int(config['gateway']['version']):
            logger.critical('Bot out of date.')
            await self.close()
            asyncio.get_running_loop().stop()
            raise RuntimeError('Bot out of date.')

        self._session_id = event.session_id
        logger.debug(repr(event.user))
        self.bot_id = event.user.id

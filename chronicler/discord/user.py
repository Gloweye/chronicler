
from typing import Optional

from chronicler.discord.mentionable import Mentionable


class User(Mentionable):
    _MENTION_PREFIX: str = '<@'
    _MENTION_SUFFIX: str = '>'
    username: str = None
    discriminator: str = None
    avatar: Optional[str] = None
    bot: Optional[bool] = None
    system: Optional[bool] = None
    mfa_enabled: Optional[bool] = None
    locale: Optional[str] = None
    verified: Optional[bool] = None
    email: Optional[str] = None
    flags: Optional[int] = None
    premium_type: Optional[int] = None
    public_flags: Optional[int] = None

    def __init__(self, mention: str = None, **kwargs):
        if mention is not None:
            mention = mention.replace('!', '')  # Compatibility with nickname mentions
        super().__init__(mention, **kwargs)

    @property
    def name(self):
        """Unique Name"""
        return f'{self.username}#{self.discriminator}'

    def __str__(self):
        return f'{self.__class__.__name__}({self.name})'

    @property
    def mention_nick(self):
        return f'<@!{self.id!s}>'

    @classmethod
    def mention_nick_from_id(cls, id_):
        return f'<@!{id_!s}>'

    @classmethod
    def from_string(cls, string):
        return cls(id=string[len(cls._MENTION_PREFIX):-len(cls._MENTION_SUFFIX)].strip('!'))

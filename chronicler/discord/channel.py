
from typing import Optional, List, Any

from chronicler.discord.snowflake import Snowflake
from chronicler.discord.user import User
from chronicler.discord.mentionable import Mentionable
from chronicler.discord.api import Method, api


class Channel(Mentionable):
    _MENTION_PREFIX: str = '<#'
    _MENTION_SUFFIX: str = '>'
    type: int = None
    guild_id: Optional[Snowflake] = None
    position: Optional[int] = None
    permission_overwrites: list = []
    name: Optional[str] = None
    topic: Optional[str] = None
    nsfw: bool = False
    last_message_id: Optional[Snowflake] = None  # Might not be a valid message
    bitrate: Optional[int] = None
    user_limit: Optional[int] = None
    rate_limit_per_user: Optional[int] = None
    recipients: Optional[List[User]] = None
    icon: Optional[str] = None
    owner_id: Optional[Snowflake] = None
    application_id: Optional[Snowflake] = None
    parent_id: Optional[Snowflake] = None
    last_pin_timestamp: Any = None

    def __str__(self):
        return f'{self.__class__.__name__}({self.name})'

    @classmethod
    async def get_by_id(cls, channel_id: Snowflake):
        return cls(**(await api.request(Method.GET, url=f'/channels/{channel_id}')).json())

    async def send_message(self, content='', embed=None):
        return await api.send_message(self.id, content=content, embed=embed)

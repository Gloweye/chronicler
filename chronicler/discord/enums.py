"""Intents are Discord's representation of what data we're interested in."""

from enum import IntFlag, IntEnum


class Intent(IntFlag):
    """Intents are Discord's representation of what data we're interested in."""

    GUILDS = 1
    GUILD_MEMBERS = 2  # Requires privileges
    GUILD_BANS = 4
    GUILD_EMOJIS = 8
    GUILD_INTEGRATIONS = 16
    GUILD_WEBHOOKS = 32
    GUILD_INVITES = 64
    GUILD_VOICE_STATES = 128
    GUILD_PRESENCES = 256  # Requires privileges
    GUILD_MESSAGES = 512
    GUILD_MESSAGE_REACTIONS = 1024
    GUILD_MESSAGE_TYPING = 2048
    DIRECT_MESSAGES = 4096
    DIRECT_MESSAGE_REACTIONS = 8192
    DIRECT_MESSAGE_TYPING = 16384


class OpCode(IntEnum):
    """Messages send/received contain OpCodes to signify the subject matter"""

    Dispatch = 0
    Heartbeat = 1
    Identify = 2
    PresenceUpdate = 3
    VoiceStateUpdate = 4

    Resume = 6
    Reconnect = 7
    RequestGuildMembers = 8
    InvalidSession = 9
    Hello = 10
    HeartbeatACK = 11


class CloseEventCode(IntEnum):
    Unknown = 4000  # Dunno. Turn it off and on again?
    UnknownOpcode = 4001  # Our message is invalid
    DecodeError = 4002  # Our payload was invalid.
    NotAuthenticated = 4003  # We didn't identify yet.
    AuthenticationFailure = 4004  # Our identification was rejected.
    AlreadyAuthenticated = 4005  # We already authenticated.

    Invalid = 4007  # Resuming Sequence is invalid. Please Reconnect and start a new session.
    RateLimited = 4008  # We're getting a hit on the nose with the newspaper for being to talkative.
    SessionTimedOut = 4009  # We should start a new session.
    InvalidShard = 4010  # This shard doesn't work.
    ShardingRequired = 4011  # We're connected to enough servers that we need to shard.
    InvalidAPIVersion = 4012  # We requested an invalid version for the gateway.
    InvalidIntent = 4013  # Our Gateway Intent value was invalid.
    DisallowedIntent = 4014  # We may not have enabled this intent. See https://discord.com/developers/docs/topics/gateway#privileged-intents





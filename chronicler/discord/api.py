
import asyncio

from pydantic import BaseModel

from requests import Request, Session, ConnectionError, Response
from enum import Enum
from typing import Union, Optional
from logging import getLogger

from chronicler.config import config
from chronicler.discord.snowflake import Snowflake

logger = getLogger(__name__)


class Method(str, Enum):
    GET = 'GET'
    POST = 'POST'


class Connection:
    """
    A connection to the Discord API, so that commands can be send.
    """

    def __init__(self):
        self.session = Session()
        self.session.headers.update({
            'Authorization': f"Bot {config['auth']['token']}",
            'User-Agent': 'DiscordBot (Chronicler, 1)'
        })
        self.base_url = config['api']['url']

    @staticmethod
    def _json(data_: dict) -> str:

        # Worker transforms Snowflakes into JSON-able strings.
        def worker(data: Union[dict, list, Snowflake, str, int]):
            if isinstance(data, dict):
                return {key: worker(value) for key, value in data.items()}
            elif isinstance(data, list):
                return [worker(value) for value in data]
            elif isinstance(data, Snowflake):
                return data.json()
            return data

        data_ = worker(data_)
        return data_

    async def request(self, method: Method, url: str, headers: dict = None, json: dict = None, params=None) -> Optional[Response]:
        # This will do the blocking wait in another thread, so that we can stay neatly asynchronous.
        try:
            response: Response = await asyncio.get_event_loop().run_in_executor(None, self.session.send, self.session.prepare_request(Request(
                method=method.value,
                url=self.base_url + url,
                headers=headers or {},
                json=json and self._json(json),
                params=params,
            )))
            logger.debug(f'Retrieved response with status {response.status_code} and contents {response.json()}')
            logger.debug(f'Headers: {response.request.headers}')
        except ConnectionError as e:
            logger.error(f'{e.__class__.__name__}:{e}')
            if int(config['api']['strict']):
                raise e
            else:
                return None

        if response.status_code > 299:
            if int(config['api']['strict']):
                logger.error(f'Sender: {method};{url};{headers};{json}')
                raise RuntimeError(f'HTTP: {response.status_code}; Details: {response.json()}')
            else:
                logger.warning(f'HTTP: {response.status_code}; Details: {response.json()}')

        return response

    async def send_message(self, channel_id: int, content: str = '', embed: Union[BaseModel, dict] = None):
        if isinstance(embed, BaseModel):
            embed = embed.dict(exclude_unset=True, by_alias=True)

        return await self.request(Method.POST, url=f'/channels/{channel_id}/messages', json={
            'content': content,
            'embed': embed or {},
        })


api = Connection()

"""Snowflakes are Discord's version of ID's."""

from typing import Union


class Snowflake(int):

    def __init__(self, flake: Union[str, int]):
        int.__init__(flake)
        self._timestamp = self >> 22
        self._worker = (self & 0x3E0000) >> 17
        self._process = (self & 0x3E0000) >> 12
        self._increment = self & 0xFFF

    @property
    def id_(self):
        return int(self)

    @property
    def timestamp(self):
        return self._timestamp + 1420070400000

    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, value):
        return cls(value) or True

    def __lt__(self, other):
        """This is a chronological comparison. Less means older."""
        if not isinstance(other, type(self)):
            raise ValueError(f'May only compare same type, not {self.__class__.__name__} and {other.__class__.__name__}')
        return self._timestamp < other._timestamp

    def __gt__(self, other):
        """This is a chronological comparison. Greater means younger."""
        if not isinstance(other, type(self)):
            raise ValueError(f'May only compare same type, not {self.__class__.__name__} and {other.__class__.__name__}')
        return self._timestamp > other._timestamp

    def __eq__(self, other: 'Snowflake'):
        return isinstance(other, Snowflake) and int(self) == int(other)

    def __repr__(self):
        return f'{self.__class__.__name__}({self})'

    def __int__(self):
        return 0 + self

    def __hash__(self):
        return hash(0 + self)

    def json(self):
        return str(self)

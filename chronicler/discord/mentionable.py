
from chronicler.discord.bases import BaseDataClass
from chronicler.discord.snowflake import Snowflake


class Mentionable(BaseDataClass):
    _MENTION_PREFIX: str = '<'
    _MENTION_SUFFIX: str = '>'
    id: Snowflake

    def __init__(self, mention: str = None, **kwargs):
        if mention is None:
            super().__init__(**kwargs)
        elif mention.startswith(self._MENTION_PREFIX) and mention.endswith(self._MENTION_SUFFIX):
            mention = mention[len(self._MENTION_PREFIX):-len(self._MENTION_SUFFIX)]
            super().__init__(id=int(mention))
            # BaseDataClass.__init__() will update the rest of the data from the previous version, and replace it in the cache with this one.
        else:
            raise RuntimeError(f'{type(self).__name__}({mention}, **{kwargs}) is not a valid constructor call.')

    @property
    def mention(self):
        return type(self).mention_from_id(self.id)

    @classmethod
    def mention_from_id(cls, id_):
        return f'{cls._MENTION_PREFIX}{id_}{cls._MENTION_SUFFIX}'

    @classmethod
    def create_child(cls, mention: str) -> 'Mentionable':
        exc = RuntimeError('No subclasses to attempt initialization of.')
        for subclass in cls.__subclasses__():
            try:
                return subclass(mention)
            except (RuntimeError, ValueError) as exc:
                exc = exc

        raise exc

    @classmethod
    def from_string(cls, string):
        return cls(id=string[len(cls._MENTION_PREFIX):-len(cls._MENTION_SUFFIX)].strip('!'))

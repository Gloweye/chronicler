
from sqlalchemy import Column, Integer, String

from chronicler.models.base import Base


class Infobox(Base):
    __tablename__ = 'information'

    entity_id: int = Column(Integer, primary_key=True, index=True, autoincrement=True)
    topic: str = Column(String, unique=True)
    content: str = Column(String)

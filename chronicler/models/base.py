"""Database classes and related initialization"""

from datetime import datetime, timedelta
from functools import wraps
from typing import Dict, Tuple, Any
from pathlib import Path

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, scoped_session, Session

from chronicler.config import config

db_file = Path.home() / config["db"]["file"]
SQLALCHEMY_DATABASE_URL = f'sqlite:///{db_file}'.replace('\\', '\\\\')  # Doubling up the backslashes, which is required...for some reason.
engine = create_engine(  # pylint: disable=invalid-name
    SQLALCHEMY_DATABASE_URL, connect_args={'check_same_thread': False}
)

Base = declarative_base()
session: Session = scoped_session(sessionmaker(bind=engine))


def simple_cache(lifetime=3600):
    def decorator(func):
        cache: Dict[Tuple[tuple, dict], Tuple[datetime, Any]] = {}

        def cache_purge():
            f"""Purges the cache of {func.__module__.__name__}.{func.__qualname__}()"""
            cache.clear()

        @wraps(func)
        def executor(*args, **kwargs):
            if (args, kwargs) not in cache or cache[args, kwargs][0] < datetime.now() - timedelta(seconds=lifetime):
                cache[(args, kwargs)] = datetime.now(), func(*args, **kwargs)

            return cache[(args, kwargs)]

        executor.cache_purge = cache_purge

        return executor
    return decorator


def finalize():
    import chronicler.models.character
    import chronicler.models.ability
    import chronicler.models.attribute
    import chronicler.models.author
    import chronicler.models.class_
    import chronicler.models.item
    import chronicler.models.perk
    import chronicler.models.skill
    import chronicler.models.info
    db_file.parent.mkdir(parents=True, exist_ok=True)
    Base.metadata.create_all(bind=engine)
    res = session.query(chronicler.models.author.Author).all()
    res = session.query(chronicler.models.ability.Classification).all()
    if __name__ == '__main__':
        print(res)

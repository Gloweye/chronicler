from collections import Counter
from itertools import chain
from typing import Dict, List, TYPE_CHECKING, Union

from sqlalchemy import Column, ForeignKey, Integer, String, Float, Text
from sqlalchemy.orm import relationship

from chronicler.models.base import Base, session

if TYPE_CHECKING:
    from chronicler.models.attribute import Attribute
    from chronicler.models.character import Character, NPC, FrozenCharacter


def counter_sum(*counters):
    """
    Required because Counter + Counter is moronic in that it discards negative values.
    :param counters: Counters to add together
    :return: Counter that is the sum of all contents, including negatives
    """
    result = Counter()
    for key, value in chain.from_iterable(counter.items() for counter in counters):
        result[key] += value

    return result


class Ability(Base):
    __tablename__ = 'abilities'

    entity_id: int = Column(Integer, primary_key=True, index=True, autoincrement=True)
    name: str = Column(String, unique=True)
    description: str = Column(Text, nullable=True)

    duration: int = Column(Integer, nullable=True)
    magnitude: int = Column(Integer, default=0)
    magnitude_scale: float = Column(Float, default=1)
    radius: float = Column(Float, default=0)
    scale_attribute_id: int = Column(Integer, ForeignKey('attributes.entity_id'), nullable=True)
    classification_id: int = Column(Integer, ForeignKey('classifications.entity_id'), nullable=True)
    swing_duration: float = Column(Float, default=1)
    divinity: int = Column(Integer, default=0)

    costs: 'List[AbilityCost]' = relationship('AbilityCost', back_populates='ability')
    classification: 'Classification' = relationship('Classification')
    scale_attribute: 'Attribute' = relationship('Attribute')

    def calc_magnitude(self, character, target=None) -> int:
        if self.magnitude is None:
            return 0
        mult = 1
        attr_bonus = 0
        if self.classification_id is not None:
            mult = self.classification.magnitude(character.attributes, getattr(target, 'attributes', Counter()))

        if self.scale_attribute_id is not None:
            attr_bonus = character.attributes[self.scale_attribute] * self.magnitude_scale

        return int(mult * (self.magnitude + attr_bonus))

    def attack_speed(self, character) -> float:
        return 1 / self.calc_swing_duration(character)

    def calc_swing_duration(self, character) -> float:
        if self.classification_id is None:
            return self.swing_duration

        return self.swing_duration * self.classification.swing_duration(character.attributes)

    def calc_duration(self, character) -> int:
        if self.duration is None:
            return 0
        if self.classification_id is None:
            return self.duration

        return int(self.duration * self.classification.duration(character.attributes))

    def cost(self, character) -> Dict['Attribute', int]:
        mult = 1 if self.classification_id is None else self.classification.cost_reduction(character.attributes)
        return {cost.attribute: int(mult * cost.magnitude) for cost in self.costs}

    def calc_radius(self, character) -> float:
        if self.classification_id is None:
            return self.radius

        return float(self.radius * self.classification.radius(character.attributes))

    @property
    def render_name(self):
        if self.classification_id is not None:
            return f'{self.classification.name}: {self.name}'
        else:
            return self.name

    @property
    def render_description(self):
        if self.scale_attribute_id is not None:
            return self.description.format_map({
                    'duration': f'**{self.duration}**',
                    'magnitude': f'**{self.magnitude}** + [{self.scale_attribute.name} * **{self.magnitude_scale}**]',
                    'attack_speed': f'**{1 / self.swing_duration}**',
                    'swing_duration': f'**{self.swing_duration}**',
                    'radius': f'**{self.radius}**',
                })
        else:
            return self.description.format_map({
                    'duration': f'**{self.duration}**',
                    'magnitude': f'**{self.magnitude}**',
                    'attack_speed': f'**{1 / self.swing_duration}**',
                    'swing_duration': f'**{self.swing_duration}**',
                    'radius': f'**{self.radius}**',
                })

    def description_for_character(self, character: 'Union[NPC, Character]'):
        return self.description.format_map({
                'duration': f'**{self.calc_duration(character)}**',
                'magnitude': f'**{self.calc_magnitude(character)}**',
                'attack_speed': f'**{self.attack_speed(character):.2f}**',
                'swing_duration': f'**{self.calc_swing_duration(character)}**',
                'radius': f'**{self.calc_radius(character):.2f}**',
            })


class Shout(Base):
    __tablename__ = 'shouts'

    entity_id: int = Column(Integer, primary_key=True, index=True, autoincrement=True)
    name: str = Column(String, unique=True)
    description: str = Column(Text, nullable=True)
    classification_id: int = Column(Integer, ForeignKey('classifications.entity_id'), nullable=True)
    difficulty: int = Column(Integer, default=1)  # How many Dragonsouls a Gamer needs to use each word in the shout.

    classification: 'Classification' = relationship('Classification')
    words: 'List[Word]' = relationship('Word', back_populates='shout')

    def word(self, character: 'Character') -> 'Word':
        try:
            return sorted(self.words, key=lambda word_: word_.word_id)[min(character.dragonsouls // self.difficulty, len(self.words) - 1)]
        except ZeroDivisionError:
            return self.words[-1]

    def divinity(self, character: 'Character') -> int:
        return self.word(character).divinity

    def calc_magnitude(self, character: 'Character', target: 'Character' = None) -> int:
        mult = 1
        if self.word(character).magnitude is None:
            return 0
        if self.classification_id is not None:
            mult = self.classification.magnitude(
                counter_sum(character.attributes, self.word(character).get_attributes()),
                getattr(target, 'attributes', Counter())
            )

        return int(mult * (self.word(character).magnitude + (character.dragonsouls * self.word(character).magnitude_soul_scale)))

    def attack_speed(self, character: 'Character') -> float:
        if self.classification_id is None:
            return 1 / self.word(character).swing_duration

        return 1 / (self.word(character).swing_duration * self.classification.swing_duration(
            counter_sum(character.attributes, self.word(character).get_attributes())))

    def calc_duration(self, character: 'Character') -> int:
        if self.classification_id is None or self.word(character).duration is None:
            return self.word(character).duration

        return int(self.word(character).duration * self.classification.duration(
            counter_sum(character.attributes, self.word(character).get_attributes())))

    def cost(self, character: 'Character') -> Dict['Attribute', int]:
        if not self.word(character).costs:
            return {}
        mult = 1 if self.classification_id is None else self.classification.cost_reduction(
            counter_sum(character.attributes, self.word(character).get_attributes()))
        return {cost.attribute: int(mult * cost.magnitude) for cost in self.word(character).costs}

    def radius(self, character: 'Character') -> float:
        if self.classification_id is None or self.word(character).radius is None:
            return self.word(character).radius

        return float(self.word(character).radius * self.classification.radius(
            counter_sum(character.attributes, self.word(character).get_attributes())))

    @property
    def type_and_name(self):
        if self.classification_id is not None:
            return f'{self.classification.name}: {self.name}'
        else:
            return self.name

    @property
    def render_description(self):
        # @TODO: Fix
        magnitude = '/'.join(str(word.magnitude) for word in self.words)
        magnitude_soul = '/'.join(str(word.magnitude_soul_scale) for word in self.words)
        return self.description.format_map({
            'duration': '**[' + '/'.join(str(word.duration) for word in self.words) + ']**',
            'magnitude': f'**[{magnitude}]** + (**[{magnitude_soul}]** * [Number of Dragon Souls])',
            'attack_speed': '**[' + '/'.join(str(1 / word.swing_duration) for word in self.words) + ']**',
            'radius': '**[' + '/'.join(str(word.radius) for word in self.words) + ']**',
        })

    def render_word_description(self, word):
        return self.description.format_map({
            'duration': f'**{word.duration}**',
            'magnitude': f'**{word.magnitude}** + (**{word.magnitude_soul_scale}** * [Number of Dragon Souls])',
            'attack_speed': f'**{word.swing_duration}**',
            'radius': f'**{word.radius}**'
        })

    def description_for_character(self, character: 'Union[NPC, Character]'):
        return self.description.format_map({
                'duration': f'**{self.calc_duration(character)}**',
                'magnitude': f'**{self.calc_magnitude(character)}**',
                'attack_speed': f'**{self.attack_speed(character):.2f}**',
                'radius': f'**{self.radius(character):.2f}**',
            })


class Word(Base):
    __tablename__ = 'words'

    entity_id: int = Column(Integer, primary_key=True, autoincrement=True)
    shout_id: int = Column(Integer, ForeignKey('shouts.entity_id'), index=True)
    word_id: int = Column(Integer)
    name: str = Column(String)  # English
    duration: int = Column(Integer, nullable=True)
    translation: str = Column(String)  # Dovahzul
    magnitude: int = Column(Integer, default=0)
    magnitude_soul_scale: int = Column(Integer, default=0)
    swing_duration: float = Column(Float, default=1)
    divinity: int = Column(Integer, default=0)
    radius: float = Column(Float, default=0)

    shout: 'Shout' = relationship('Shout', back_populates='words')
    costs: 'List[WordCost]' = relationship('WordCost', back_populates='word')
    attributes: 'List[WordAttribute]' = relationship('WordAttribute', back_populates='word')

    def get_attributes(self):
        attributes = Counter()

        for word_attr in self.attributes:
            attributes[word_attr.attribute] += word_attr.value

        return attributes


class CharacterShout(Base):
    __tablename__ = 'character_shouts'

    character_id: int = Column(Integer, ForeignKey('characters.entity_id'), primary_key=True)
    shout_id: int = Column(Integer, ForeignKey('shouts.entity_id'), primary_key=True)

    character: 'Character' = relationship('Character', back_populates='shouts')
    shout: 'Shout' = relationship('Shout')

    @property
    def max_words(self):
        return self.shout.word(self.character).word_id + 1

    def freeze(self, character: 'FrozenCharacter'):
        session.add(FrozenShout(character=character, shout=self.shout, max_words=self.max_words))


class FrozenShout(Base):
    __tablename__ = 'frozen_shouts'

    character_id: int = Column(Integer, ForeignKey('frozen_characters.entity_id'), primary_key=True)
    shout_id: int = Column(Integer, ForeignKey('shouts.entity_id'), primary_key=True)
    max_words: int = Column(Integer)

    character: 'FrozenCharacter' = relationship('FrozenCharacter', back_populates='shouts')
    shout: 'Shout' = relationship('Shout')

    @property
    def name(self):
        return self.shout.name

    @property
    def words(self):
        return self.shout.words


class FrozenAbility(Base):
    __tablename__ = 'frozen_abilities'

    character_id: int = Column(Integer, ForeignKey('frozen_characters.entity_id'), primary_key=True)
    ability_id: int = Column(Integer, ForeignKey('abilities.entity_id'), primary_key=True)

    character: 'FrozenCharacter' = relationship('FrozenCharacter', back_populates='abilities')


class WordAttribute(Base):
    __tablename__ = 'word_attribute'
    # Attribute boni when this word is used.

    word_id: int = Column(Integer, ForeignKey('words.entity_id'), primary_key=True)
    attribute_id: int = Column(Integer, ForeignKey('attributes.entity_id'), primary_key=True)
    value: int = Column(Integer)

    word: 'Word' = relationship('Word', back_populates='attributes')
    attribute: 'Attribute' = relationship('Attribute')


class WordCost(Base):
    __tablename__ = 'word_cost'

    word_id: int = Column(Integer, ForeignKey('words.entity_id'), primary_key=True)
    attribute_id: int = Column(Integer, ForeignKey('attributes.entity_id'), primary_key=True)
    magnitude: int = Column(Integer)

    word: 'Word' = relationship('Word', back_populates='costs')
    attribute: 'Attribute' = relationship('Attribute')


class AbilityCost(Base):
    __tablename__ = 'ability_cost'

    ability_id: int = Column(Integer, ForeignKey('abilities.entity_id'), primary_key=True)
    attribute_id: int = Column(Integer, ForeignKey('attributes.entity_id'), primary_key=True)
    magnitude: int = Column(Integer)

    ability: 'Ability' = relationship('Ability', back_populates='costs')
    attribute: 'Attribute' = relationship('Attribute')


class Classification(Base):
    __tablename__ = 'classifications'

    entity_id: int = Column(Integer, primary_key=True, index=True, autoincrement=True)
    name: str = Column(String, unique=True)
    emoji: int = Column(Integer, nullable=True)
    resistor_id: int = Column(Integer, ForeignKey('attributes.entity_id'), nullable=True)
    magnifier_id: int = Column(Integer, ForeignKey('attributes.entity_id'), nullable=True)
    extender_id: int = Column(Integer, ForeignKey('attributes.entity_id'), nullable=True)
    piercer_id: int = Column(Integer, ForeignKey('attributes.entity_id'), nullable=True)
    reductor_id: int = Column(Integer, ForeignKey('attributes.entity_id'), nullable=True)
    speeder_id: int = Column(Integer, ForeignKey('attributes.entity_id'), nullable=True)
    expander_id: int = Column(Integer, ForeignKey('attributes.entity_id'), nullable=True)

    resistor: 'Attribute' = relationship('Attribute', foreign_keys=[resistor_id])
    magnifier: 'Attribute' = relationship('Attribute', foreign_keys=[magnifier_id])
    extender: 'Attribute' = relationship('Attribute', foreign_keys=[extender_id])
    piercer: 'Attribute' = relationship('Attribute', foreign_keys=[piercer_id])
    reductor: 'Attribute' = relationship('Attribute', foreign_keys=[reductor_id])
    speeder: 'Attribute' = relationship('Attribute', foreign_keys=[speeder_id])
    expander: 'Attribute' = relationship('Attribute', foreign_keys=[expander_id])

    def magnitude(self, attributes: 'Counter[Attribute, int]', target: 'Counter[Attribute, int]') -> float:
        # attributes[self.magnifier] == 100 results in a multiplier of 1, which is "no effect"
        # Note that if resist > pierce + 100, damage is absorbed and heals instead. Absorption is signified by a negative result.
        return ((100 + attributes[self.magnifier]) * (100 + attributes[self.piercer] - target[self.resistor])) / 10000

    def cost_reduction(self, attributes: 'Counter[Attribute, int]') -> float:
        return 0.99 ** attributes[self.reductor]

    def duration(self, attributes: 'Counter[Attribute, int]') -> float:
        extend = attributes[self.extender] + 100  # 100 results in a multiplier of 1, which is "no effect"
        return extend * 0.01

    def swing_duration(self, attributes: 'Counter[Attribute, int]') -> float:
        return 0.99 ** attributes[self.speeder]

    def radius(self, attributes: 'Counter[Attribute, int]') -> float:
        radius = attributes[self.expander] + 100
        return radius * 0.01

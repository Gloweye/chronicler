
from typing import TYPE_CHECKING, List

from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship

from chronicler.models.base import Base
from chronicler.discord.snowflake import Snowflake

if TYPE_CHECKING:
    from chronicler.models.character import Character


class ServerGlobal(Base):
    __tablename__ = 'server_globals'

    guild_id: int = Column(Integer, primary_key=True, index=True)
    setting: str = Column(String, primary_key=True)
    value: str = Column(String)


class Author(Base):
    __tablename__ = 'authors'

    entity_id: int = Column(Integer, primary_key=True, index=True, autoincrement=True)
    user_id: int = Column(Integer)
    guild_id: int = Column(Integer)
    channels: 'List[AuthorChannel]' = relationship('AuthorChannel', back_populates='author')
    characters: 'List[Character]' = relationship('Character', back_populates='author')


class AuthorChannel(Base):
    __tablename__ = 'author_channels'

    author_id: int = Column(Integer, ForeignKey('authors.entity_id'), primary_key=True)
    channel_id: int = Column(Integer, primary_key=True)

    author: 'Author' = relationship('Author', back_populates='channels')

    @property
    def channel(self) -> Snowflake:
        return Snowflake(self._channel)


class ChannelRenderer(Base):
    __tablename__ = 'renderers'

    channel_id: int = Column(Integer, primary_key=True)
    renderer: str = Column(String)


class OpenChannel(Base):
    __tablename__ = 'open_channels'

    channel_id: int = Column(Integer, primary_key=True)

    @property
    def channel(self) -> Snowflake:
        return Snowflake(self._channel)


from typing import Dict, List, Generator, Optional, TYPE_CHECKING

from sqlalchemy import Column, ForeignKey, Integer, String, Float, Text
from sqlalchemy.orm import relationship

from chronicler.models.base import Base, simple_cache, session

if TYPE_CHECKING:
    from chronicler.models.character import Character, FrozenCharacter
    from chronicler.models.ability import Ability
    from chronicler.models.attribute import Attribute
    from chronicler.models.perk import Perk
    from chronicler.models.skill import Skill


class ClassUnlock(Base):
    __tablename__ = 'class_unlocks'

    entity_id: int = Column(Integer, primary_key=True, index=True, autoincrement=True)
    parent_id: int = Column(Integer, ForeignKey('classes.entity_id'))
    level: int = Column(Integer)

    class_id: int = Column(Integer, ForeignKey('classes.entity_id'), nullable=True)
    skill_id: int = Column(Integer, ForeignKey('skills.entity_id'), nullable=True)
    perk_id: int = Column(Integer, ForeignKey('perks.entity_id'), nullable=True)
    ability_id: int = Column(Integer, ForeignKey('abilities.entity_id'), nullable=True)

    parent: 'Class' = relationship('Class', foreign_keys=[parent_id], back_populates='unlocks')
    class_: 'Class' = relationship('Class', foreign_keys=[class_id])
    skill: 'Skill' = relationship('Skill')
    perk: 'Perk' = relationship('Perk')
    ability: 'Ability' = relationship('Ability')


class Class(Base):
    __tablename__ = 'classes'

    entity_id: int = Column(Integer, primary_key=True, index=True, autoincrement=True)
    level_max: int = Column(Integer, nullable=True)
    skill_level_ratio: float = Column(Float, default=10)

    unlocks: 'List[ClassUnlock]' = relationship('ClassUnlock', primaryjoin=entity_id == ClassUnlock.parent_id, back_populates='parent')
    attributes: 'List[ClassAttribute]' = relationship('ClassAttribute', back_populates='class_')
    skills: 'List[Skill]' = relationship('Skill', back_populates='class_')
    info: 'List[ClassInfo]' = relationship('ClassInfo', back_populates='class_')

    @property
    def name_fallback(self) -> str:
        for inf in self.info:
            if inf.level == 0:
                return inf.name

    @property
    def description_fallback(self) -> Optional[str]:
        for inf in self.info:
            if inf.level == 0:
                return inf.description

    @classmethod
    def get_by_name(cls, name: str) -> 'Class':
        return session.query(ClassInfo).filter(ClassInfo.name == name).one().class_  # Yes, this has to be gotten through the info.


class CharacterClass(Base):
    __tablename__ = 'character_classes'

    character_id: int = Column(Integer, ForeignKey('characters.entity_id'), primary_key=True)
    class_id: int = Column(Integer, ForeignKey('classes.entity_id'), primary_key=True)

    character: 'Character' = relationship('Character', back_populates='classes')
    class_: 'Class' = relationship('Class')

    @property
    def level(self) -> int:
        return int(sum(char_skill.level for char_skill in self.character.skills if char_skill.skill in self.class_.skills) / self.class_.skill_level_ratio)

    @property
    def unlocks(self) -> Generator['ClassUnlock', None, None]:
        for unlock in self.class_.unlocks:  # type: ClassUnlock
            if unlock.level <= self.level:
                yield unlock

    @property
    def attributes(self) -> Dict['Attribute', int]:
        return {class_attr.attribute: int(class_attr.multiplier * self.level) for class_attr in self.class_.attributes}

    @property
    def titles(self) -> Generator[str, None, None]:
        """Gives all titles unlocked by the class."""
        for inf in self.class_.info:
            if self.level >= inf.level:
                yield inf.name

    @property
    def title(self) -> str:
        """Gives the allowed title with the highest level."""
        name = 'No Name Assigned'
        name_lvl = -1
        for inf in self.class_.info:
            if self.level >= inf.level > name_lvl:
                name = inf.name
                name_lvl = inf.level
        return name

    @property
    def description(self) -> Optional[str]:
        desc = None
        desc_lvl = -1
        for inf in self.class_.info:
            if self.level >= inf.level > desc_lvl and inf.description is not None:
                desc = inf.description
                desc_lvl = inf.level
        return desc

    def freeze(self, character: 'FrozenCharacter'):
        session.add(FrozenClass(character=character, class_=self.class_, level=self.level, name=self.title))


class ClassAttribute(Base):
    __tablename__ = 'class_attributes'

    class_id: int = Column(Integer, ForeignKey('classes.entity_id'), primary_key=True)
    attribute_id: int = Column(Integer, ForeignKey('attributes.entity_id'), primary_key=True)
    multiplier: float = Column(Float)

    class_: 'Class' = relationship('Class')
    attribute: 'Attribute' = relationship('Attribute')


class ClassInfo(Base):
    __tablename__ = 'class_info'

    class_id: int = Column(Integer, ForeignKey('classes.entity_id'), primary_key=True)
    level: int = Column(Integer, primary_key=True)
    name: str = Column(String)
    description: str = Column(Text, nullable=True)

    class_: 'Class' = relationship('Class', back_populates='info')


class FrozenClass(Base):
    __tablename__ = 'frozen_classes'

    class_id: int = Column(Integer, ForeignKey('classes.entity_id'), primary_key=True)
    character_id: int = Column(Integer, ForeignKey('frozen_characters.entity_id'), primary_key=True)
    level: int = Column(Integer)
    name: str = Column(String)

    character: 'Character' = relationship('FrozenCharacter', back_populates='classes')
    class_: 'Class' = relationship('Class')

    @property  # Still needed?
    def unlocks(self) -> Generator['ClassUnlock', None, None]:
        for unlock in self.class_.unlocks:  # type: ClassUnlock
            if unlock.level <= self.level:
                yield unlock

    @property  # Still needed?
    def attributes(self) -> Dict['Attribute', int]:
        return {class_attr.attribute: int(class_attr.multiplier * self.level) for class_attr in self.class_.attributes}

    @property
    def titles(self) -> Generator[str, None, None]:
        """Gives all titles unlocked by the class."""
        for inf in self.class_.info:
            if self.level >= inf.level:
                yield inf.name

    @property
    def title(self) -> str:
        """Gives the allowed title with the highest level."""
        name = 'No Name Assigned'
        name_lvl = -1
        for inf in self.class_.info:
            if self.level >= inf.level > name_lvl:
                name = inf.name
                name_lvl = inf.level
        return name

    @property
    def description(self) -> Optional[str]:
        desc = None
        desc_lvl = -1
        for inf in self.class_.info:
            if self.level >= inf.level > desc_lvl and inf.description is not None:
                desc = inf.description
                desc_lvl = inf.level
        return desc


from typing import Dict, List, Generator, TYPE_CHECKING

from sqlalchemy import Column, ForeignKey, Integer, String, Float, Text
from sqlalchemy.orm import relationship

from chronicler.models.base import Base, session

if TYPE_CHECKING:
    from chronicler.models.character import Character, FrozenCharacter
    from chronicler.models.class_ import Class
    from chronicler.models.ability import Ability
    from chronicler.models.attribute import Attribute
    from chronicler.models.perk import Perk


class SkillUnlock(Base):
    __tablename__ = 'skill_unlocks'

    entity_id: int = Column(Integer, primary_key=True, index=True, autoincrement=True)
    parent_id: int = Column(Integer, ForeignKey('skills.entity_id'))
    level: int = Column(Integer)

    class_id: int = Column(Integer, ForeignKey('classes.entity_id'), nullable=True)
    skill_id: int = Column(Integer, ForeignKey('skills.entity_id'), nullable=True)
    perk_id: int = Column(Integer, ForeignKey('perks.entity_id'), nullable=True)
    ability_id: int = Column(Integer, ForeignKey('abilities.entity_id'), nullable=True)

    parent: 'Skill' = relationship('Skill', foreign_keys=[parent_id], back_populates='unlocks')
    class_: 'Class' = relationship('Class')
    skill: 'Skill' = relationship('Skill', foreign_keys=[skill_id])
    perk: 'Perk' = relationship('Perk')
    ability: 'Ability' = relationship('Ability')


class Skill(Base):
    __tablename__ = 'skills'

    entity_id: int = Column(Integer, primary_key=True, index=True, autoincrement=True)
    name: str = Column(String)
    description: str = Column(Text, nullable=True)
    level_max: int = Column(Integer, nullable=True)
    class_id: int = Column(Integer, ForeignKey('classes.entity_id'), nullable=True)

    class_: 'Class' = relationship('Class', back_populates='skills')
    unlocks: List['SkillUnlock'] = relationship('SkillUnlock', primaryjoin=entity_id == SkillUnlock.parent_id, back_populates='parent')
    attributes: List['SkillAttribute'] = relationship('SkillAttribute', back_populates='skill')


class CharacterSkill(Base):
    __tablename__ = 'character_skills'

    character_id: int = Column(Integer, ForeignKey('characters.entity_id'), primary_key=True)
    skill_id: int = Column(Integer, ForeignKey('skills.entity_id'), primary_key=True)
    level: int = Column(Integer, default=0)

    character: 'Character' = relationship('Character', back_populates='skills')
    skill: 'Skill' = relationship('Skill')

    @property
    def name(self):
        return self.skill.name

    @property
    def unlocks(self) -> Generator['SkillUnlock', None, None]:
        for unlock in self.skill.unlocks:
            if unlock.level <= self.level:
                yield unlock

    @property
    def attributes(self) -> Dict['Attribute', int]:
        return {skill_attr.attribute: int(skill_attr.multiplier * self.level) for skill_attr in self.skill.attributes}

    def freeze(self, character: 'FrozenCharacter'):
        session.add(FrozenSkill(character=character, skill=self.skill, level=self.level))


class SkillAttribute(Base):
    __tablename__ = 'skill_attributes'

    skill_id: int = Column(Integer, ForeignKey('skills.entity_id'), primary_key=True)
    attribute_id: int = Column(Integer, ForeignKey('attributes.entity_id'), primary_key=True)
    multiplier: float = Column(Float)

    skill: 'Skill' = relationship('Skill', back_populates='attributes')
    attribute: 'Attribute' = relationship('Attribute')


class FrozenSkill(Base):
    __tablename__ = 'frozen_skills'

    skill_id: int = Column(Integer, ForeignKey('skills.entity_id'), primary_key=True)
    character_id: int = Column(Integer, ForeignKey('frozen_characters.entity_id'), primary_key=True)
    level: int = Column(Integer)

    character: 'Character' = relationship('FrozenCharacter', back_populates='skills')
    skill: 'Skill' = relationship('Skill')

    @property
    def name(self):
        return self.skill.name

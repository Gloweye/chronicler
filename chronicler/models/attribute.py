from typing import List, TYPE_CHECKING

from sqlalchemy import Boolean, Column, ForeignKey, Integer, String, Float, Text
from sqlalchemy.orm import relationship

from chronicler.models.base import Base, session

if TYPE_CHECKING:
    from chronicler.models.character import FrozenCharacter, Character


class SubAttribute(Base):
    """Attributes bestowed by other attributes"""
    __tablename__ = 'sub_attributes'

    parent_id: int = Column(Integer, ForeignKey('attributes.entity_id'), primary_key=True)
    child_id: int = Column(Integer, ForeignKey('attributes.entity_id'), primary_key=True)
    multiplier: float = Column(Float)

    parent: 'Attribute' = relationship('Attribute', foreign_keys=[parent_id], back_populates='attributes')
    child: 'Attribute' = relationship('Attribute', foreign_keys=[child_id])


class Attribute(Base):
    __tablename__ = 'attributes'

    entity_id: int = Column(Integer, primary_key=True, index=True, autoincrement=True)
    name: str = Column(String, nullable=True, default=None, unique=True)
    max_value: int = Column(Integer, nullable=True)
    description: str = Column(Text, default='')
    dragon_soul_mod: float = Column(Float, default=0)  # Multiplier per dragon soul, attr = base_attr * (1 + (Character.DragonSouls * attr.DragonSoulMod))
    emoji: int = Column(Integer, nullable=True)  # Probably only gonna use it for HP/MP/SP

    attributes: 'List[SubAttribute]' = relationship('SubAttribute', primaryjoin=entity_id == SubAttribute.parent_id, back_populates='parent')

    def __hash__(self):  # Instances are used as dict keys on character.attributes dict.
        return hash(f'Attribute{self.entity_id}')

    def freeze(self, character: 'FrozenCharacter', value: int):
        attr_damage = session.query(AttributeDamage).filter(AttributeDamage.attribute == self, AttributeDamage.character == character.character).one_or_none()
        damage = attr_damage.damage if attr_damage is not None else 0
        session.add(FrozenAttribute(character=character, attribute=self, value=value - damage, max_value=value))


class AttributeDamage(Base):
    __tablename__ = 'attr_damage'

    character_id: int = Column(Integer, ForeignKey('characters.entity_id'), primary_key=True)
    attribute_id: int = Column(Integer, ForeignKey('attributes.entity_id'), primary_key=True)
    damage: int = Column(Integer)

    character: 'Character' = relationship('Character', back_populates='damage')
    attribute: 'Attribute' = relationship('Attribute')


class FrozenAttribute(Base):
    __tablename__ = 'frozen_attributes'

    character_id: int = Column(Integer, ForeignKey('frozen_characters.entity_id'), primary_key=True)
    attribute_id: int = Column(Integer, ForeignKey('attributes.entity_id'), primary_key=True)
    value: int = Column(Integer)
    max_value: int = Column(Integer)

    character: 'FrozenCharacter' = relationship('FrozenCharacter', back_populates='_attributes')
    attribute: 'Attribute' = relationship('Attribute')

    @property
    def name(self):
        return self.attribute.name

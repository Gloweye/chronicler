

from typing import List, TYPE_CHECKING

from sqlalchemy import Column, ForeignKey, Integer, String, Text
from sqlalchemy.orm import relationship

from chronicler.models.base import Base, session
from chronicler.models.attribute import Attribute
from chronicler.models.ability import Ability

if TYPE_CHECKING:
    from chronicler.models.character import Character, FrozenCharacter
    from chronicler.models.skill import Skill
    from chronicler.models.class_ import Class


class PerkUnlock(Base):
    __tablename__ = 'perk_unlocks'

    entity_id: int = Column(Integer, primary_key=True, index=True, autoincrement=True)
    parent_id: int = Column(Integer, ForeignKey('perks.entity_id'))

    class_id: int = Column(Integer, ForeignKey('classes.entity_id'), nullable=True)
    skill_id: int = Column(Integer, ForeignKey('skills.entity_id'), nullable=True)
    perk_id: int = Column(Integer, ForeignKey('perks.entity_id'), nullable=True)
    ability_id: int = Column(Integer, ForeignKey('abilities.entity_id'), nullable=True)

    parent: 'Perk' = relationship('Perk', foreign_keys=[parent_id], back_populates='unlocks')
    class_: 'Class' = relationship('Class')
    skill: 'Skill' = relationship('Skill')
    perk: 'Perk' = relationship('Perk', foreign_keys=[perk_id])
    ability: 'Ability' = relationship('Ability')


class Perk(Base):
    __tablename__ = 'perks'

    entity_id: int = Column(Integer, primary_key=True, index=True, autoincrement=True)
    name: str = Column(String)
    description: str = Column(Text, nullable=True)

    attributes: List['PerkAttribute'] = relationship('PerkAttribute', back_populates='perk')
    unlocks: List['PerkUnlock'] = relationship('PerkUnlock', primaryjoin=entity_id == PerkUnlock.parent_id, back_populates='parent')

    def freeze(self, character: 'FrozenCharacter'):
        session.add(FrozenPerk(perk=self, character=character))


class PerkAttribute(Base):
    __tablename__ = 'perk_attributes'

    perk_id: int = Column(Integer, ForeignKey('perks.entity_id'), primary_key=True)
    attribute_id: int = Column(Integer, ForeignKey('attributes.entity_id'), primary_key=True)
    value: int = Column(Integer)

    perk: 'Perk' = relationship('Perk', back_populates='attributes')
    attribute: 'Attribute' = relationship('Attribute')


class BonusPerk(Base):
    __tablename__ = 'bonus_perks'

    character_id: int = Column(Integer, ForeignKey('characters.entity_id'), primary_key=True)
    perk_id: int = Column(Integer, ForeignKey('perks.entity_id'), primary_key=True)

    character: 'Character' = relationship('Character', back_populates='bonus_perks')
    perk: 'Perk' = relationship('Perk')


class FrozenPerk(Base):
    __tablename__ = 'frozen_perks'

    character_id: int = Column(Integer, ForeignKey('frozen_characters.entity_id'), primary_key=True)
    perk_id: int = Column(Integer, ForeignKey('perks.entity_id'), primary_key=True)

    character: 'Character' = relationship('FrozenCharacter', back_populates='perks')
    perk: 'Perk' = relationship('Perk')

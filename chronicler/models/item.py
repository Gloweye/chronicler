
from typing import Dict, List, Optional, TYPE_CHECKING, Union

from sqlalchemy import Column, ForeignKey, Integer, String, Text, Table, Boolean
from sqlalchemy.orm import relationship

from chronicler.models.base import Base, session

if TYPE_CHECKING:
    from chronicler.models.character import Character, FrozenCharacter
    from chronicler.models.attribute import Attribute
    from chronicler.models.perk import Perk
    from chronicler.models.ability import Ability


weapon_ability_table = Table(
    'weapon_abilities', Base.metadata,
    Column('weapon_id', Integer, ForeignKey('weapons.entity_id')),
    Column('ability_id', Integer, ForeignKey('abilities.entity_id')),
)


class DummyAbility:
    newline = '\n'

    def __init__(self, ability: 'Ability', enchants: 'List[WeaponEnchantment]', source: Union['Weapon', 'FrozenWeapon']):
        self.name = f'{ability.name} (from: {source.name})'
        self.abil = ability
        self.enchants = enchants

    def description_for_character(self, character: 'Character'):
        desc = self.abil.description_for_character(character)
        if self.enchants:
            desc = f'\n- {desc}'
        for enchant in self.enchants:
            desc += f'\n- {enchant.ability.description_for_character(character)}'
        return desc

    @classmethod
    def create(cls, abilities: 'List[Ability]', enchants: 'List[WeaponEnchantment]', source: 'Weapon'):
        return [cls(ability, enchants, source) for ability in abilities]

    @property
    def costs(self):
        return self.abil.costs

    def cost(self, character: 'Character'):
        return self.abil.cost(character)

    @property
    def classification(self):
        return self.abil.classification


class Weapon(Base):
    __tablename__ = 'weapons'

    entity_id: int = Column(Integer, primary_key=True, index=True, autoincrement=True)
    owner_id: int = Column(Integer, ForeignKey('characters.entity_id'), nullable=True)
    name: str = Column(String)
    description: str = Column(Text)
    value: int = Column(Integer, default=0)
    divinity: int = Column(Integer, default=0)
    equipped: bool = Column(Boolean, default=False)

    owner: 'Character' = relationship('Character', back_populates='weapons')
    enchantments: 'List[WeaponEnchantment]' = relationship('WeaponEnchantment', back_populates='weapon')
    abilities: 'List[Ability]' = relationship('Ability', secondary=weapon_ability_table)

    @classmethod
    def get_or_none(cls, character: 'Character', name: str) -> 'Weapon':
        return session.query(Weapon).filter(Weapon.owner == character, Weapon.name == name).one_or_none()

    def enchant(self, ability: 'Ability') -> 'WeaponEnchantment':
        enchant = WeaponEnchantment(weapon=self, ability=ability)
        session.add(enchant)
        return enchant

    def enchanted_abilities(self):
        return DummyAbility.create(self.abilities, self.enchantments, self)

    def freeze(self, frozen: 'FrozenCharacter'):
        """
        :param frozen: Character being frozen.
        :return:
        """
        frozen_self = FrozenWeapon(
            owner=frozen,
            name=self.name,
            description=self.description,
            divinity=self.divinity,
            value=self.value,
            abilities=self.abilities,
        )
        session.add(frozen_self)
        for enchant in self.enchantments:
            session.add(FrozenWeaponEnchantment(weapon=frozen_self, ability=enchant.ability))


class WeaponEnchantment(Base):
    __tablename__ = 'weapon_enchantments'

    entity_id: int = Column(Integer, primary_key=True, index=True, autoincrement=True)
    weapon_id: int = Column(Integer, ForeignKey('weapons.entity_id'))
    ability_id: int = Column(Integer, ForeignKey('abilities.entity_id'))

    weapon: 'Weapon' = relationship('Weapon', back_populates='enchantments')
    ability: 'Ability' = relationship('Ability')


armor_perk_table = Table(
    'armor_perks', Base.metadata,
    Column('armor_id', Integer, ForeignKey('armor.entity_id')),
    Column('perk_id', Integer, ForeignKey('perks.entity_id')),
)


class Armor(Base):
    __tablename__ = 'armor'

    entity_id: int = Column(Integer, primary_key=True, index=True, autoincrement=True)
    name: str = Column(String)
    divinity: int = Column(Integer, default=0)
    owner_id: int = Column(Integer, ForeignKey('characters.entity_id'))
    equipped: bool = Column(Boolean, default=False)

    owner: 'Character' = relationship('Character', back_populates='armor')
    # Note: Perks granting skills/classes will have permanent effects. Don't use those.
    perks: 'List[Perk]' = relationship('Perk', secondary=armor_perk_table)
    _attributes: 'List[ArmorAttribute]' = relationship('ArmorAttribute', back_populates='armor')

    @property
    def attributes(self) -> Dict['Attribute', int]:
        return {ar_attr.attribute: ar_attr.value for ar_attr in self._attributes}

    def freeze(self, frozen: 'FrozenCharacter'):
        session.add(FrozenArmor(
            name=self.name,
            owner=frozen,
            divinity=self.divinity,
            equipped=self.equipped,
        ))


class ArmorAttribute(Base):
    __tablename__ = 'armor_attributes'

    armor_id: int = Column(Integer, ForeignKey('armor.entity_id'), primary_key=True)
    attribute_id: int = Column(Integer, ForeignKey('attributes.entity_id'), primary_key=True)
    value: int = Column(Integer)

    armor: 'Armor' = relationship('Armor', back_populates='_attributes')
    attribute: 'Attribute' = relationship('Attribute')


frozen_weapon_ability_table = Table(
    'frozen_weapon_abilities', Base.metadata,
    Column('weapon_id', Integer, ForeignKey('frozen_weapons.entity_id')),
    Column('ability_id', Integer, ForeignKey('abilities.entity_id')),
)


class Item(Base):
    __tablename__ = 'items'

    character_id: int = Column(Integer, ForeignKey('characters.entity_id'), primary_key=True)
    name: str = Column(String, primary_key=True)
    amount: int = Column(Integer)

    character: 'Character' = relationship('Character', back_populates='items')

    def freeze(self, frozen: 'FrozenCharacter'):
        session.add(FrozenItem(character=frozen, name=self.name, amount=self.amount))


class FrozenItem(Base):
    __tablename__ = 'frozen_items'

    character_id: int = Column(Integer, ForeignKey('frozen_characters.entity_id'), primary_key=True)
    name: str = Column(String, primary_key=True)
    amount: int = Column(Integer)

    character: 'FrozenCharacter' = relationship('FrozenCharacter', back_populates='items')


class FrozenWeapon(Base):
    __tablename__ = 'frozen_weapons'

    entity_id: int = Column(Integer, primary_key=True, index=True, autoincrement=True)
    owner_id: int = Column(Integer, ForeignKey('frozen_characters.entity_id'), nullable=True)
    name: str = Column(String)
    description: str = Column(Text)
    divinity: int = Column(Integer, default=0)
    value: int = Column(Integer, default=0)

    owner: 'FrozenCharacter' = relationship('FrozenCharacter', back_populates='weapons')
    enchantments: 'FrozenWeaponEnchantment' = relationship('FrozenWeaponEnchantment', back_populates='weapon')
    abilities: 'List[Ability]' = relationship('Ability', secondary=frozen_weapon_ability_table)

    def enchanted_abilities(self):
        return DummyAbility.create(self.abilities, self.enchantments, self)


class FrozenWeaponEnchantment(Base):
    __tablename__ = 'frozen_weapon_enchantments'

    entity_id: int = Column(Integer, primary_key=True, index=True, autoincrement=True)
    weapon_id: int = Column(Integer, ForeignKey('frozen_weapons.entity_id'))
    ability_id: int = Column(Integer, ForeignKey('abilities.entity_id'))

    weapon: 'FrozenWeapon' = relationship('FrozenWeapon', back_populates='enchantments')
    ability: 'Ability' = relationship('Ability')


class FrozenArmor(Base):
    __tablename__ = 'frozen_armor'

    entity_id: int = Column(Integer, primary_key=True, index=True, autoincrement=True)
    name: str = Column(String)
    divinity: int = Column(Integer, default=0)
    owner_id: int = Column(Integer, ForeignKey('frozen_characters.entity_id'))
    equipped: bool = Column(Boolean, default=False)

    owner: 'FrozenCharacter' = relationship('FrozenCharacter', back_populates='armor')
    # Note: Perks granting skills/classes will have permanent effects. Don't use those.

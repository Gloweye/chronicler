from collections import Counter
from typing import List, Generator, TYPE_CHECKING, Counter as typing_Counter

from sqlalchemy import Column, ForeignKey, Integer, String, Boolean, Text, Float
from sqlalchemy.orm import relationship

from chronicler.models.base import Base, session
from chronicler.models.skill import CharacterSkill, FrozenSkill
from chronicler.models.class_ import CharacterClass, FrozenClass

if TYPE_CHECKING:
    from chronicler.models.attribute import Attribute, SubAttribute, AttributeDamage, FrozenAttribute
    from chronicler.models.ability import Ability, CharacterShout, FrozenShout, FrozenAbility
    from chronicler.models.perk import Perk, BonusPerk, FrozenPerk
    from chronicler.models.item import Weapon, Armor, FrozenWeapon, FrozenArmor, Item, FrozenItem
    from chronicler.models.author import Author


class Relationship(Base):
    __tablename__ = 'relationships'

    name: str = Column(String, primary_key=True)
    character_id: int = Column(Integer, ForeignKey('characters.entity_id'), primary_key=True)
    value: int = Column(Integer, default=0)
    sexual: bool = Column(Boolean, default=False, primary_key=True)

    character: 'Character' = relationship('Character', back_populates='relationships')

    def freeze(self, character: 'FrozenCharacter'):
        session.add(FrozenRelationship(character=character, name=self.name, sexual=self.sexual, value=self.value))


class FrozenRelationship(Base):
    __tablename__ = 'frozen_relationships'

    name: str = Column(String, primary_key=True)
    character_id: int = Column(Integer, ForeignKey('frozen_characters.entity_id'), primary_key=True)
    sexual: bool = Column(Boolean, default=False, primary_key=True)
    value: int = Column(Integer, default=0)

    character: 'FrozenCharacter' = relationship('FrozenCharacter', back_populates='relationships')


class Character(Base):
    __tablename__ = 'characters'

    entity_id: int = Column(Integer, primary_key=True, index=True, autoincrement=True)
    name: str = Column(String)
    divinity: int = Column(Integer, default=0)
    title: str = Column(String, nullable=True)
    author_id: int = Column(Integer, ForeignKey('authors.entity_id'))
    dragonsouls: int = Column(Integer, default=0)

    author: 'Author' = relationship('Author', back_populates='characters')
    skills: 'List[CharacterSkill]' = relationship('CharacterSkill', back_populates='character')
    classes: 'List[CharacterClass]' = relationship('CharacterClass', back_populates='character')
    bonus_perks: 'List[BonusPerk]' = relationship('BonusPerk', back_populates='character')
    damage: 'List[AttributeDamage]' = relationship('AttributeDamage', back_populates='character')
    relationships: 'List[Relationship]' = relationship('Relationship', back_populates='character')
    weapons: 'List[Weapon]' = relationship('Weapon', back_populates='owner')
    armor: 'List[Armor]' = relationship('Armor', back_populates='owner')
    instances: 'List[FrozenCharacter]' = relationship('FrozenCharacter', back_populates='character')
    titles: 'List[CharacterTitle]' = relationship('CharacterTitle', back_populates='character')
    shouts: 'List[CharacterShout]' = relationship('CharacterShout', back_populates='character')
    aliases: 'List[CharacterAlias]' = relationship('CharacterAlias', back_populates='character')
    items: 'List[Item]' = relationship('Item', back_populates='character')

    def freeze(self, bookmark_name: str):
        new = FrozenCharacter(name=self.name, identifier=bookmark_name, divinity=self.divinity, title=self.title, author_id=self.author_id,
                              dragonsouls=self.dragonsouls, level=self.level, character=self)
        session.add(new)
        for char_skill in self.skills:
            char_skill.freeze(new)

        for char_class in self.classes:
            char_class.freeze(new)

        for perk in self.perks:
            perk.freeze(new)

        for attribute, value in self.attributes.items():
            attribute.freeze(new, value)

        for relationship_ in self.relationships:
            relationship_.freeze(new)

        for char_shout in self.shouts:
            char_shout.freeze(new)

        for weapon in self.weapons:
            weapon.freeze(new)

        for armor in self.armor:
            armor.freeze(new)

        for item in self.items:
            item.freeze(new)

        for title in self.available_titles:
            session.add(FrozenTitle(character=new, title=title))

        session.commit()

    @property
    def level(self):
        return sum(char_class.level for char_class in self.classes)

    @property
    def full_name(self):
        if self.title is None:
            return self.name

        return f'{self.name} the {self.title}'

    def process_unlocks(self):
        """Adds all indirectly gained skills and classes."""
        old_skills = [chr_skill.skill for chr_skill in self.skills]
        old_classes = [chr_cls.class_ for chr_cls in self.classes]
        changes = []
        for char_skill in self.skills:
            for unlock in char_skill.unlocks:
                if unlock.skill_id is not None and unlock.skill not in old_skills:
                    changes.append(CharacterSkill(character=self, skill=unlock.skill))
                elif unlock.class_id is not None and unlock.skill not in old_classes:
                    changes.append(CharacterClass(character=self, class_=unlock.class_))

        for char_class in self.classes:
            for unlock in char_class.unlocks:
                if unlock.skill_id is not None and unlock.skill not in old_skills:
                    changes.append(CharacterSkill(character=self, skill=unlock.skill))
                elif unlock.class_id is not None and unlock.skill not in old_classes:
                    changes.append(CharacterClass(character=self, class_=unlock.class_))

        for perk in self.perks:
            for unlock in perk.unlocks:
                if unlock.skill_id is not None and unlock.skill not in old_skills:
                    changes.append(CharacterSkill(character=self, skill=unlock.skill))
                if unlock.class_id is not None and unlock.skill not in old_classes:
                    changes.append(CharacterClass(character=self, class_=unlock.class_))

        if changes:
            for new in changes:
                session.add(new)
            session.commit()
            for new in changes:
                session.refresh(new)
            self.process_unlocks()

    @property
    def perks(self) -> Generator['Perk', None, None]:
        for bonus_perk in self.bonus_perks:
            yield bonus_perk.perk

        for char_skill in self.skills:
            for unlock in char_skill.unlocks:
                if unlock.perk is not None:
                    yield unlock.perk

        for char_class in self.classes:
            for unlock in char_class.unlocks:
                if unlock.perk is not None:
                    yield unlock.perk

        for armor in self.armor:
            if armor.equipped:
                yield from armor.perks

    @property
    def abilities(self) -> Generator['Ability', None, None]:
        for char_skill in self.skills:
            for unlock in char_skill.unlocks:
                if unlock.ability is not None:
                    yield unlock.ability

        for char_class in self.classes:
            for unlock in char_class.unlocks:
                if unlock.ability is not None:
                    yield unlock.ability

        for perk in self.perks:
            for unlock in perk.unlocks:
                if unlock.ability is not None:
                    yield unlock.ability

        for weapon in self.weapons:
            if weapon.equipped:
                yield from weapon.enchanted_abilities()

    @property
    def attributes(self) -> 'typing_Counter[Attribute]':
        """Calculates the current values for all attributes."""
        attributes: 'typing_Counter[Attribute]' = Counter()
        for char_skill in self.skills:
            for skill_attr, value in char_skill.attributes.items():
                attributes[skill_attr] += value

        for char_class in self.classes:
            for attribute, value in char_class.attributes.items():
                attributes[attribute] += value

        for perk in self.perks:
            for perk_attr in perk.attributes:
                attributes[perk_attr.attribute] += perk_attr.value

        for armor in self.armor:
            if armor.equipped:
                for attr, value in armor.attributes.items():
                    attributes[attr] += value

        for attr, value in list(attributes.items()):
            # This is very naive and has undefined behaviour when an attribute that is increased by another attribute while also itself increasing one.
            # It may or may not actually boost with the higher value, which depends on dict ordering.
            # Instead of building up boost trees or something asinine like that, I just outlaw chaining boosts.
            # Which will actually be outlawed in the endpoint. Probably.
            for sub_attr in attr.attributes:  # type: SubAttribute
                attributes[sub_attr.child] += value * sub_attr.multiplier

        for attr in attributes:
            attributes[attr] = int(attributes[attr] * (self.dragonsouls * attr.dragon_soul_mod + 1))

        return attributes

    @property
    def available_titles(self) -> Generator[str, None, None]:
        for char_class in self.classes:
            yield from char_class.titles

        for char_title in self.titles:
            yield char_title.title

    @staticmethod
    def get_by_name(name: str) -> 'Character':
        att = session.query(Character).filter(Character.name == name).one_or_none()
        if att is not None:
            return att

        return session.query(CharacterAlias).filter(CharacterAlias.name == name).one().character


class CharacterAlias(Base):
    __tablename__ = 'character_alias'

    character_id: int = Column(Integer, ForeignKey('characters.entity_id'))
    name: str = Column(String, primary_key=True)

    character = relationship('Character', back_populates='aliases')


class CharacterTitle(Base):
    __tablename__ = 'character_titles'
    entity_id: int = Column(Integer, primary_key=True, index=True, autoincrement=True)
    character_id: int = Column(Integer, ForeignKey('characters.entity_id'))
    title: str = Column(String)

    character = relationship('Character', back_populates='titles')


class NPC(Base):
    __tablename__ = 'npc_characters'

    entity_id: int = Column(Integer, primary_key=True, index=True, autoincrement=True)
    name: str = Column(String)
    title: str = Column(String, nullable=True)
    description: str = Column(Text)
    divinity: str = Column(Integer, default=0)
    author_id: int = Column(Integer, ForeignKey('authors.entity_id'))
    dragonsouls: int = Column(Integer, default=0)
    level: int = Column(Integer)
    perks: 'List[NPCPerk]' = relationship('NPCPerk', back_populates='npc')
    attr_per_level: 'List[NPCAttr]' = relationship('NPCAttr', back_populates='npc')

    @property
    def attributes(self) -> 'typing_Counter[Attribute]':
        attributes: 'typing_Counter[Attribute]' = Counter()
        for npc_attr in self.attr_per_level:
            attributes[npc_attr.attribute] += npc_attr.value * (self.level if npc_attr.per_level else 1)

        for npc_perk in self.perks:
            if self.level >= npc_perk.level:
                for perk_attr in npc_perk.perk.attributes:
                    attributes[perk_attr.attribute] += perk_attr.value

        for attr, value in list(attributes.items()):
            # This is very naive and has undefined behaviour when an attribute that is increased by another attribute while also itself increasing one.
            # It may or may not actually boost with the higher value, which depends on dict ordering.
            # Instead of building up boost trees or something asinine like that, I just outlaw chaining boosts.
            # Which will actually be outlawed in the endpoint. Probably.
            for sub_attr in attr.attributes:  # type: SubAttribute
                attributes[sub_attr.child] += value * sub_attr.multiplier

        return attributes

    @property
    def abilities(self) -> Generator['Ability', None, None]:
        for npc_perk in self.perks:
            if self.level >= npc_perk.level:
                for unlock in npc_perk.perk.unlocks:
                    if unlock.ability is not None:
                        yield unlock.ability


class NPCPerk(Base):
    __tablename__ = 'npc_perks'

    npc_id: int = Column(Integer, ForeignKey('npc_characters.entity_id'), primary_key=True)
    perk_id: int = Column(Integer, ForeignKey('perks.entity_id'), primary_key=True)
    level: int = Column(Integer, default=0)

    npc: 'NPC' = relationship('NPC', back_populates='perks')
    perk: 'Perk' = relationship('Perk')


class NPCAttr(Base):
    __tablename__ = 'npc_attributes'

    npc_id: int = Column(Integer, ForeignKey('npc_characters.entity_id'), primary_key=True)
    attribute_id: int = Column(Integer, ForeignKey('attributes.entity_id'), primary_key=True)
    value: float = Column(Float)
    per_level: bool = Column(Boolean, default=False, primary_key=True)

    npc: 'NPC' = relationship('NPC', back_populates='attr_per_level')
    attribute = relationship('Attribute')


class AttributeHolder:
    def __init__(self, attributes):
        self.attributes = attributes


class FrozenTitle(Base):
    __tablename__ = 'frozen_titles'
    entity_id: int = Column(Integer, primary_key=True, index=True, autoincrement=True)
    character_id: int = Column(Integer, ForeignKey('frozen_characters.entity_id'))
    title: str = Column(String)

    character = relationship('FrozenCharacter', back_populates='titles')


class FrozenCharacter(Base):
    __tablename__ = 'frozen_characters'

    entity_id: int = Column(Integer, primary_key=True, index=True, autoincrement=True)
    character_id: int = Column(Integer, ForeignKey('characters.entity_id'))
    name: str = Column(String)
    identifier: str = Column(String)  # probably just gonna be chapter number.
    divinity: int = Column(Integer, default=0)
    title: str = Column(String, default='Gamer')
    author_id: int = Column(Integer, ForeignKey('authors.entity_id'))
    dragonsouls: int = Column(Integer, default=0)
    level: int = Column(Integer)

    classes: 'List[FrozenClass]' = relationship('FrozenClass', back_populates='character')
    skills: 'List[FrozenSkill]' = relationship('FrozenSkill', back_populates='character')
    _attributes: 'List[FrozenAttribute]' = relationship('FrozenAttribute', back_populates='character')
    perks: 'List[FrozenPerk]' = relationship('FrozenPerk', back_populates='character')
    relationships: 'List[FrozenRelationship]' = relationship('FrozenRelationship', back_populates='character')
    weapons: 'List[FrozenWeapon]' = relationship('FrozenWeapon', back_populates='owner')
    armor: 'List[FrozenArmor]' = relationship('FrozenArmor', back_populates='owner')
    shouts: 'List[FrozenShout]' = relationship('FrozenShout', back_populates='character')
    abilities: 'List[FrozenAbility]' = relationship('FrozenAbility', back_populates='character')
    character: 'Character' = relationship('Character', back_populates='instances')
    items: 'List[FrozenItem]' = relationship('FrozenItem', back_populates='character')
    titles: 'List[FrozenTitle]' = relationship('FrozenTitle', back_populates='character')

    @property
    def full_name(self):
        if self.title is None:
            return self.name

        return f'{self.name} the {self.title}'

    @property
    def attributes(self) -> AttributeHolder:
        return AttributeHolder(Counter((attr.attribute, attr.value) for attr in self._attributes))

    @property
    def attribute_objects(self) -> List['FrozenAttribute']:
        return self._attributes

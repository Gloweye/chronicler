#! /usr/bin/python3
"""
CLI Entrypoint for Chronicler
"""
import sys
from asyncio import get_event_loop
from pprint import pprint

from chronicler.discord.message import Message
from chronicler.discord.user import User
from chronicler.endpoints.parser import create_parser
from chronicler.models.base import finalize

if __name__ == '__main__':
    finalize()
    message = Message(
        guild_id=1,
        channel_id=1,
        author=User(id=410886004677804042),  # Admin access because it now thinks it's me.
        content='',
        timestamp='',
        tts=False,
        mention_everyone=False,
        mentions=[],
        mention_roles=[],
        type=1,
        pinned=False,
    )
    parser = create_parser()
    args = parser.parse_args(args=sys.argv[1:], message=message, skip_checks=True)
    result = get_event_loop().run_until_complete(parser.func(*vars(args).pop('POSITIONALS'), **vars(args)))
    if 'content' in result:
        pprint(result['content'])
    else:
        print(result['embed'].json(indent=2))

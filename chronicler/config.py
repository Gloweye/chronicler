from configparser import ConfigParser
from pathlib import Path

config = ConfigParser()
config.read(Path.home() / '.config' / 'chronicler.ini')

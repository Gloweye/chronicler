"""Manage Classes"""

from logging import getLogger
from typing import Optional

from sqlalchemy.orm.exc import NoResultFound

from chronicler.discord.embed import Embed, EmbedField, EmbedFooter
from chronicler.models.ability import Ability
from chronicler.models.attribute import Attribute
from chronicler.models.class_ import Class, ClassInfo, ClassAttribute, ClassUnlock
from chronicler.models.perk import Perk
from chronicler.models.skill import Skill
from chronicler.parser import SubParser
from chronicler.models.base import session
from chronicler.parser import verify_author_and_channel, verify_open_channel
from chronicler.tools import class_by_name_or_id

logger = getLogger(__name__)


def add_class_parsers(subparsers: SubParser):
    parser = subparsers.add_parser('class', description='Functions for managing classes.', aliases=['classes', 'Classes', 'Class'])
    sub: SubParser = parser.add_subparsers()
    for func in [create, info_, view, attribute_, unlock, list_, edit]:
        sub.add_parser_from_function(func)


@verify_open_channel
async def list_(*args, filter_: Optional[str]):
    """
    Lists all classes.
    :param filter_: Case-insensitive filtration of names.
    """
    page = int(args[0])-1 if args else 0
    items_per_page = 20
    num_classes = session.query(Class).filter(Class.name.like(f'%{filter_}%')).count() if filter_ else session.query(Class).count()
    total_pages = max((num_classes // items_per_page) + ((num_classes % items_per_page) != 0), 1)  # Don't need to add 1 if it divides exactly.
    if 0 > page or page >= total_pages:
        return {'content': f'Invalid page ID. Please use 1 - {total_pages}'}

    query = session.query(Class)
    if filter_:
        query = query.filter(Class.name.like(f'%{filter_}%'))

    classes = query.offset(page * items_per_page).limit(items_per_page).all()

    embed = Embed(
        title='Class List',
        description=f'Filtering on: {filter_} (case-insensitive)' if filter_ else 'No filter used.',
        footer=EmbedFooter(text=f'Page: {page+1}/{total_pages}'),
        fields_=[]
    )
    for class_ in classes:
        embed.fields_.append(EmbedField(
            name=class_.name_fallback,
            value='\n'.join(f' - {cls_info.level}: {cls_info.name}' for cls_info in sorted(class_.info, key=lambda cls_info: cls_info.level))
        ))

    return {'embed': embed}


@verify_author_and_channel
async def unlock(
        parent: Class,
        level: int,
        class_: Optional[Class] = None,
        skill: Optional[Skill] = None,
        perk: Optional[Perk] = None,
        ability: Optional[Ability] = None
):
    """
    Adds an unlock to a class.
    :param parent: Class which gets a new boost on unlock.
    :param level: What level the class needs to be to grant the unlock.
    :param class_: Class to unlock, if any.
    :param skill: Skill to unlock, if any.
    :param perk: Perk to unlock, if any.
    :param ability: Abilities to unlock.
    """
    kwargs = {'parent': parent, 'level': level}
    if class_ is not None:
        kwargs['class'] = class_
    if skill is not None:
        kwargs['skill'] = skill
    if perk is not None:
        kwargs['perk'] = perk
    if ability is not None:
        kwargs['ability'] = ability

    session.add(ClassUnlock(**kwargs))
    session.commit()
    return {'content': f'Successfully added unlock to {parent.name_fallback}'}


@verify_open_channel
async def view(class_: Class):
    """
    Shows details of a class
    :param class_: Name of the class.
    """
    embed = Embed(
        title=class_.name_fallback,
        description=class_.description_fallback + f'\nSkill levels required to level up {class_.name_fallback}: {class_.skill_level_ratio}',
        fields_=[]
    )
    if len(class_.info) > 1:
        embed.fields_.append(EmbedField(
            name='Titles',
            value='\n'.join(f' - **Level {info.level}:**\n{info.description}' for info in class_.info if info.level != 0)
        ))
    if class_.attributes:
        embed.fields_.append(EmbedField(
            name='Attributes per level',
            value='\n'.join(f' - {cls_attr.attribute.name}: {cls_attr.multiplier}' for cls_attr in class_.attributes)
        ))
    if class_.skills:
        embed.fields_.append(EmbedField(
            name='Skills',
            value='\n'.join(f' - {skill.name}' for skill in class_.skills)
        ))
    if class_.unlocks:
        classes, skills, abilities, perks = [], [], [], []
        for class_unlock in class_.unlocks:
            if class_unlock.class_id is not None:
                classes.append(class_unlock)
            if class_unlock.skill_id is not None:
                skills.append(class_unlock)
            if class_unlock.perk_id is not None:
                perks.append(class_unlock)
            if class_unlock.ability_id is not None:
                abilities.append(class_unlock)

        if classes:
            embed.fields_.append(EmbedField(
                name='Classes Granted',
                value='\n'.join(f' - {unlock_.level: <3} {unlock_.class_.name}' for unlock_ in classes)
            ))
        if skills:
            embed.fields_.append(EmbedField(
                name='Skills Granted',
                value='\n'.join(f' - {unlock_.level: <3} {unlock_.skill.name}' for unlock_ in skills)
            ))
        if perks:
            embed.fields_.append(EmbedField(
                name='Perks Granted',
                value='\n'.join(f' - {unlock_.level: <3} {unlock_.perk.name}' for unlock_ in perks)
            ))
        if abilities:
            embed.fields_.append(EmbedField(
                name='Abilities Granted',
                value='\n'.join(f' - {unlock_.level: <3} {unlock_.ability.name}' for unlock_ in abilities)
            ))

    return {'embed': embed}


@verify_author_and_channel
async def create(name: str, description: str, level_max: Optional[int], skill_level_ratio: Optional[int]):
    """
    Creates a new class.
    :param name: Class name.
    :param description: Class Description.
    :param level_max: Maximum level the class can be.
    :param skill_level_ratio: How many skill levels are required for a single class level.
    """
    try:
        class_by_name_or_id(name)
        return {'content': 'A Class with this name already exists.'}
    except NoResultFound:
        pass

    class_ = Class(level_max=level_max, skill_level_ratio=skill_level_ratio)
    session.add(class_)
    class_info = ClassInfo(name=name, description=description, level=0, class_=class_)
    session.add(class_info)
    session.commit()
    return {'content': f'Successfully created a class named {name}'}


@verify_author_and_channel
async def edit(
        class_: Class,
        level_max: Optional[int],
        skill_level_ratio: Optional[int],
):
    """
    Edits a class.
    :param class_: Class to edit.
    :param level_max: Set new max level.
    :param skill_level_ratio: Set new skill/class level ratio.
    """
    lines = []
    if level_max is not None:
        lines.append(f'Max level of class {class_.name_fallback} is now {level_max}')
        class_.level_max = level_max

    if skill_level_ratio is not None:
        lines.append(f'Class {class_.name_fallback} now gains a level every {skill_level_ratio} skill levels.')
        class_.skill_level_ratio = skill_level_ratio

    session.commit()
    return {'content': '\n'.join(lines)}


@verify_author_and_channel
async def attribute_(class_: Class, attribute: Attribute, multiplier: float):
    """
    Adds an attribute to a class.
    :param class_: Name of the Class
    :param attribute: Name fo the Attribute
    :param multiplier: How much the attribute should increase for every class level.
    """
    cls_attr = session.query(ClassAttribute).filter(
        ClassAttribute.class_id == class_.entity_id,
        ClassAttribute.attribute_id == attribute.entity_id
    ).one_or_none()
    if cls_attr is None:
        session.add(ClassAttribute(class_=class_, attribute=attribute, multiplier=multiplier))
    else:
        cls_attr.multiplier = multiplier
    session.commit()
    return {'content': f'Class {class_.name_fallback} now increases {attribute.name} by {multiplier} per level.'}


@verify_author_and_channel
async def info_(class_: Class, level: int, name: Optional[str], description: Optional[str]):
    """
    Adds info to a class. This can allow a class' name and description to update as your level increases.
    :param class_: Level 0 class name to add the info to.
    :param name: Class name at the level.
    :param level: Level at which this new info kicks in. For characters, the highest level they've reached is shown.
    :param description: Description of the class level. Optional. If empty, the highest level not empty is shown.
    """
    for inf in class_.info:
        if inf.level == level:
            if description:
                inf.description = description
            if name:
                inf.name = name
            break
    else:
        session.add(ClassInfo(level=level, class_=class_, name=name, description=description))
    session.commit()
    return {'content': f'Class info {name} updated/added to class {class_.name_fallback}'}

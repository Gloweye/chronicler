"""Manage Classifications"""

from logging import getLogger
from typing import Optional

from chronicler.discord.embed import Embed, EmbedField, EmbedFooter
from chronicler.discord.emoji import Emoji
from chronicler.models.attribute import Attribute
from chronicler.parser import SubParser
from chronicler.models.base import session
from chronicler.models.ability import Classification
from chronicler.parser import verify_author_and_channel, verify_open_channel

logger = getLogger(__name__)


def add_classification_parsers(subparsers: SubParser):
    parser = subparsers.add_parser('classification', description='Functions for managing Classifications.',
                                   aliases=['Classification', 'classifications', 'Classifications'])
    sub: SubParser = parser.add_subparsers()
    for func in [create, view, list_, edit]:
        sub.add_parser_from_function(func)


@verify_open_channel
async def view(classification: Classification):
    """
    Shows a short overview of a classification.
    :param classification: Classification to inspect
    """
    attrs = {
        "This attribute decreases damage taken from abilities with this classification.": classification.resistor,
        "This attribute increases the magnitude of abilities with this classification.": classification.magnifier,
        "This attribute increases the duration of abilities with this classification.": classification.extender,
        "When using an ability with this classification, the resistance of the target is lowered by the value of this attribute.": classification.piercer,
        "This attribute reduces the cost for using abilities with this classification.": classification.reductor,
        "This attribute allows you to attack more often with abilities belonging to this classification.": classification.speeder,
        "This attribute allows abilities belonging to this classification to affect a greater area.": classification.expander,
    }
    embed = Embed(
        title=f'{Emoji(id=classification.emoji)} {classification.name}' if classification.emoji is not None else classification.name,
        fields_=[],
    )
    for key, value in attrs.items():
        if value is None:
            continue
        embed.fields_.append(EmbedField(name=value.name, value=key))

    return {'embed': embed}


@verify_open_channel
async def list_(*args, filter_: str = ''):
    """
    Shows a list of all classification.
    :param filter_: Exclude results not like %this%.
    """
    page = int(args[0])-1 if args else 0
    items_per_page = 10
    num_classs = session.query(Classification).filter(Attribute.name.like(f'%{filter_}%')).count() if filter_ else session.query(Classification).count()
    total_pages = max((num_classs // items_per_page) + ((num_classs % items_per_page) != 0), 1)  # Don't need to add 1 if it divides exactly.
    if 0 > page or page >= total_pages:
        return {'content': f'Invalid page ID. Please use 1 - {total_pages}'}

    query = session.query(Classification)
    if filter_:
        query = query.filter(Classification.name.like(f'%{filter_}%'))

    classifications = query.offset(page * items_per_page).limit(items_per_page).all()
    embed = Embed(
        title='Classification List',
        description='Lists all classifications.',
        fields_=[],
        footer=EmbedFooter(text=f'Page: {page + 1}/{total_pages}')
    )
    for classification in classifications:
        attrs = {
            "Resistance": classification.resistor,
            "Magnitude Increase": classification.magnifier,
            "Duration Extension": classification.extender,
            "Resistance Penetration": classification.piercer,
            "Cost Reduction": classification.reductor,
            "Attack Speed Increase": classification.speeder,
            "Area Of Effect Increase": classification.expander,
        }
        embed.fields_.append(EmbedField(
            name=f'{Emoji(id=classification.emoji)} {classification.name}' if classification.emoji is not None else classification.name,
            value='\n'.join(f'{key}: {value.name}' for key, value in attrs.items() if value is not None) or 'Placeholder'
        ))
    return {'embed': embed}


@verify_author_and_channel
async def edit(
        classification: Classification,
        emoji: Optional[Emoji],
        name: Optional[str],
        resistor: Optional[Attribute],
        magnifier: Optional[Attribute],
        extender: Optional[Attribute],
        piercer: Optional[Attribute],
        reductor: Optional[Attribute],
        speeder: Optional[Attribute],
        expander: Optional[Attribute],
):
    """
    Creates a new Classification for abilities.
    :param classification: Which classification to edit.
    :param emoji: Optional Emoji to represent the classification.
    :param name: New name for this classification
    :param resistor: What Attribute should resist this damage type.
    :param magnifier: What Attribute should Increase this damage type.
    :param extender: What Attribute should extend durations of this type.
    :param piercer: What Attribute should lower target resistance of this type.
    :param reductor: What Attribute should reduces costs of this type.
    :param speeder: What Attribute should raise attack speed for this type.
    :param expander: What Attribute should increase the area of effect for this type.
    """
    lines = [f'Updated Classification {classification.name}']
    if emoji is not None:
        lines.append(f'Changed Emote to {emoji}')
        classification.emoji = int(emoji.id)

    if name is not None:
        lines.append(f'Renamed {classification.name} to {name}')
        classification.name = name

    if resistor is not None:
        lines.append(f'Changed Resistor to {resistor.name}')
        classification.resistor = resistor

    if magnifier is not None:
        lines.append(f'Charnged Magnifier to {magnifier.name}')
        classification.magnifier = magnifier

    if extender is not None:
        lines.append(f'Charnged Extender to {extender.name}')
        classification.extender = extender

    if piercer is not None:
        lines.append(f'Charnged Piercer to {piercer.name}')
        classification.piercer = piercer

    if reductor is not None:
        lines.append(f'Charnged Reductor to {reductor.name}')
        classification.reductor = reductor

    if speeder is not None:
        lines.append(f'Charnged Speeder to {speeder.name}')
        classification.speeder = speeder

    if expander is not None:
        lines.append(f'Charnged Expander to {expander.name}')
        classification.expander = expander

    session.commit()
    return {'content': '\n'.join(lines)}


@verify_author_and_channel
async def create(
        name: str, emoji: Optional[Emoji],
        resistor: Optional[Attribute],
        magnifier: Optional[Attribute],
        extender: Optional[Attribute],
        piercer: Optional[Attribute],
        reductor: Optional[Attribute],
        speeder: Optional[Attribute],
        expander: Optional[Attribute],
):
    """
    Creates a new Classification for abilities.
    :param name: How to name it. For example, "Fire"
    :param emoji: Optional Emoji to represent the classification.
    :param resistor: What Attribute should resist this damage type.
    :param magnifier: What Attribute should Increase this damage type.
    :param extender: What Attribute should extend durations of this type.
    :param piercer: What Attribute should lower target resistance of this type.
    :param reductor: What Attribute should reduces costs of this type.
    :param speeder: What Attribute should raise attack speed for this type.
    :param expander: What Attribute should increase the area of effect for this type.
    """
    classification = Classification(name=name)
    session.add(classification)

    if emoji is not None:
        classification.emoji = int(emoji.id)

    if resistor is not None:
        classification.resistor = resistor

    if magnifier is not None:
        classification.magnifier = magnifier

    if extender is not None:
        classification.extender = extender

    if piercer is not None:
        classification.piercer = piercer

    if reductor is not None:
        classification.reductor = reductor

    if speeder is not None:
        classification.speeder = speeder

    if expander is not None:
        classification.expander = expander

    session.commit()
    return {'content': f'Succesfully created the classification {classification.name}'}

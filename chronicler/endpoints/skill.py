"""Manage Skills"""

from logging import getLogger
from typing import Optional

from chronicler.discord.embed import Embed, EmbedField, EmbedFooter
from chronicler.models.ability import Ability
from chronicler.models.attribute import Attribute
from chronicler.models.class_ import Class
from chronicler.models.perk import Perk
from chronicler.parser import SubParser
from chronicler.models.base import session
from chronicler.models.skill import Skill, SkillUnlock, SkillAttribute
from chronicler.parser import verify_author_and_channel, verify_open_channel

logger = getLogger(__name__)


def add_skill_parsers(subparsers: SubParser):
    parser = subparsers.add_parser('skill', description='Functions for managing skills.', aliases=['skills', 'Skills', 'Skill'])
    sub: SubParser = parser.add_subparsers()
    for func in [create, view, unlock, attribute_, list_, edit]:
        sub.add_parser_from_function(func)


@verify_open_channel
async def list_(*args, filter_: Optional[str]):
    """
    Lists all perks.
    :param filter_: Case-insensitive filtration of names.
    """
    page = int(args[0])-1 if args else 0
    items_per_page = 20
    num_skills = session.query(Skill).filter(Skill.name.like(f'%{filter_}%')).count() if filter_ else session.query(Skill).count()
    total_pages = max((num_skills // items_per_page) + ((num_skills % items_per_page) != 0), 1)  # Don't need to add 1 if it divides exactly.
    if 0 > page or page >= total_pages:
        return {'content': f'Invalid page ID. Please use 1 - {total_pages}'}

    query = session.query(Skill)
    if filter_:
        query = query.filter(Skill.name.like(f'%{filter_}%'))

    skills = query.offset(page * items_per_page).limit(items_per_page).all()

    embed = Embed(
        title='Skills',
        description='\n'.join(f' - **{skill.name}**: {skill.description}' for skill in skills),
        footer=EmbedFooter(text=f'Page: {page+1}/{total_pages}')
    )
    return {'embed': embed}


@verify_author_and_channel
async def attribute_(skill: Skill, attribute: Attribute, multiplier: float):
    """
    Adds an attribute to a skill.
    :param skill: Name of the Skill
    :param attribute: Name of the Attribute
    :param multiplier: How much the attribute should increase for every skill level.
    """
    skill_attr = session.query(SkillAttribute).filter(SkillAttribute.skill == skill, SkillAttribute.attribute == attribute).one_or_none()
    if skill_attr is None and multiplier != 0:
        session.add(SkillAttribute(skill=skill, attribute=attribute, multiplier=multiplier))
    elif skill_attr is None:
        return {'content': 'Setting a non-existent attribute with value 0 is nonsensical.'}
    elif multiplier == 0:
        session.delete(skill_attr)
        session.commit()
        return {'content': f'Skill {skill.name} no longer improves {attribute.name}'}
    else:
        skill_attr.multiplier = multiplier
    session.commit()
    return {'content': f'Skill {skill.name} now increases {attribute.name} by {multiplier} per level.'}


@verify_author_and_channel
async def unlock(
        parent: Skill,
        level: int,
        class_: Optional[Class] = None,
        skill: Optional[Skill] = None,
        perk: Optional[Perk] = None,
        ability: Optional[Ability] = None
):
    """
    Adds an unlock to a skill.
    :param parent: Skill which gets a new boost on unlock.
    :param level: What level the class needs to be to grant the unlock.
    :param class_: Class to unlock, if any.
    :param skill: Skill to unlock, if any.
    :param perk: Perk to unlock, if any.
    :param ability: Abilities to unlock.
    """
    kwargs = {'parent': parent, 'level': level}
    if class_ is not None:
        kwargs['class'] = class_
    if skill is not None:
        kwargs['skill'] = skill
    if perk is not None:
        kwargs['perk'] = perk
    if ability is not None:
        kwargs['ability'] = ability

    session.add(SkillUnlock(**kwargs))
    session.commit()
    return {'content': f'Successfully added unlock to {parent.name}'}


@verify_open_channel
async def view(skill: Skill):
    """
    Shows details of a skill
    :param skill: Name of the skill.
    """
    embed = Embed(
        title=skill.name,
        description=skill.description,
        fields_=[]
    )
    if skill.level_max is not None:
        embed.fields_.append(EmbedField(
            name='Maximum Level',
            value=f'{skill.level_max}'
        ))

    if skill.class_id:
        embed.fields_.append(EmbedField(
            name='Class',
            value=f'{skill.class_.name_fallback}'
        ))

    if skill.attributes:
        embed.fields_.append(EmbedField(
            name='Attributes per level',
            value='\n'.join(f'- {skill_attr.attribute.name}: {skill_attr.multiplier}' for skill_attr in skill.attributes)
        ))

    if skill.unlocks:
        classes, skills, abilities, perks = [], [], [], []
        for skill_unlock in skill.unlocks:
            if skill_unlock.class_id is not None:
                classes.append(skill_unlock)
            if skill_unlock.skill_id is not None:
                skills.append(skill_unlock)
            if skill_unlock.perk_id is not None:
                perks.append(skill_unlock)
            if skill_unlock.ability_id is not None:
                abilities.append(skill_unlock)

        if classes:
            embed.fields_.append(EmbedField(
                name='Classes Granted',
                value='\n'.join(f'- {unlock_.level: <3} {unlock_.class_.name}' for unlock_ in sorted(classes, key=lambda item: item.level))
            ))
        if skills:
            embed.fields_.append(EmbedField(
                name='Skills Granted',
                value='\n'.join(f'- {unlock_.level: <3} {unlock_.skill.name}' for unlock_ in sorted(skills, key=lambda item: item.level))
            ))
        if perks:
            embed.fields_.append(EmbedField(
                name='Perks Granted',
                value='\n'.join(f'- {unlock_.level: <3} {unlock_.perk.name}' for unlock_ in sorted(perks, key=lambda item: item.level))
            ))
        if abilities:
            embed.fields_.append(EmbedField(
                name='Abilities Granted',
                value='\n'.join(f'- {unlock_.level: <3} {unlock_.ability.name}' for unlock_ in sorted(abilities, key=lambda item: item.level))
            ))

    return {'embed': embed}


@verify_author_and_channel
async def edit(
        skill: Skill,
        description: Optional[str],
        level_max: Optional[int],
        class_: Optional[Class],
        name: Optional[str],
        remove_class: bool,
        remove_level_cap: bool,
):
    """
    Edits a Skill
    :param skill: Skill to edit. Use old name if renaming.
    :param description: New description
    :param level_max: New Maximum Level
    :param class_: New Class to govern this skill
    :param name: New Skill name.
    :param remove_class: Ignore the class parameter, and remove it instead.
    :param remove_level_cap: Ignore level-max parameter, and remove maximum level instead.
    """
    changes = []
    if description is not None:
        changes.append(f'Skill description changed to "{description}"')
        skill.description = description

    if level_max is not None and not remove_level_cap:
        changes.append(f'Maximum Skill Level changed to "{level_max}"')
        skill.level_max = level_max
    elif remove_level_cap:
        changes.append('Maximum skill level removed.')
        skill.level_max = None

    if class_ is not None and not remove_class:
        changes.append(f'Governing class changed to  "{class_.name_fallback}"')
        skill.class_ = class_
    elif remove_class:
        changes.append('Skill is no longer governed by a class.')

    if name is not None:
        changes.append(f'Skill name changed to "{name}"')
        skill.name = name

    session.commit()
    return {'content': '\n'.join(changes)}


@verify_author_and_channel
async def create(name: str, description: str, level_max: Optional[int], class_: Optional[Class] = None):
    """
    Creates a new skill, optionally linked to a certain class.
    :param name: Name of the skill
    :param description: Short description of the skill
    :param level_max: Maximum level the skill can get
    :param class_: Class under which the skill is categorized
    """
    if session.query(Skill).filter(Skill.name == name).one_or_none() is not None:
        return {'content': f'A Skill named {name} already exists.'}

    skill = Skill(
        name=name,
        description=description,
        level_max=level_max,
        class_=class_
    )
    session.add(skill)
    session.commit()
    return view(skill)

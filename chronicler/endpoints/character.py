"""Manage NPCs"""

from logging import getLogger
from typing import Optional, List, Dict, TYPE_CHECKING

from sqlalchemy import desc

from chronicler.discord.embed import Embed, EmbedField, EmbedFooter
from chronicler.models.ability import Shout, CharacterShout, Ability
from chronicler.models.attribute import Attribute
from chronicler.models.author import Author
from chronicler.models.character import Character, Relationship, CharacterAlias
from chronicler.models.class_ import Class, CharacterClass
from chronicler.models.item import Weapon, Item, Armor, ArmorAttribute
from chronicler.models.perk import Perk, BonusPerk
from chronicler.models.skill import Skill, CharacterSkill
from chronicler.parser import SubParser, FromMessage
from chronicler.models.base import session
from chronicler.parser import verify_author_and_channel, verify_open_channel, verify, check_channel_open, check_author_and_channel
from chronicler.tools import chunks

if TYPE_CHECKING:
    from argparse import Namespace
    from chronicler.discord.message import Message

logger = getLogger(__name__)


def add_character_parsers(subparsers: SubParser):
    parser = subparsers.add_parser('character', description='Functions for managing Characters.',
                                   aliases=['Character', 'characters', 'Characters', 'char', 'Char'])
    sub: SubParser = parser.add_subparsers()
    for func in [create, edit, perk_, view, list_, perks_, attributes_, abilities_, classes_, skills_, shouts_, class__, skill_, shout_, relationship,
                 relationships_, weapon_, enchant_, weapons_, item_, inventory, armor_, ]:
        sub.add_parser_from_function(func)


@verify_open_channel
async def inventory(character: Character, *args, filter_: Optional[str]):
    """
    Shows all items the character has. Does not display weapons or armor.
    :param character: Whose inventory to view
    :param args: Page number.
    :param filter_: Filter by item name.
    """
    page = int(args[0])-1 if args else 0
    items_per_page = 10
    items = list(character.items)
    num_items = len(items)
    pages, remainder = divmod(num_items, items_per_page)
    total_pages = (pages + remainder != 0) or 1  # Don't need to add 1 if it divides exactly.
    if 0 > page or page >= total_pages:
        return {'content': f'Invalid page ID. Please use 1 - {total_pages}'}

    if filter_:
        items = [item for item in items if filter_ in item.name]

    items = items[page * items_per_page:(1 + page) * items_per_page]

    embed = Embed(
        title=f'Inventory of {character.full_name}',
        description='\n'.join(f'{item.name}: {item.amount}' for item in items),
    )
    return {'embed': embed}


@verify_open_channel
async def classes_(character: Character, *args, filter_: Optional[str]):
    """
    Displays all of a character's classes.
    :param character: Character to inspect
    :param args: Page number, if more than 10 exist. Pages of the same character are as consistent as the filter.
    :param filter_: Filter by name.
    """
    page = int(args[0])-1 if args else 0
    items_per_page = 10
    classes = list(character.classes)
    num_classes = len(classes)
    total_pages = max((num_classes // items_per_page) + ((num_classes % items_per_page) != 0), 1)  # Don't need to add 1 if it divides exactly.
    if 0 > page or page >= total_pages:
        return {'content': f'Invalid page ID. Please use 1 - {total_pages}'}

    if filter_:
        classes = [char_class for char_class in classes if filter_ in any(inf.name for inf in char_class.class_.info)]

    classes = classes[page * items_per_page:(1 + page) * items_per_page]

    embed = Embed(
        title=f'Classes of {character.full_name}',
        description='',
        fields_=[],
        footer=EmbedFooter(text=f'Page: {page + 1}/{total_pages}')
    )
    skill_levels: Dict[Skill, int] = {char_skill.skill: char_skill.level for char_skill in character.skills}

    for char_class in classes:
        lines = [char_class.description]
        if char_class.class_.skills:
            lines.append('Skills:')
            lines.extend(f'- {skill.name} - Level {skill_levels[skill]}' for skill in char_class.class_.skills)

        titles = list(char_class.titles)
        if titles:
            lines.append('Titles')
            lines.extend(f'- {title}' for title in titles)

        embed.fields_.append(EmbedField(
            name=f'{char_class.title} - Level {char_class.level}',
            value='\n'.join(lines)
        ))
    return {'embed': embed}


@verify_open_channel
async def relationships_(character: Character, *args, filter_: Optional[str], reverse: bool):
    """
    Shows all relationships a character has.
    :param character: Character to inspect
    :param args: Page number, if more than 10 exist. Pages of the same character are as consistent as the filter.
    :param filter_: Filter by name.
    :param reverse: Reverse ordering, putting lowest values on top instead of highest.
    """
    page = int(args[0])-1 if args else 0
    items_per_page = 10
    number = session.query(Relationship).filter(Relationship.name.like(f'%{filter_}%'), Relationship.character == character).count() if filter_ else \
        session.query(Relationship).filter(Relationship.character == character).count()
    total_pages = max((number // items_per_page) + ((number % items_per_page) != 0), 1)  # Don't need to add 1 if it divides exactly.
    if 0 > page or page >= total_pages:
        return {'content': f'Invalid page ID. Please use 1 - {total_pages}'}

    query = session.query(Relationship).order_by(Relationship.value if reverse else desc(Relationship.value))
    if filter_:
        query = query.filter(Relationship.name.like(f'%{filter_}%'))

    relationships: List[Relationship] = query.offset(page * items_per_page).limit(items_per_page).all()
    embed = Embed(
        title=f'{character.full_name}\'s Relationships',
        description='\n'.join(f' - {rel.value} {"Affection from" if rel.sexual else "Reputation with"} {rel.name}' for rel in relationships),
        footer=EmbedFooter(text=f'Page: {page + 1}/{total_pages}')
    )
    return {'embed': embed}


@verify_open_channel
async def skills_(character: Character, *args, filter_: Optional[str]):
    """
    Displays all of a character's skills.
    :param character: Character to inspect
    :param args: Page number, if more than 10 exist. Pages of the same character are as consistent as the filter.
    :param filter_: Filter by name.
    """
    page = int(args[0])-1 if args else 0
    items_per_page = 10
    skills = list(character.skills)
    num_skills = len(skills)
    total_pages = max((num_skills // items_per_page) + ((num_skills % items_per_page) != 0), 1)  # Don't need to add 1 if it divides exactly.
    if 0 > page or page >= total_pages:
        return {'content': f'Invalid page ID. Please use 1 - {total_pages}'}

    if filter_:
        skills = [skill for skill in skills if filter_ in skill.name]

    skills = skills[page * items_per_page:(1 + page) * items_per_page]

    embed = Embed(
        title=f'Skills of {character.full_name}',
        description='',
        fields_=[],
        footer=EmbedFooter(text=f'Page: {page + 1}/{total_pages}')
    )
    for char_skill in skills:
        skill = char_skill.skill
        embed.fields_.append(EmbedField(
            name=f' - {skill.name} - Level {char_skill.level}/{skill.level_max}' if skill.level_max else f' - {skill.name} - Level {char_skill.level}',
            value=skill.description
        ))
    return {'embed': embed}


@verify_open_channel
async def shouts_(character: Character, *args, filter_: Optional[str]):
    """
    Displays all of a character's shouts.
    :param character: Character to inspect
    :param args: Page number, if more than 10 exist. Pages of the same character are as consistent as the filter.
    :param filter_: Filter by name.
    """
    page = int(args[0])-1 if args else 0
    items_per_page = 10
    shouts = list(character.shouts)
    num_shouts = len(shouts)
    total_pages = max((num_shouts // items_per_page) + ((num_shouts % items_per_page) != 0), 1)  # Don't need to add 1 if it divides exactly.
    if 0 > page or page >= total_pages:
        return {'content': f'Invalid page ID. Please use 1 - {total_pages}'}

    if filter_:
        shouts = [shout for shout in shouts if filter_ in shout.name or any(filter_ in word.name or filter_ in word.translation for word in shout.words)]

    shouts: List[CharacterShout] = shouts[page * items_per_page:(1 + page) * items_per_page]
    primary_lines = []
    if character.dragonsouls:
        primary_lines.append(f'Dragon Souls: {character.dragonsouls}')

    embed = Embed(
        title=f'Shouts of {character.full_name}',
        description='\n'.join(primary_lines),
        fields_=[],
        footer=EmbedFooter(text=f'Page: {page + 1}/{total_pages}')
    )
    for char_shout in shouts:
        shout: Shout = char_shout.shout
        lines = [
            f'**{" ".join(word.translation for word in shout.words)}**\n*{" - ".join(word.name for word in shout.words)}*',
            shout.description_for_character(character),
        ]
        if shout.cost(character):
            lines.append('Cost:')
            lines.extend(f'- {attribute.name}: **{value}**' for attribute, value in shout.cost(character).items())
        if shout.divinity(character):
            lines.append(f'Divinity: {shout.divinity(character)}')
        embed.fields_.append(EmbedField(
            name=shout.type_and_name,
            value='\n'.join(lines)
        ))
    return {'embed': embed}


@verify_open_channel
async def abilities_(character: Character, *args, filter_: Optional[str]):
    """
    Shows all abilities that belong to a character.
    :param character: Character to inspect
    :param args: Page number, if more than 10 exist. Pages of the same character are as consistent as the filter.
    :param filter_: Filter by name.
    """
    page = int(args[0])-1 if args else 0
    items_per_page = 10
    abilities = list(character.abilities)
    num_abil = len(abilities)
    total_pages = max((num_abil // items_per_page) + ((num_abil % items_per_page) != 0), 1)  # Don't need to add 1 if it divides exactly.
    if 0 > page or page >= total_pages:
        return {'content': f'Invalid page ID. Please use 1 - {total_pages}'}

    if filter_:
        abilities = [ability for ability in abilities if filter_ in ability.name]

    abilities = abilities[page * items_per_page:(1 + page) * items_per_page]
    embed = Embed(
        title=f'Abilities of {character.full_name}',
        description='',
        fields_=[],
        footer=EmbedFooter(text=f'Page: {page + 1}/{total_pages}')
    )
    for ability in abilities:
        lines = [ability.description_for_character(character)]
        if ability.costs:
            lines.append('Cost:')
            lines.extend(f'- {attribute.name}: **{int(value)}**' for attribute, value in ability.cost(character).items())
        embed.fields_.append(EmbedField(
            name=ability.name if ability.classification is None else f'{ability.classification.name}: {ability.name}',
            value='\n'.join(lines)
        ))
    return {'embed': embed}


@verify_open_channel
async def attributes_(character: Character, *args, filter_: Optional[str]):
    """
    Shows the attributes a given character has.
    :param character: Character to inspect
    :param args: Page number, if more than 10 exist. Pages of the same character are as consistent as the filter.
    :param filter_: Filter by name.
    """
    page = int(args[0])-1 if args else 0
    items_per_page = 20
    attributes = sorted(character.attributes.items(), key=lambda item: item[1], reverse=True)
    attributes = [attr for attr in attributes if attr[1] != 0]  # 0-valued isn't interesting.
    num_attr = len(attributes)
    total_pages = max((num_attr // items_per_page) + ((num_attr % items_per_page) != 0), 1)  # Don't need to add 1 if it divides exactly.
    if 0 > page or page >= total_pages:
        return {'content': f'Invalid page ID. Please use 1 - {total_pages}'}

    if filter_:
        attributes = [(attribute, value) for attribute, value in attributes if filter_ in attribute.name]

    attributes = attributes[page * items_per_page:(1 + page) * items_per_page]
    embed = Embed(
        title=f'Attributes of {character.name}',
        description='\n'.join(f' - **{attribute.name}**: {int(value)}' for attribute, value in attributes),
        footer=EmbedFooter(text=f'Page: {page + 1}/{total_pages}')
    )
    return {'embed': embed}


@verify_open_channel
async def perks_(character: Character, *args, filter_: Optional[str]):
    """
    Shows the perks of a given Character.
    :param character: Character to inspect
    :param args: Page number, if more than 10 exist. Pages of the same character are as consistent as the filter.
    :param filter_: Filter by name.
    """
    page = int(args[0])-1 if args else 0
    items_per_page = 20
    perks = list(character.perks)
    num_perks = len(perks)
    total_pages = max((num_perks // items_per_page) + ((num_perks % items_per_page) != 0), 1)  # Don't need to add 1 if it divides exactly.
    if 0 > page or page >= total_pages:
        return {'content': f'Invalid page ID. Please use 1 - {total_pages}'}

    if filter_:
        perks = [perk for perk in perks if filter_ in perk.name or filter_ in perk.description]

    perks = perks[page * items_per_page:(1 + page) * items_per_page]
    embed = Embed(
        title=f'Perks of {character.full_name}',
        description='\n'.join(f' - **{perk.name}**: {perk.description or ""}' for perk in perks),
        footer=EmbedFooter(text=f'Page: {page + 1}/{total_pages}')
    )
    return {'embed': embed}


@verify_open_channel
async def weapons_(character: Character, *args, filter_: Optional[str], equipped: bool):
    """
    Shows the weapons of a given Character.
    :param character: Character to inspect
    :param args: Page number, if more than 20 exist. Pages of the same character are as consistent as the filter.
    :param filter_: Filter by name.
    :param equipped: Only display equipped weapons.
    """
    page = int(args[0])-1 if args else 0
    items_per_page = 20
    num_weapons = session.query(Weapon).filter(Weapon.name.like(f'%{filter_}%'), Weapon.owner == character).count() if filter_ else \
        session.query(Weapon).filter(Weapon.owner == character).count()
    total_pages = max((num_weapons // items_per_page) + ((num_weapons % items_per_page) != 0), 1)  # Don't need to add 1 if it divides exactly.
    if 0 > page or page >= total_pages:
        return {'content': f'Invalid page ID. Please use 1 - {total_pages}'}

    query = session.query(Weapon).filter(Weapon.owner == character)
    if filter_:
        query = query.filter(Weapon.name.like(f'%{filter_}%'))

    if equipped:
        query = query.filter(Weapon.equipped == True)

    query = query.order_by(desc(Weapon.equipped))

    weapons: List[Weapon] = query.offset(page * items_per_page).limit(items_per_page).all()
    embed = Embed(
        title=f'{character.full_name}\'s Weapons',
        description=f'Weapons of {character.full_name}',
        fields=[],
        footer=EmbedFooter(text=f'Page: {page + 1}/{total_pages}')
    )
    for weapon in weapons:
        lines = [weapon.description]
        for ability in weapon.enchanted_abilities():
            lines.append(f'{ability.name}\n- {ability.description_for_character(character)}')
        embed.fields_.append(EmbedField(
            name=f'{weapon.name} (Equipped)' if weapon.equipped else weapon.name,
            value='\n'.join(lines)
        ))

    return {'embed': embed}


@verify_open_channel
async def list_(*args, filter_: Optional[str]):
    """
    Shows a list of all characters.
    :param args: Page number, if more than 10 exist. Pages of the same character are as consistent as the filter.
    :param filter_: Filter by name.
    """
    page = int(args[0])-1 if args else 0
    items_per_page = 20
    num_chars = session.query(Character).filter(Character.name.like(f'%{filter_}%')).count() if filter_ else session.query(Character).count()
    total_pages = max((num_chars // items_per_page) + ((num_chars % items_per_page) != 0), 1)  # Don't need to add 1 if it divides exactly.
    if 0 > page or page >= total_pages:
        return {'content': f'Invalid page ID. Please use 1 - {total_pages}'}

    query = session.query(Character)
    if filter_:
        query = query.filter(Character.name.like(f'%{filter_}%'))

    characters: List[Character] = query.offset(page * items_per_page).limit(items_per_page).all()
    embed = Embed(
        title='Character List',
        description='\n'.join(f' - **{char.full_name}**: Level {char.level or ""}' for char in characters),
        footer=EmbedFooter(text=f'Page: {page + 1}/{total_pages}')
    )
    return {'embed': embed}


@verify_open_channel
async def view(character: Character):
    """
    View details about a specific character
    :param character: Character to view
    """
    embed = Embed(
        title=character.full_name,
        fields_=[],
    )
    level = character.level
    primary_field = [f'Level: **{level}**']
    if character.divinity:
        primary_field.append(f'Divinity: **{character.divinity}**')

    if character.dragonsouls:
        primary_field.append(f'Dragon Souls: **{character.dragonsouls}**')

    embed.fields_.append(EmbedField(name='General', value='\n'.join(primary_field)))

    classes: List[CharacterClass] = sorted(character.classes, key=lambda item: item.level, reverse=True)
    if classes:
        embed.fields_.append(EmbedField(
            name='Classes' if len(classes) <= 10 else f'Classes (10/{len(classes)})',
            value='\n'.join(f' - *{char_class.title}* : {char_class.level}' for char_class in classes[:10])
        ))

    skills: List[CharacterSkill] = sorted(character.skills, key=lambda item: item.level, reverse=True)
    if skills:
        def skill_gen():
            for char_skill in skills[:10]:
                if char_skill.skill.class_ in [char_class.class_ for char_class in character.classes]:
                    char_class = [char_class for char_class in character.classes if char_class.class_ is char_skill.skill.class_][0]
                    class_line = f'({char_class.title}) '
                else:
                    class_line = ''
                yield f'- {class_line}*{char_skill.skill.name}* : {char_skill.level}'

        embed.fields_.append(EmbedField(
            name='Skills' if len(skills) <= 10 else f'Skills (10/{len(skills)})',
            value='\n'.join(skill_gen())
        ))

    perks = list(character.perks)
    if perks:
        embed.fields_.append(EmbedField(
            name='Perks' if len(perks) <= 10 else f'Perks (10/{len(perks)})',
            value='\n'.join(f'- {perk.name}' for perk in perks[:10])
        ))

    attributes = sorted(character.attributes.items(), key=lambda item: item[1], reverse=True)  # Sort by value, descending
    damage = {attr_dam.attribute: attr_dam.damage for attr_dam in character.damage if attr_dam.damage != 0}
    attributes = [(name, value) for name, value in attributes if (value + damage.get(name, 0)) != 0]
    if attributes:
        def attribute_gen():
            for attr, value in attributes[:10]:
                if value == 0:
                    continue
                if attr not in damage:
                    yield f'- *{attr.name}* : {int(value)}'
                else:
                    yield f'- *{attr.name}* : {int(value - damage.get(attr))}/{int(value)}'

        embed.fields_.append(EmbedField(
            name=f'Attributes (10/{len(attributes)})' if len(attributes) > 10 else 'Attributes',
            value='\n'.join(attribute_gen())
        ))

    abilities = list(character.abilities)
    if abilities:
        embed.fields_.append(EmbedField(
            name='Abilities' if len(abilities) <= 5 else f'Abilities (5/{len(abilities)})',
            value='\n'.join(f'- *{ability.name}* : {ability.description_for_character(character)}' for ability in abilities[:5])
        ))

    shouts = list(character.shouts)
    if shouts:
        embed.fields_.append(EmbedField(
            name='Shouts' if len(shouts) <= 3 else f'Shouts (3/{len(shouts)})',
            value='\n'.join(f'- **{shout.shout.name}** '
                            f'*{" ".join(word.translation for word in sorted(shout.shout.words, key=lambda word: word.word_id)[:shout.max_words])}* : '
                            f'{shout.shout.description_for_character(character)}' for shout in shouts[:3])
        ))

    weapons: List[Weapon] = sorted(character.weapons, key=lambda weapon__: not weapon__.equipped)
    if weapons:
        def weapon_gen():
            for weapon in weapons[:2]:
                yield f' - **{f"{weapon.name} (Equipped)" if weapon.equipped else weapon.name}:** {weapon.description}'
                for ability in weapon.enchanted_abilities():
                    yield f'*{ability.name}*: {ability.description_for_character(character)}'

        embed.fields_.append(EmbedField(
            name=f'Weapons (2/{len(weapons)})' if len(weapons) > 2 else 'Weapons',
            value='\n'.join(weapon_gen())
        ))

    armor: List[Armor] = sorted(character.armor, key=lambda armor__: armor__.equipped)
    if armor:
        def armor_gen():
            for armor_item in armor[:5]:
                yield f'- {armor_item.name} {"(Equipped) " if armor_item.equipped else ""}{f"Divinity: {armor_item.divinity}" if armor_item.divinity else ""}'
                if armor_item.perks:
                    yield 'Perks: ' + ' '.join(perk.name for perk in armor_item.perks)
                if armor_item.attributes:
                    yield f'Armor Attributes:'
                    for attribute, value in armor_item.attributes.items():
                        yield f'   - {attribute.name}: {value}'

        embed.fields_.append(EmbedField(
            name=f'Armor (5/{len(armor)})' if len(armor) <= 5 else 'Armor',
            value='\n'.join(armor_gen())
        ))

    titles = list(character.available_titles)
    if titles:
        embed.fields_.append(EmbedField(name='Titles', value='\n- '.join(titles)))

    return {'embed': embed}


def collect_unlocks(unlocks: list):
    """
    Parses unlocks and returns it in a neat dictionary.
    :param unlocks:
    :return:
    """
    result = {
        'classes': [],
        'skills': [],
        'abilities': [],
        'perks': [],
    }
    for unlock in unlocks:
        if unlock.class_id is not None:
            result['classes'].append((unlock.level, unlock.class_.name))
        if unlock.skill_id is not None:
            result['skills'].append((unlock.level, unlock.skill.name))
        if unlock.perk_id is not None:
            result['abilities'].append((unlock.level, unlock.perk.name))
        if unlock.ability_id is not None:
            result['perks'].append((unlock.level, unlock.ability.name))

    return result

'''
@verify_open_channel
async def sheet(character: Character):
    """
    View ALL details about a specific character.
    **This function assumes that BBCode Renderer is used.**
    :param character: Character to view
    """
    newline = '\n'  #... really?
    embed = Embed(
        title=character.full_name
    )
    level = character.level
    primary_field = [f'Level: **{level}**']
    if character.divinity:
        primary_field.append(f'Divinity: **{character.divinity}**')

    if character.dragonsouls:
        primary_field.append(f'Dragon Souls: **{character.dragonsouls}**')

    embed.description = '\n'.join(primary_field)

    classes: List[CharacterClass] = sorted(character.classes, key=lambda item: item.level, reverse=True)
    if classes:
        tabs = []
        for char_class in classes:
            class_ = char_class.class_
            cls = f"**{char_class.title}**\n*{char_class.description}*"
            if class_.attributes:
                cls += '[fieldset=Attributes per Level]'
                for cls_attr in class_.attributes:
                    cls += f'\n- {cls_attr.attribute.name}: {cls_attr.multiplier}'
                cls += '[/fieldset]'
            if class_.skills:
                cls += '[fieldset=Skills]'
                for skill in class_.skills:
                    cls += f'- {skill.name}'
                cls += '[/fieldset]'
            unlocks = collect_unlocks(class_.unlocks)
            if unlocks['classes']:
                cls += '[fieldset=Classes Unlocked]'
                for level, name in unlocks['classes']:
                    cls += f'- {level}: {name}'
                cls += '[/fieldset]'
            if unlocks['skills']:
                cls += '[fieldset=Skills Unlocked]'
                for level, name in unlocks['skills']:
                    cls += f'- {level}: {name}'
                cls += '[/fieldset]'
            if unlocks['perks']:
                cls += '[fieldset=Perks Unlocked]'
                for level, name in unlocks['perks']:
                    cls += f'- {level}: {name}'
                cls += '[/fieldset]'
            if unlocks['abilities']:
                cls += '[fieldset=Abilities Unlocked]'
                for level, name in unlocks['abilities']:
                    cls += f'- {level}: {name}'
                cls += '[/fieldset]'

            tabs.append(f'[tab={char_class.title}]{cls}[/tab]')

        embed.description += f'[tabs]{newline.join(tabs)}[/tabs]'

    skills: List[CharacterSkill] = sorted(character.skills, key=lambda item: item.level, reverse=True)
    if skills:
        tabs = []
        for char_skill in skills:
            skill = char_skill.skill
            skl = f"**{skill.name}**\n*{skill.description}*"
            if skill.level_max is not None:
                skl += f'Maximum Level: {skill.level_max}'

            if skill.class_ in [char_class.class_ for char_class in character.classes]:
                char_class = [char_class for char_class in character.classes if char_class.class_ is char_skill.skill.class_][0]
                skl += f'Class: {char_class.title}'

            if skill.attributes:
                skl += '[fieldset=Attributes per Level]'
                for cls_attr in skill.attributes:
                    skl += f'\n- {cls_attr.attribute.name}: {cls_attr.multiplier}'
                skl += '[/fieldset]'
            unlocks = collect_unlocks(skill.unlocks)
            if unlocks['classes']:
                skl += '[fieldset=Classes Unlocked]'
                for level, name in unlocks['classes']:
                    skl += f'- {level}: {name}'
                skl += '[/fieldset]'
            if unlocks['skills']:
                skl += '[fieldset=Skills Unlocked]'
                for level, name in unlocks['skills']:
                    skl += f'- {level}: {name}'
                skl += '[/fieldset]'
            if unlocks['perks']:
                skl += '[fieldset=Perks Unlocked]'
                for level, name in unlocks['perks']:
                    skl += f'- {level}: {name}'
                skl += '[/fieldset]'
            if unlocks['abilities']:
                skl += '[fieldset=Abilities Unlocked]'
                for level, name in unlocks['abilities']:
                    skl += f'- {level}: {name}'
                skl += '[/fieldset]'

            tabs.append(f'[tab={skill.name}]{skl}[/tab]')

        embed.description += f'[tabs]{newline.join(tabs)}[/tabs]'

    perks = list(character.perks)
    if perks:
        tabs = []
        for perk in perks:

            prk = f"**{perk.name}**\n*{perk.description}*"

            if perk.attributes:
                prk += '[fieldset=Attributes]'
                for perk_attr in perk.attributes:
                    prk += f'\n- {perk_attr.attribute.name}: {perk_attr.value}'
                prk += '[/fieldset]'

            unlocks = collect_unlocks(perk.unlocks)
            if unlocks['classes']:
                prk += '[fieldset=Classes Unlocked]'
                for level, name in unlocks['classes']:
                    prk += f'- {level}: {name}'  # NOTE: DO PUT NEWLINES IN THESE STATEMENTS
                prk += '[/fieldset]'
            if unlocks['skills']:
                prk += '[fieldset=Skills Unlocked]'
                for level, name in unlocks['skills']:
                    prk += f'- {level}: {name}'
                prk += '[/fieldset]'
            if unlocks['perks']:
                prk += '[fieldset=Perks Unlocked]'
                for level, name in unlocks['perks']:
                    prk += f'- {level}: {name}'
                prk += '[/fieldset]'
            if unlocks['abilities']:
                prk += '[fieldset=Abilities Unlocked]'
                for level, name in unlocks['abilities']:
                    prk += f'- {level}: {name}'
                prk += '[/fieldset]'

            tabs.append(f'[tab={perk.name}]{prk}[/tab]')

        embed.description += f'[tabs]{newline.join(tabs)}[/tabs]'

    attributes = sorted(character.attributes.items(), key=lambda item: item[1], reverse=True)  # Sort by value, descending
    damage = {attr_dam.attribute: attr_dam.damage for attr_dam in character.damage if attr_dam.damage != 0}
    attributes = [(name, value) for name, value in attributes if (value + damage.get(name, 0)) != 0]

    if attributes:
        for attr_chunk in chunks(attributes, 20):
            attrs = ''
            for attribute, value in attr_chunk:
                if value == 0:
                    continue
                if attribute not in damage:
                    lines.append(f'- *{attribute.name}* : {int(value)}')
                else:
                    lines.append(f'- *{attribute.name}* : {int(value - damage.get(attribute))}/{int(value)}')



    lines.clear()
    if len(attributes) > 10:
        lines.append(f'To see all of {character.name}\'s attributes, use `character attributes "{character.name}"`')
        attributes = attributes[:9]

    for attribute, value in attributes:
        if value == 0:
            continue
        if attribute not in damage:
            lines.append(f' - *{attribute.name}* : {int(value)}')
        else:
            lines.append(f' - *{attribute.name}* : {int(value - damage.get(attribute))}/{int(value)}')

    if lines:
        embed.fields_.append(EmbedField(
            name='Attributes',
            value='\n'.join(lines)
        ))

    abilities = list(character.abilities)
    lines.clear()
    if len(abilities) > 5:
        lines.append(f'To see all {character.name}\'s abilities, use `character abilities "{character.name}"`')
        abilities = abilities[:4]

    lines.extend(f' - *{ability.name}* : {ability.description_for_character(character)}' for ability in abilities)
    if lines:
        embed.fields_.append(EmbedField(
            name='Abilities',
            value='\n'.join(lines)
        ))

    shouts = list(character.shouts)
    lines.clear()
    if len(shouts) > 3:
        lines.append(f'To see all {character.name}\'s shouts, use `character shouts "{character.name}"`')
        shouts = shouts[:2]

    lines.extend(f'- **{shout.shout.name}** '
                 f'*{" ".join(word.translation for word in sorted(shout.shout.words, key=lambda word: word.word_id)[:shout.max_words])}* : '
                 f'{shout.shout.description_for_character(character)}' for shout in shouts)
    if lines:
        embed.fields_.append(EmbedField(
            name='Shouts',
            value='\n'.join(lines)
        ))

    weapons: List[Weapon] = sorted(character.weapons, key=lambda weapon__: not weapon__.equipped)
    lines.clear()
    if len(weapons) > 1:
        lines.append(f'To view all weapons, use `character weapons {character.name}`')
        weapons = [weapons[0], ]

    for weapon in weapons:
        lines.append(f' - **{f"{weapon.name} (Equipped)" if weapon.equipped else weapon.name}:** {weapon.description}')
        for ability in weapon.enchanted_abilities():
            lines.append(f'*{ability.name}*: {ability.description_for_character(character)}')

    if lines:
        embed.fields_.append(EmbedField(
            name='Weapons',
            value='\n'.join(lines)
        ))

    armor: List[Armor] = sorted(character.armor, key=lambda armor__: armor__.equipped)
    lines.clear()
    if len(armor) > 5:
        lines.append(f'To view all armor, use `character armor {character.name}`')
        armor = [armor__ for armor__ in armor if armor__.equipped]

    for armor_item in armor:
        lines.append(f'- {armor_item.name} {"(Equipped) " if armor_item.equipped else ""}{f"Divinity: {armor_item.divinity}" if armor_item.divinity else ""}')
        if armor_item.perks:
            lines.append('Perks: ' + ' '.join(perk.name for perk in armor_item.perks))
        if armor_item.attributes:
            lines.append(f'Armor Attributes:')
            for attribute, value in armor_item.attributes.items():
                lines.append(f'   - {attribute.name}: {value}')

    if lines:
        embed.fields_.append(EmbedField(name='Armor', value='\n'.join(lines)))

    items: List[Item] = sorted(character.items, key=lambda item: item.amount)
    lines.clear()
    if len(items) > 10:
        lines.append(f'To view a complete inventory (Potentially through pagination), use `character inventory {character.name}`')
        items = items[:9]

    lines.extend(f' - {item.name}: {item.amount}' for item in items)
    if lines:
        embed.fields_.append(EmbedField(name='Inventory', value='\n'.join(lines)))

    titles = list(character.available_titles)
    if titles:
        embed.fields_.append(EmbedField(name='Titles', value=' '.join(titles)))

    return {'embed': embed}
'''


def check_armor_endpoint(message: 'Message', args: 'Namespace'):
    if not args or args[0].isdecimal():  # View listing, no editing.
        return check_channel_open(message, args)
    return check_author_and_channel(message, args)


@verify(check_armor_endpoint)
async def armor_(character: Character, *args, divinity: Optional[int], equipped: Optional[int], filter_: Optional[str], perks: Optional[List[Perk]]):
    """
    Adds Armor to a character, or changes existent armor.
    :param character: Character whose armor it's about
    :param divinity: Divinity Rating
    :param equipped: Whether it is equipped or not.
    :param perks: When editing/creating armor, a list of perks to attach to that armor. Adding existing perks removes them instead.
    :param filter_: Filter by armor name.
    """
    if not args or args[0].isdecimal():
        page = int(args[0]) - 1 if args else 0
        items_per_page = 5
        armor = list(character.armor)
        num_armor_pieces = len(armor)
        total_pages = max((num_armor_pieces // items_per_page) + ((num_armor_pieces % items_per_page) != 0), 1)  # Don't need to add 1 if it divides exactly.
        if 0 > page or page >= total_pages:
            return {'content': f'Invalid page ID. Please use 1 - {total_pages}'}

        if filter_:
            armor = [armor__ for armor__ in armor if filter_ in armor__.name]

        if equipped is not None:
            armor = [armor__ for armor__ in armor if armor__.equipped == bool(equipped)]

        armor = armor[page * items_per_page:(1 + page) * items_per_page]
        lines = []
        embed = Embed(
            title=f'Armor of {character.full_name}',
            fields_=[],
            footer=EmbedFooter(text=f'Page: {page + 1}/{total_pages}')
        )

        for armor_item in armor:
            lines.clear()
            if armor_item.perks:
                lines.append('Perks: ' + ' '.join(perk.name for perk in armor_item.perks))
            if armor_item.attributes:
                lines.append(f'Armor Attributes:')
                for attribute, value in armor_item.attributes.items():
                    lines.append(f'{attribute.name}: {value}')

            embed.fields_.append(EmbedField(
                name=f'{armor_item.name} {"(Equipped) " if armor_item.equipped else ""}{f"Divinity: {armor_item.divinity}" if armor_item.divinity else ""}',
                value='\n'.join(lines) or 'No bonus.'
            ))

        return {'embed': embed}

    name = args[0]
    armor = session.query(Armor).filter(Armor.owner == character, Armor.name == name).one_or_none()
    new = armor is None
    if new:
        armor = Armor(owner=character, name=name, divinity=divinity or 0, equipped=bool(equipped))
        session.add(armor)
        session.commit()
        return {
            'content': f'Created new armor {name} for {character.full_name} which is {"" if equipped else "not "}equipped and has {divinity or 0} divinity.'
        }

    lines = [f'Changed {character.full_name}\'s {armor.name}.']

    if perks is not None:
        for perk in perks:
            if perk in armor.perks:
                lines.append(f'Removed armor perk: {perk.name}')
                armor.perks.remove(perk)
            else:
                lines.append(f'Adding {perk.name} to {armor.name}')
                armor.perks.append(perk)

    if divinity is not None:
        lines.append(f'Set Divinity from {armor.divinity} to {divinity}.')
        armor.divinity = divinity

    if equipped is not None:
        lines.append(f'Set to {"" if equipped else "not "}equipped.')

    session.commit()
    return {'content': '\n'.join(lines)}


@verify_author_and_channel
async def armor_attribute(character: Character, name: str, attribute: Attribute, value: int):
    """
    Adds an attribute value to an armor.
    :param character: Character who has the armor
    :param name: Which armor it is
    :param attribute: Which attribute should be boosted
    :param value: How much the attribute should be boosted
    """
    armor = session.query(Armor).filter(Armor.owner == character, Armor.name == name).one_or_none()
    if armor is None:
        return {'content': f'Armor {name} not found for {character.name}.'}
    armor_attr = session.query(ArmorAttribute).filter(ArmorAttribute.armor == armor, ArmorAttribute.attribute == attribute).one_or_none()
    if armor_attr is None:
        armor_attr = ArmorAttribute(armor=armor, attribute=attribute, value=value)
        session.add(armor_attr)
        session.commit()
        return {'content': f'{armor.name} now boosts {attribute.name} with {value}'}
    armor_attr.value = value
    session.commit()
    return {'content': f'{armor.name}\'s boost of {attribute.name} is now {value}'}


@verify_author_and_channel
async def item_(character: Character, name: str, amount_mod: int):
    """
    Adds/removes items, and/or modifies their description.
    :param character: Character to get/lost items
    :param name: Item to get/lose
    :param amount_mod: How many items to give (negative to take away)
    """
    item = session.query(Item).filter(Item.character == character, Item.name == name).one_or_none()
    new = item is None
    if new:
        item = Item(character=character, name=name, amount=amount_mod)
        session.add(item)
    else:
        item.amount += amount_mod
        if item.amount == 0:
            session.delete(item)

    session.commit()
    return {'content': f'{character.full_name} now has {item.amount} {item.name}'}


@verify_author_and_channel
async def weapon_(
        character: Character, name: str, description: Optional[str], value: Optional[int], divinity: Optional[int], equipped: Optional[int],
        abilities: List[Ability], remove: bool, rename: Optional[str]):
    """
    Adds a weapon to a character
    :param character: Character to get a weapon
    :param name: Name of the weapon, like "Steel Sword", Dawnbreaker
    :param description: Description of the weapon, if any.
    :param value: Value of the weapon
    :param divinity: Divinity rating of the weapon
    :param equipped: Whether it should be equipped or not.
    :param abilities: Abilities to attach to this weapon. Giving an ability already present removes it.
    :param remove: Ignores everything not required to identify the weapon, and removes the weapon.
    :param rename: Sets a new name. Ignored on creation of a new weapon.
    """
    weapon = session.query(Weapon).filter(Weapon.owner == character, Weapon.name == name).one_or_none()
    new = weapon is None
    res = []
    if new and not remove:
        res.append(f'New Weapon for {character.full_name}: {name}')
        weapon = Weapon(owner=character, name=name)
        session.add(weapon)
    elif new and remove:
        return {'content': f'Cannot delete weapon {name} from {character.full_name}, because it doesn\'t exist.'}
    elif remove:
        session.delete(weapon)
        session.commit()
        return {'content': f'Deleted {weapon.name} from {character.full_name}'}
    else:
        res.append(f'Editing {character.full_name}\'s {weapon.name}')

    if not new and rename is not None:
        res.append(f'Renaming {weapon.name} to {rename}')
        weapon.name = rename

    if description is not None:
        res.append(f'Setting Description to {description}')
        weapon.description = description

    if value is not None:
        res.append(f'Setting Value to {value}')
        weapon.value = value

    if divinity is not None:
        res.append(f'Setting divinity to {divinity}')
        weapon.divinity = divinity

    if equipped is not None:
        res.append(f'Setting weapon as {"" if bool(equipped) else "not "}equipped.')
        weapon.equipped = bool(equipped)

    for ability in abilities:
        if ability in weapon.abilities:
            res.append(f'Removing {ability.name} from {weapon.name}')
            weapon.abilities.remove(ability)
        else:
            res.append(f'Adding {ability.name} to {weapon.name}')
            weapon.abilities.append(ability)

    session.commit()
    return {'content': '\n'.join(res)}


@verify_author_and_channel
async def enchant_(character: Character, weapon_name: str, ability: Ability):
    """
    Adds an enchantment to a weapon
    :param character: Character who owns the weapon
    :param weapon_name: Name of the weapon
    :param ability: Enchantment the weapon has
    """
    weapon = Weapon.get_or_none(character, weapon_name)
    if weapon is None:
        return {'content': f'Weapon {weapon_name} not found in {character.full_name}\'s inventory.'}
    weapon.enchant(ability)
    session.commit()
    return {'content': f'{weapon_name} enchanted with {ability.render_name}'}


@verify_author_and_channel
async def perk_(
        character: Character,
        perk: Perk,
        remove: bool,
):
    """
    Adds a perk to a character.
    :param character: Which character is to have a perk or not.
    :param perk: Which perk it is about.
    :param remove: Remove instead of add the perk.
    """
    bonus_perk = session.query(BonusPerk).filter(BonusPerk.character == character, BonusPerk.perk == perk).one_or_none()
    if remove:
        if bonus_perk is None:
            return {'content': f'Error, NPC {character.name} does not have perk {perk.name}'}

        session.delete(bonus_perk)
        character.process_unlocks()
        session.commit()
        return {'content': f'Perk {perk.name} removed from {character.name}'}

    if bonus_perk is None:
        session.add(BonusPerk(character=character, perk=perk))
        character.process_unlocks()
        session.commit()
        return {'content': f'Successfully added {perk.name} to {character.name}'}

    return {'content': f'Error, {character.name} already has {perk.name}.'}


@verify_author_and_channel
async def class__(
        character: Character,
        class_: Class,
        remove: bool,
):
    """
    Adds a class to a character.
    :param character: Which character is to have a class or not.
    :param class_: Which class it is about.
    :param remove: Remove instead of add the class.
    """
    char_cls: CharacterClass = session.query(CharacterClass).filter(CharacterClass.character == character, CharacterClass.class_ == class_).one_or_none()
    if remove:
        if char_cls is None:
            return {'content': f'Error, Character {character.name} does not have class {class_.name_fallback}'}

        session.delete(char_cls)
        character.process_unlocks()
        session.commit()
        return {'content': f'Class {class_.name_fallback} removed from {character.name}'}

    if char_cls is None:
        session.add(CharacterClass(character=character, class_=class_))
        character.process_unlocks()
        session.commit()
        return {'content': f'Successfully added {class_.name_fallback} to {character.name}.'}

    return {'content': f'Error, {character.name} already has Class {class_.name_fallback}.'}


@verify_author_and_channel
async def skill_(
        character: Character,
        skill: Skill,
        remove: bool,
        level: Optional[int],
):
    """
    Adds a skill to a character.
    :param character: Which character is to have a skill or not.
    :param skill: Which skill it is about.
    :param remove: Remove instead of add the skill.
    :param level: What level the skill should have.
    """
    char_skill: CharacterSkill = session.query(CharacterSkill).filter(CharacterSkill.character == character, CharacterSkill.skill == skill).one_or_none()
    if remove:
        if char_skill is None:
            return {'content': f'Error, Character {character.name} does not have skill {skill.name}'}

        session.delete(char_skill)
        character.process_unlocks()
        session.commit()
        return {'content': f'Skill {skill.name} removed from {character.name}'}

    if char_skill is None:
        session.add(CharacterSkill(character=character, skill=skill, level=level or 0))  # We need to be able to omit for editing, so level can be None.
        character.process_unlocks()
        session.commit()
        return {'content': f'Successfully added {skill.name} to {character.name} with level {level or 0}'}

    char_skill.level = level or 0
    character.process_unlocks()
    session.commit()
    return {'content': f'{skill.name} is now level {char_skill.level}.'}


@verify_author_and_channel
async def shout_(
        character: Character,
        shout: Shout,
        remove: bool,
):
    """
    Makes a Shout available to a character. Words are automatically learned if sufficient dragonsouls available.
    :param character: Character to get a shout.
    :param shout: Shout to give.
    :param remove: Remove the shout instead of giving it.
    """
    char_shout = session.query(CharacterShout).filter(CharacterShout.character == character, CharacterShout.shout == shout).one_or_none()
    if char_shout is None:
        if remove:
            return {'content': f'Error, Character {character.full_name} doesn\'t have shout {shout.name}'}

        session.add(CharacterShout(character=character, shout=shout))
        session.commit()
        return {'content': f'Successfully added {shout.name} to {character.full_name}'}
    elif remove:
        session.delete(char_shout)
        session.commit()
        return {'content': f'Successfully removed {shout.name} from {character.full_name}'}

    return {'content': f'Error, Character {character.full_name} already has shout {shout.name}'}


@verify_author_and_channel
async def relationship(
        character: Character,
        name: str,
        value: int,
        sexual: Optional[int],
        remove: bool,
):
    """
    Sets relationship value with `name`.
    :param character: Character who has the relationship
    :param name: Name of who the relationship is with.
    :param value: Numerical value: -100 -> Hated, 100 -> Loved.
    :param sexual: Whether the relationship is potentially sexual.
    :param remove: Removes the reputation relationship instead.
    """
    rel = session.query(Relationship).filter(Relationship.character == character, Relationship.name == name).one_or_none()
    if rel is None:
        rel = Relationship(character=character, name=name, value=value, sexual=bool(sexual))
        session.add(rel)
    elif remove:
        session.delete(rel)
        session.commit()
        return {'content': f'Deleted relationship with {name}.'}
    else:
        rel.value = value
        if sexual is not None:
            rel.sexual = bool(sexual)

    session.commit()
    form = 'Affection from' if rel.sexual else 'Reputation with'
    return {'content': f'{character.name} has {rel.value} {form} {rel.name}'}


@verify_author_and_channel
async def edit(
        character: Character,
        title: Optional[str],
        divinity: Optional[int],
        dragonsouls: Optional[int],
        name: Optional[str],
        author: int = FromMessage.Author,
        guild_id: int = FromMessage.Guild,
):
    """
    Edits a character.
    :param character: Name of the Character
    :param title: Title of the Character
    :param divinity: Divinity rating.
    :param author: RL person managing this NPC.
    :param dragonsouls: Number of dragon souls the NPC possesses.
    :param name: New name for the NPC.
    :param guild_id: Current Discord Server.
    """
    if title not in character.available_titles and title is not None:
        return {'content': f'Title "{title}" is not available to {character.name}'}

    if name is not None:
        session.add(CharacterAlias(name=character.name, character=character))
        character.name = name

    return await create(name=character.name, title=title, divinity=divinity, author_id=int(author), dragonsouls=dragonsouls, guild_id=guild_id)


@verify_author_and_channel
async def create(
        name: str,
        title: Optional[str],
        divinity: int = 0,
        author_id: int = FromMessage.Author,
        guild_id: int = FromMessage.Guild,
        dragonsouls: int = 0,
):
    """
    Creates a new Character.
    :param name: Name of the Character
    :param title: Title of the Character
    :param divinity: Divinity rating.
    :param author_id: RL person managing this Character.
    :param guild_id: Discord Guild we're working in.
    :param dragonsouls: Number of dragon souls the Character possesses.
    """
    # MC Name prefixes: J(a), Ra, Ri
    character: Character = session.query(Character).filter(Character.name == name).one_or_none()
    author = session.query(Author).filter(Author.user_id == author_id, Author.guild_id == guild_id).one()
    new = character is None
    if new:
        character = Character(name=name, author_id=author.entity_id)
        session.add(character)

    if title in list(character.available_titles):  # None isn't in here, so that stops it from resetting when omitted.
        character.title = title

    if divinity is not None:
        character.divinity = divinity

    if dragonsouls is not None:
        character.dragonsouls = dragonsouls

    session.commit()
    if new:
        return {'content': f'Successfully created {character.name} as new Character.'}

    return {'content': f'Updated {character.name}'}

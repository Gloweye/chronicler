"""Manage Perks"""

from logging import getLogger
from typing import Optional

from chronicler.discord.embed import Embed, EmbedField, EmbedFooter
from chronicler.models.ability import Ability
from chronicler.models.attribute import Attribute
from chronicler.models.class_ import Class
from chronicler.models.skill import Skill
from chronicler.parser import SubParser
from chronicler.models.base import session
from chronicler.models.perk import Perk, PerkUnlock, PerkAttribute
from chronicler.parser import verify_author_and_channel, verify_open_channel

logger = getLogger(__name__)


def add_perk_parsers(subparsers: SubParser):
    parser = subparsers.add_parser('perk', description='Functions for managing perks.', aliases=['perks', 'Perks', 'Perk'])
    sub: SubParser = parser.add_subparsers()
    for func in [create, unlock, view, list_, attribute]:
        sub.add_parser_from_function(func)


@verify_open_channel
async def view(perk: Perk):
    """
    Shows the details of a perk.
    :param perk: Name of the perk.
    """
    embed = Embed(
        title=perk.name,
        description=perk.description,
        fields_=[]
    )
    if perk.attributes:
        embed.fields_.append(EmbedField(
            name='Attribute Increases',
            value='\n'.join(f'{p_attr.attribute.name}: {p_attr.value}' for p_attr in perk.attributes)
        ))
    if perk.unlocks:
        classes, skills, abilities, perks = [], [], [], []
        for perk_unlock in perk.unlocks:
            if perk_unlock.class_id is not None:
                classes.append(perk_unlock)
            if perk_unlock.skill_id is not None:
                skills.append(perk_unlock)
            if perk_unlock.perk_id is not None:
                perks.append(perk_unlock)
            if perk_unlock.ability_id is not None:
                abilities.append(perk_unlock)

        if classes:
            embed.fields_.append(EmbedField(
                name='Classes Granted',
                value='\n'.join(unlock.class_.name for unlock in classes)
            ))
        if skills:
            embed.fields_.append(EmbedField(
                name='Skills Granted',
                value='\n'.join(unlock.skill.name for unlock in skills)
            ))
        if perks:
            embed.fields_.append(EmbedField(
                name='Perks Granted',
                value='\n'.join(unlock.perk.name for unlock in perks)
            ))
        if abilities:
            embed.fields_.append(EmbedField(
                name='Abilities Granted',
                value='\n'.join(unlock.ability.name for unlock in abilities)
            ))

    return {'embed': embed}


@verify_open_channel
async def list_(*args, filter_: Optional[str]):
    """
    Lists all perks.
    :param filter_: Case-insensitive filtration of names.
    """
    page = int(args[0])-1 if args else 0
    items_per_page = 20
    num_perks = session.query(Perk).filter(Perk.name.like(f'%{filter_}%')).count() if filter_ else session.query(Perk).count()
    total_pages = max((num_perks // items_per_page) + ((num_perks % items_per_page) != 0), 1)  # Don't need to add 1 if it divides exactly.
    if 0 > page or page >= total_pages:
        return {'content': f'Invalid page ID. Please use 1 - {total_pages}'}

    query = session.query(Perk)
    if filter_:
        query = query.filter(Perk.name.like(f'%{filter_}%'))

    perks = query.offset(page * items_per_page).limit(items_per_page).all()
    embed = Embed(
        title='Perk List',
        description='\n'.join(f'**{perk.name}**: {perk.description}' for perk in perks),
        footer=EmbedFooter(text=f'Page: {page+1}/{total_pages}')
    )
    return {'embed': embed}


@verify_author_and_channel
async def create(name: str, description: str):
    """
    Creates a new Perk.
    :param name: Name of the new perk.
    :param description: Description of the new perk.
    """
    if session.query(Perk).filter(Perk.name == name).one_or_none() is not None:
        return {'content': f'Sorry, there already exists a Perk named {name}'}

    session.add(Perk(
        name=name,
        description=description
    ))
    session.commit()
    return {'content': f'Perk "{name}" has been successfully added.'}


@verify_author_and_channel
async def attribute(perk: Perk, attribute: Attribute, magnitude: int):
    """
    Adds an attribute boost to a Perk
    :param perk: Perk to get the boost
    :param attribute: Attribute to be boosted
    :param magnitude: How big the boost is.
    """
    perk_attr = session.query(PerkAttribute).filter(PerkAttribute.perk == perk, PerkAttribute.attribute == attribute).one_or_none()
    if perk_attr is None:
        perk_attr = PerkAttribute(perk=perk, attribute=attribute, value=magnitude)
        session.add(perk_attr)
    else:
        perk_attr.value = magnitude
    session.commit()
    return {'content': f'Successfully set {perk.name} to grant {magnitude} {attribute.name}'}


@verify_author_and_channel
async def unlock(parent: Perk, class_: Optional[Class] = None, skill: Optional[Skill] = None, perk: Optional[Perk] = None, ability: Optional[Ability] = None):
    """
    Adds an unlock to a perk.
    Please note that everything except abilities is gained permanently, even if this perk is later lost.
    :param parent: Perk which gets a new boost on unlock.
    :param class_: Class to unlock, if any.
    :param skill: Skill to unlock, if any.
    :param perk: Perk to unlock, if any.
    :param ability: Abilities to unlock.
    """
    kwargs = {'parent': parent}
    if class_ is not None:
        kwargs['class'] = class_
    if skill is not None:
        kwargs['skill'] = skill
    if perk is not None:
        kwargs['perk'] = perk
    if ability is not None:
        kwargs['ability'] = ability

    session.add(PerkUnlock(**kwargs))
    session.commit()
    return {'content': f'Successfully added unlock to Perk {parent.name}'}

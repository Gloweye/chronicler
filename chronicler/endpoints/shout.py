"""Manage Abilities"""

from logging import getLogger
from typing import Optional, List

from sqlalchemy import or_

from chronicler.discord.embed import Embed, EmbedField, EmbedFooter
from chronicler.models.attribute import Attribute
from chronicler.parser import SubParser
from chronicler.models.base import session
from chronicler.models.ability import Ability, Classification, Shout, Word, WordCost, WordAttribute
from chronicler.parser import verify_author_and_channel, verify_open_channel

logger = getLogger(__name__)


def add_shout_parsers(subparsers: SubParser):
    parser = subparsers.add_parser('shout', description='Functions for managing shouts.', aliases=['shouts', 'Shouts', 'Shout'])
    sub: SubParser = parser.add_subparsers()
    for func in [create, view, cost, list_, word_, attribute_, edit]:
        sub.add_parser_from_function(func)


@verify_author_and_channel
async def attribute_(shout: Shout, word: str, attribute: Attribute, magnitude: int):
    """
    Adds **attribute** to **word**. When this **word** is the last word used in **shout**, **attribute** counts as being **magnitude** higher.
    :param shout: Shout the word belongs to
    :param word: The word which is to get the attribute
    :param attribute: Which attribute is higher when the word is used.
    :param magnitude: How much the attribute is higher
    """
    word = session.query(Word).filter(Word.shout == shout, Word.name == word).one()
    attrib: WordAttribute = session.query(WordAttribute).filter(WordAttribute.word == word, WordAttribute.attribute == attribute).one_or_none()
    if attrib is None:
        session.add(WordAttribute(word=word, attribute=attribute, value=magnitude))
    else:
        attrib.value = magnitude

    session.commit()
    return {'content': f'Using {word.name} ({shout.name}) now acts as if {attribute.name} is {magnitude} higher.'}


@verify_author_and_channel
async def cost(shout: Shout, word: str, attribute: Attribute, magnitude: int):
    """
    Adds a cost to an word of power, for when that word is the final one.
    :param shout: Which shout the word belongs to
    :param word: What word should get a cost
    :param attribute: What attribute the cost should be paid in.
    :param magnitude: How much of that attribute does the ability cost
    """
    word = session.query(Word).filter(Word.shout == shout, Word.name == word).one()
    cost_: WordCost = session.query(WordCost).filter(WordCost.word == word, WordCost.attribute == attribute).one_or_none()
    if cost_ is None:
        session.add(WordCost(word=word, attribute=attribute, magnitude=magnitude))
    else:
        cost_.magnitude = magnitude

    session.commit()
    return {'content': f'Successfully set the {attribute.name} cost of {word.name} to {magnitude}'}


@verify_open_channel
async def list_(*args, filter_: Optional[str] = None):
    """
    Lists all shouts
    :param filter_: Exclude results not like %this%.
    """
    page = int(args[0]) - 1 if args else 0
    items_per_page = 10
    num_shouts = session.query(Shout).filter(Shout.name.like(f'%{filter_}%')).count() if filter_ else session.query(Shout).count()
    total_pages = max((num_shouts // items_per_page) + ((num_shouts % items_per_page) != 0), 1)  # Don't need to add 1 if it divides exactly.
    if 0 > page or page >= total_pages:
        return {'content': f'Invalid page ID. Please use 1 - {total_pages}'}

    query = session.query(Shout)
    if filter_:
        query = query.filter(Shout.name.like(f'%{filter_}%'))

    abilities: List[Shout] = query.offset(page * items_per_page).limit(items_per_page).all()

    embed = Embed(
        title='Abilities',
        footer=EmbedFooter(text=f'Page: {page + 1}/{total_pages}'),
        fields_=[]
    )
    for ability in abilities:
        embed.fields_.append(EmbedField(
            name=ability.type_and_name,
            value=ability.render_description,
        ))

    return {'embed': embed}


@verify_open_channel
async def view(shout: Shout):
    """
    Views details of a shout.
    :param shout: Shout to view
    """
    embed = Embed(
        title=shout.type_and_name,
        description=shout.render_description,
        fields_=[],
    )
    if shout.words:
        embed.fields_.append(EmbedField(
            name='Words of Power',
            value=f'**{" ".join(word.translation for word in shout.words)}**\n*{" - ".join(word.name for word in shout.words)}*'
        ))
    for word in sorted(shout.words, key=lambda word: word.word_id):
        desc = [f'Attack Speed: {1 / word.swing_duration:.2f} attacks per second.']
        if word.duration is not None:
            desc.append(f'Duration: {word.duration} seconds.')
        if word.magnitude:
            desc.append(f'Base Strength: {word.magnitude}')
        if word.magnitude_soul_scale:
            desc.append(f'Bonus Strength: {word.magnitude_soul_scale} * [Number of Dragon Souls]')
        if word.radius:
            desc.append(f'Area: {word.radius}.')
        if word.divinity:
            desc.append(f'Divinity: {word.divinity}')
        newline = '\n - '
        desc.append(f'Cost: \n - {newline.join(f"{word_cost.magnitude} {word_cost.attribute.name}" for word_cost in word.costs)}')
        if word.attributes:
            desc.append(f'Attribute increases when {word.name} is the final word:\n'
                        f'{newline.join(f"{word_attr.attribute.name}: {word_attr.value}" for word_attr in word.attributes)}')
        embed.fields_.append(EmbedField(
            name=f'**{word.translation}** - *{word.name}*',
            value='\n'.join(desc)
        ))
    embed.fields_.append(EmbedField(
        name='Complexity',
        value=f'This shout requires {shout.difficulty} Dragon Souls for each word.',
    ))

    embed.footer = EmbedFooter(  # @TODO: Give the exact command to use when calculating how a character uses an ability.
        text='To view how shouts look when used by a specific character, use `character shouts [name]`'
    )

    return {'embed': embed}


@verify_author_and_channel
async def word_(
        shout: Shout,
        name: str,
        translation: Optional[str],
        swing_duration: Optional[float],
        duration: Optional[int],
        magnitude: Optional[int],
        magnitude_soul_scale: Optional[float],
        divinity: Optional[int],
        radius: Optional[float],
):
    """
    Adds a word to a shout, or modifies it.
    :param shout: What Shout to add the word to, eg, "Fire Breath". Never Optional, since a word.name can be part of multiple shouts.
    :param name: What word to add, in english. eg, "Fire", "Inferno" or "Sun"
    :param translation: Dovahzul translation. eg, "Yol", "Toor", "Shul"
    :param swing_duration: How long it takes to pronounce this and all previous words.
    :param duration: How long any long-lasting effects last, like burning duration, or the zombie lifetime from Soul Tear.
    :param magnitude: How much primary effect the shout has, like damage (Fire Breath) or level cap increase (Calm, Bend Will)
    :param magnitude_soul_scale: How much the magnitude scales with Shout.scale_attribute
    :param divinity: Divinity rating of the shout being used with this as the last word.
    :param radius: Area of effect of the word.
    """
    word = session.query(Word).filter(Word.name == name, Word.shout == shout).one_or_none()
    new = word is None
    if word is None:
        word = Word(shout=shout, word_id=len(shout.words), translation=translation, name=name)
        session.add(word)
    elif translation is not None:
        word.translation = translation

    if swing_duration is not None:
        word.swing_duration = swing_duration

    if duration is not None:
        word.duration = duration

    if magnitude is not None:
        word.magnitude = magnitude

    if magnitude_soul_scale is not None:
        word.magnitude_soul_scale = magnitude_soul_scale

    if divinity is not None:
        word.divinity = divinity

    if radius is not None:
        word.radius = radius

    session.commit()
    if new:
        return {'content': f'Successfully attached {word.name} to {shout.name}'}
    else:
        return {'content': f'Successfully updated {word.name}'}


@verify_author_and_channel
async def edit(
        shout: Shout,
        description: Optional[str],
        difficulty: Optional[str],
        classification: Optional[Classification],
        remove_classification: bool,
        new_name: Optional[str],
):
    """
    Edits a shout. Omitted values are unchanged.
    :param shout: Name of the shout, e.g. "Fire Breath"
    :param description: Description of the shout.
    :param difficulty: How many dragon souls are required for each word.
    :param classification: Classification of the shout, e.g. "Fire".
    :param remove_classification: Ignore the classification parameter, and remove the classification instead.
    :param new_name: Change the shout's name.
    """
    resp = []
    if new_name is not None:
        resp.append(f'{shout.name} Name changed to {new_name}')
        shout.name = new_name

    if description is not None:
        resp.append(f'{shout.name} Description changed to {description}')
        shout.description = description

    if difficulty is not None:
        resp.append(f'{shout.name} Difficulty changed to {difficulty}')
        shout.difficulty = difficulty

    if classification is not None and not remove_classification:
        resp.append(f'{shout.name} classification changed to {classification.name}')
        shout.classification = classification
    elif remove_classification:
        resp.append(f'{shout.name} classification removed.')
        shout.classification = None

    session.commit()
    return {'content': '\n'.join(resp)}


@verify_author_and_channel
async def create(
        name: str,
        description: str,
        difficulty: int,
        classification: Optional[Classification] = None,
):
    """
    Creates a new shout.
    :param name: Name of the Shout
    :param description: Description. Supports {}-enclosed attributes magnitude, attack_speed, duration and cost.
    :param difficulty: How many Dragon Souls a Character needs to use each word.
    :param classification: What classification the ability has, which affects which attributes affect damage, resistance, cost, duration and casting time.
    """
    if session.query(Shout).filter(Shout.name == name).one_or_none() is not None:
        return {'content': f'Shout "{name}" already exists.'}
    session.add(Shout(
        name=name,
        description=description,
        difficulty=difficulty,
        classification=classification
    ))
    session.commit()
    return {'content': f'Created shout {name} successfully.'}

"""
Manage bot config for the server.
"""
from enum import Enum, EnumMeta
from logging import getLogger
from typing import List, Optional

from sqlalchemy.orm.exc import MultipleResultsFound, NoResultFound

from chronicler import renderers
from chronicler.config import config
from chronicler.parser import SubParser, FromMessage, verify_server_owner, Choices
from chronicler.discord.user import User
from chronicler.discord.channel import Channel
from chronicler.discord.guild import Guild
from chronicler.models.author import Author, AuthorChannel, OpenChannel, ServerGlobal, ChannelRenderer
from chronicler.models.base import session
from chronicler.discord.embed import Embed, EmbedField
from chronicler.discord.snowflake import Snowflake
from chronicler.discord.cache import cache
import chronicler.tools  # Need to be sure to import this to build the renderers dictionary and fill it.

logger = getLogger(__name__)


def add_config_parsers(subparsers: SubParser):
    parser = subparsers.add_parser('config', description='Functions for managing what the bot.')
    sub: SubParser = parser.add_subparsers()
    for func in [add_author, add_channel, view, enable_channel, disable_channel, set_prefix, renderer_, set_logging_channel]:
        sub.add_parser_from_function(func)


@verify_server_owner
async def set_prefix(prefix: str, guild_id: Snowflake = FromMessage.Guild):
    """
    Sets a new prefix for use on this server.
    :param prefix: New prefix to use.
    :param guild_id: guild_id to use the prefix for.
    """
    old = session.query(ServerGlobal).filter(ServerGlobal.guild_id == guild_id, ServerGlobal.setting == 'prefix').one_or_none()
    if old is None:
        session.add(ServerGlobal(guild_id=guild_id, value=prefix, setting='prefix'))
    else:
        old.value = prefix

    session.commit()
    return {'content': f'Successfully set new prefix to {prefix}'}


@verify_server_owner
async def add_author(
        user: User,
        guild_id: Snowflake = FromMessage.Guild):
    """
    Adds an author to the server
    :param user: Author to add (Mention)
    :param guild_id: adsf
    """
    db_author = Author(user_id=user.id, guild_id=guild_id)
    session.add(db_author)
    session.commit()
    return {'content': f'Successfully registered {user.mention} as an author'}


@verify_server_owner
async def add_channel(author: User, channel: Channel, guild_id: Snowflake = FromMessage.Guild):
    """
    Adds this channel to an author, for use in creating a character.
    :param author: Author who gets to use this channel
    :param channel: asdf
    :param guild_id: afds
    """
    guild = cache[Guild, guild_id]
    try:
        db_author = session.query(Author).filter(Author.guild_id == guild.id, Author.user_id == author.id).one()
    except NoResultFound:
        return {'content': f'Author {nickname_in_guild(author, guild)} is not a registered author on this server.'}

    session.add(AuthorChannel(author_id=db_author.entity_id, channel_id=channel.id))
    session.commit()
    return {'content': f'Successfully opened {channel.mention} as channel for {nickname_in_guild(author, guild)} to work in.'}


@verify_server_owner
async def enable_channel(channel: Channel):
    """
    Enables this channel for anyone to use for public endpoints.
    Chronicler uses a whitelist system, so all channels are disabled unless specifically configured otherwise.
    :param channel: Channel to enable.
    """
    session.add(OpenChannel(channel_id=channel.id))
    session.commit()
    return {'content':  f'{channel.mention} is now open for the public.'}


@verify_server_owner
async def disable_channel(channel: Channel):
    """
    Removes this channel from open channels, which means that the public cannot make requests of the bot.
    Also removes it as author channel, so that characters can no longer be created/edited inside of it.
    :param channel: Channel to disable.
    """
    ch = session.query(OpenChannel).filter(OpenChannel.channel_id == channel.id).one_or_none()
    if ch is not None:
        session.delete(ch)

    auth_chans = session.query(AuthorChannel).filter(AuthorChannel.channel_id == channel.id).all()
    for auth_chan in auth_chans:
        session.delete(auth_chan)

    session.commit()
    return {'content':  f'{Channel.mention_from_id(channel.id)} is now closed.'}


@verify_server_owner
async def view(guild_id: Snowflake = FromMessage.Guild):
    """
    Display authors and the channels they have to work in.
    """
    try:
        guild = cache[Guild, guild_id]
    except KeyError:
        from pprint import pprint
        pprint(cache.entities)
        raise

    embed = Embed(
        title='Authors',
        description='Shows which authors are using characters being tracked by Chronicler on this server, and which channels are open.',
        fields_=[],
    )
    prefix = session.query(ServerGlobal).filter(ServerGlobal.guild_id == guild_id, ServerGlobal.setting == 'prefix').one_or_none()
    if prefix is None:
        prefix = config['main']['server_prefix']
    else:
        prefix = prefix.value
    embed.fields_.append(EmbedField(
        name='Server Prefix',
        value=f'`{prefix}`'
    ))

    authors = session.query(Author).filter(Author.guild_id == guild_id).all()
    if len(authors) < 19:  # Discord max number of embed fields limit
        for author in authors:
            embed.fields_.append(EmbedField(
                name=nickname_in_guild(cache[User, author.user_id], guild),
                value='\n'.join(Channel.mention_from_id(author_channel.channel_id) for author_channel in author.channels) or 'No Channels assigned'
            ))
    else:
        embed.fields_.append(EmbedField(
            name='Authors:',
            value='\n'.join(f'{nickname_in_guild(cache[User, author.user_id], guild)}:'
                            f'{"".join(Channel.mention_from_id(author_channel.channel_id) for author_channel in author.channels)}'
                            for author in authors)

        ))
    guild_ch = tuple(int(channel.id) for channel in guild.channels)
    print(f'Guild Channels: {guild_ch}')
    opens = session.query(OpenChannel).filter(OpenChannel.channel_id.in_(guild_ch)).all()
    ch_string = '\n'.join(Channel.mention_from_id(open_.channel_id) for open_ in opens)
    if ch_string:
        embed.fields_.append(EmbedField(name='Public Channels', value=ch_string))

    return {'embed': embed}


Renderer = Enum('Renderer', names=tuple(renderers.keys()), type=Choices)


async def set_logging_channel(level: int, channel_id: List[Snowflake] = FromMessage.Channel_Mentions, guild_id: Snowflake = FromMessage.Guild):
    """
    Designate a logging channel for this server.
    :param level: Like Python, CRITICAL = 50, ERROR 40, WARNING 30, INFO 20, DEBUG 10
    :param channel_id: Channel to register as dedicated logging channel.
    :param guild_id: Guild ID.
    """
    channel_id = channel_id[0]  # Channel_Mentions returns a list.
    chan_setting = session.query(ServerGlobal).filter(ServerGlobal.guild_id == guild_id, ServerGlobal.setting == 'LogChannel').one_or_none()
    chan_loglevel = session.query(ServerGlobal).filter(ServerGlobal.guild_id == guild_id, ServerGlobal.setting == 'LogLevel').one_or_none()

    if chan_setting is None:
        chan_setting = ServerGlobal(
            guild_id=guild_id,
            setting='LogChannel',
            value=channel_id,
        )
        session.add(chan_setting)
    else:
        chan_setting.value = channel_id

    if chan_loglevel is None:
        chan_loglevel = ServerGlobal(
            guild_id=guild_id,
            setting='LogLevel',
            value=level,
        )
        session.add(chan_loglevel)
    else:
        chan_loglevel.value = level

    session.commit()
    from chronicler.main import client  # Not like we're keeping best practices elsewhere.
    await client.configure_logger(channel_id, level)


async def renderer_(renderer_id: Optional[Renderer], channel_id: Snowflake = FromMessage.Channel, show_options: bool = False):
    """
    Sets a renderer for a channel. This renderer will do post-processing on the return message, for example converting the Markdown to html.
    :param channel_id: Current Channel
    :param renderer_id: Renderer to use. Omit to delete current renderer.
    :param show_options: Shows possible renderer values.
    """
    if show_options:
        return {'content': f'Valid renderers: { {renderer.value: renderer.name for renderer in Renderer}}'}
    renderer = session.query(ChannelRenderer).filter(ChannelRenderer.channel_id == channel_id).one_or_none()
    renderer_name = Renderer(renderer_id).name
    if renderer_name is not None:
        if not renderer:
            renderer = ChannelRenderer(channel_id=channel_id, renderer=renderer_name)
            session.add(renderer)
        else:
            renderer.renderer = renderer_name.name
        session.commit()
        return {'content': f'Set the renderer for this channel to {renderer_name}'}
    elif renderer and renderer_name is None:
        session.delete(renderer)
        session.commit()
        return {'content': f'Deleted renderer for this channel, so default rendering is used.'}
    else:
        return {'content': 'Cannot delete a renderer that doesn\'t exist.'}


def nickname_in_guild(user: User, guild: Guild):
    if guild.members is not None:
        for member in guild.members:
            if member.user.id == user.id:
                return member.nick

    return user.username


"""Creates the parser."""
from typing import Optional

from chronicler.discord.embed import Embed, EmbedFooter
from chronicler.endpoints.ability import add_ability_parsers
from chronicler.endpoints.character import add_character_parsers
from chronicler.endpoints.classification import add_classification_parsers
from chronicler.endpoints.npc import add_npc_parsers
from chronicler.endpoints.shout import add_shout_parsers
from chronicler.endpoints.skill import add_skill_parsers
from chronicler.models.base import session
from chronicler.models.info import Infobox
from chronicler.parser import Parser
from chronicler.endpoints.config import add_config_parsers
from chronicler.endpoints.attribute import add_attribute_parsers
from chronicler.endpoints.perk import add_perk_parsers
from chronicler.endpoints.class_ import add_class_parsers
from chronicler.endpoints.chapter import add_chapter_parsers


def create_parser():
    parser = Parser('', description='Chronicler is a bot that keeps track of characters.')
    subs = parser.add_subparsers(title='Categories')
    add_config_parsers(subs)
    add_character_parsers(subs)
    add_attribute_parsers(subs)
    add_perk_parsers(subs)
    add_class_parsers(subs)
    add_skill_parsers(subs)
    add_ability_parsers(subs)
    add_shout_parsers(subs)
    add_classification_parsers(subs)
    add_chapter_parsers(subs)
    add_npc_parsers(subs)
    for func in [info, list_info, info_create]:
        subs.add_parser_from_function(func)
    return parser


async def list_info(*args, filter_: Optional[str]):
    """
    Lists info topics.
    """
    page = int(args[0])-1 if args else 0
    items_per_page = 20
    num_topics = session.query(Infobox).filter(Infobox.topic.like(f'%{filter_}%')).count() if filter_ else session.query(Infobox).count()
    total_pages = max((num_topics // items_per_page) + ((num_topics % items_per_page) != 0), 1)  # Don't need to add 1 if it divides exactly.
    if 0 > page or page >= total_pages:
        return {'content': f'Invalid page ID. Please use 1 - {total_pages}'}

    query = session.query(Infobox)
    if filter_:
        query = query.filter(Infobox.topic.like(f'%{filter_}%'))

    topics = query.offset(page * items_per_page).limit(items_per_page).all()
    embed = Embed(
        title='Information Topics',
        description='\n'.join(f' - {topic}' for topic in topics),
        footer=EmbedFooter(text=f'Page: {page+1}/{total_pages}')
    )
    return {'embed': embed}


async def info(topic: Infobox):
    """
    Displays information about a certain topic.
    :param topic: What the information is about.
    """
    return {'embed': Embed(
        title=topic.topic,
        description=topic.content
    )}


async def info_create(topic: str, content: str):
    """
    Creates a new infobox topic.
    :param topic: Name of the Infobox.
    :param content: Explanation
    """
    if topic in {info_.topic for info_ in session.query(Infobox).add_columns(Infobox.topic).all()}:
        return {'content': f'Info topic {topic} already exists.'}
    session.add(Infobox(topic=topic, content=content))
    session.commit()
    return {'content': f'Successfully created info topic {topic}'}

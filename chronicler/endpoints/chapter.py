"""Manage NPCs"""

from logging import getLogger
from typing import Optional, List, Dict

from sqlalchemy import desc

from chronicler.discord.embed import Embed, EmbedField, EmbedFooter
from chronicler.models.ability import Shout, CharacterShout, Ability
from chronicler.models.attribute import FrozenAttribute
from chronicler.models.character import Character, FrozenCharacter, FrozenRelationship
from chronicler.models.class_ import FrozenClass
from chronicler.models.item import Item, Armor, FrozenWeapon
from chronicler.models.skill import Skill, FrozenSkill
from chronicler.parser import SubParser
from chronicler.models.base import session
from chronicler.parser import verify_open_channel, verify_author_and_channel

logger = getLogger(__name__)


def add_chapter_parsers(subparsers: SubParser):
    parser = subparsers.add_parser('chapter', description='Functions for managing Chapter snapshots of characters.',
                                   aliases=['Chapter', 'chap', 'Chap'])
    sub: SubParser = parser.add_subparsers()
    for func in [view, list_, perks_, attributes_, abilities_, classes_, skills_, shouts_, relationships_, weapons_, inventory, armor_, new]:
        sub.add_parser_from_function(func)


@verify_author_and_channel
async def new(bookmark: str, character: Character):
    """
    Creates a new bookmark, saving a snapshot of the character's current state.
    :param bookmark: Name of the bookmark.
    :param character: Which character.
    """
    character.freeze(bookmark)
    session.commit()
    return {'content': f'Successfully made bookmark {bookmark} of {character.full_name}'}


@verify_open_channel
async def inventory(bookmark: str, character: Character, *args, filter_: Optional[str]):
    """
    Shows all items the character has. Does not display weapons or armor.
    :param bookmark: Bookmark number
    :param character: Whose inventory to view
    :param args: Page number.
    :param filter_: Filter by item name.
    """
    frozen_character: FrozenCharacter = session.query(FrozenCharacter).filter(FrozenCharacter.character == character, FrozenCharacter.identifier == bookmark).one()
    page = int(args[0])-1 if args else 0
    items_per_page = 10
    items = list(frozen_character.items)
    num_items = len(items)
    pages, remainder = divmod(num_items, items_per_page)
    total_pages = (pages + remainder != 0) or 1  # Don't need to add 1 if it divides exactly.
    if 0 > page or page >= total_pages:
        return {'content': f'Invalid page ID. Please use 1 - {total_pages}'}

    if filter_:
        items = [item for item in items if filter_ in item.name]

    items = items[page * items_per_page:(1 + page) * items_per_page]

    embed = Embed(
        title=f'Inventory of {frozen_character.full_name}',
        description='\n'.join(f'{item.name}: {item.amount}' for item in items),
    )
    return {'embed': embed}


@verify_open_channel
async def classes_(bookmark: str, character: Character, *args, filter_: Optional[str]):
    """
    Displays all of a character's classes.
    :param bookmark: Bookmark number
    :param character: Character to inspect
    :param args: Page number, if more than 10 exist. Pages of the same character are as consistent as the filter.
    :param filter_: Filter by name.
    """
    frozen_character: FrozenCharacter = session.query(FrozenCharacter).filter(FrozenCharacter.character == character, FrozenCharacter.identifier == bookmark).one()
    page = int(args[0])-1 if args else 0
    items_per_page = 10
    classes = list(frozen_character.classes)
    num_classes = len(classes)
    total_pages = max((num_classes // items_per_page) + ((num_classes % items_per_page) != 0), 1)  # Don't need to add 1 if it divides exactly.
    if 0 > page or page >= total_pages:
        return {'content': f'Invalid page ID. Please use 1 - {total_pages}'}

    if filter_:
        classes = [char_class for char_class in classes if filter_ in any(inf.name for inf in char_class.class_.info)]

    classes = classes[page * items_per_page:(1 + page) * items_per_page]

    embed = Embed(
        title=f'Classes of {frozen_character.full_name}',
        description='',
        fields_=[],
        footer=EmbedFooter(text=f'Page: {page + 1}/{total_pages}')
    )
    skill_levels: Dict[Skill, int] = {char_skill.skill: char_skill.level for char_skill in frozen_character.skills}

    for char_class in classes:
        lines = [char_class.description]
        if char_class.class_.skills:
            lines.append('Skills:')
            lines.extend(f'- {skill.name} - Level {skill_levels[skill]}' for skill in char_class.class_.skills)

        titles = list(char_class.titles)
        if titles:
            lines.append('Titles')
            lines.extend(f'- {title}' for title in titles)

        embed.fields_.append(EmbedField(
            name=f'{char_class.title} - Level {char_class.level}',
            value='\n'.join(lines)
        ))
    return {'embed': embed}


@verify_open_channel
async def relationships_(bookmark: str, character: Character, *args, filter_: Optional[str], reverse: bool):
    """
    Shows all relationships a character has.
    :param bookmark: Bookmark number
    :param character: Character to inspect
    :param args: Page number, if more than 10 exist. Pages of the same character are as consistent as the filter.
    :param filter_: Filter by name.
    :param reverse: Reverse ordering, putting lowest values on top instead of highest.
    """
    frozen_character: FrozenCharacter = session.query(FrozenCharacter).filter(FrozenCharacter.character == character, FrozenCharacter.identifier == bookmark).one()
    del character
    page = int(args[0])-1 if args else 0
    items_per_page = 10
    number = session.query(FrozenRelationship).filter(FrozenRelationship.name.like(f'%{filter_}%'), FrozenRelationship.character == frozen_character).count() \
        if filter_ else session.query(FrozenRelationship).filter(FrozenRelationship.character == frozen_character).count()
    total_pages = max((number // items_per_page) + ((number % items_per_page) != 0), 1)  # Don't need to add 1 if it divides exactly.
    if 0 > page or page >= total_pages:
        return {'content': f'Invalid page ID. Please use 1 - {total_pages}'}

    query = session.query(FrozenRelationship).order_by(FrozenRelationship.value if reverse else desc(FrozenRelationship.value))
    if filter_:
        query = query.filter(FrozenRelationship.name.like(f'%{filter_}%'))

    relationships: List[FrozenRelationship] = query.offset(page * items_per_page).limit(items_per_page).all()
    embed = Embed(
        title=f'{frozen_character.full_name}\'s Relationships',
        description='\n'.join(f'{rel.value} {"Affection from" if rel.sexual else "Reputation with"} {rel.name}' for rel in relationships),
        footer=EmbedFooter(text=f'Page: {page + 1}/{total_pages}')
    )
    return {'embed': embed}


@verify_open_channel
async def skills_(bookmark: str, character: Character, *args, filter_: Optional[str]):
    """
    Displays all of a character's skills.
    :param bookmark: Bookmark number
    :param character: Character to inspect
    :param args: Page number, if more than 10 exist. Pages of the same character are as consistent as the filter.
    :param filter_: Filter by name.
    """
    frozen_character: FrozenCharacter = session.query(FrozenCharacter).filter(FrozenCharacter.character == character, FrozenCharacter.identifier == bookmark).one()
    del character
    page = int(args[0])-1 if args else 0
    items_per_page = 10
    skills = list(frozen_character.skills)
    num_skills = len(skills)
    total_pages = max((num_skills // items_per_page) + ((num_skills % items_per_page) != 0), 1)  # Don't need to add 1 if it divides exactly.
    if 0 > page or page >= total_pages:
        return {'content': f'Invalid page ID. Please use 1 - {total_pages}'}

    if filter_:
        skills = [skill for skill in skills if filter_ in skill.name]

    skills = skills[page * items_per_page:(1 + page) * items_per_page]

    embed = Embed(
        title=f'Skills of {frozen_character.full_name}',
        description='',
        fields_=[],
        footer=EmbedFooter(text=f'Page: {page + 1}/{total_pages}')
    )
    for char_skill in skills:
        skill = char_skill.skill
        embed.fields_.append(EmbedField(
            name=f'{skill.name} - Level {char_skill.level}/{skill.level_max}' if skill.level_max else f'{skill.name} - Level {char_skill.level}',
            value=skill.description
        ))
    return {'embed': embed}


@verify_open_channel
async def shouts_(bookmark: str, character: Character, *args, filter_: Optional[str]):
    """
    Displays all of a character's shouts.
    :param bookmark: Bookmark number
    :param character: Character to inspect
    :param args: Page number, if more than 10 exist. Pages of the same character are as consistent as the filter.
    :param filter_: Filter by name.
    """
    frozen_character: FrozenCharacter = session.query(FrozenCharacter).filter(FrozenCharacter.character == character, FrozenCharacter.identifier == bookmark).one()
    del character
    page = int(args[0])-1 if args else 0
    items_per_page = 10
    shouts = list(frozen_character.shouts)
    num_shouts = len(shouts)
    total_pages = max((num_shouts // items_per_page) + ((num_shouts % items_per_page) != 0), 1)  # Don't need to add 1 if it divides exactly.
    if 0 > page or page >= total_pages:
        return {'content': f'Invalid page ID. Please use 1 - {total_pages}'}

    if filter_:
        shouts = [shout for shout in shouts if filter_ in shout.name or any(filter_ in word.name or filter_ in word.translation for word in shout.words)]

    shouts: List[CharacterShout] = shouts[page * items_per_page:(1 + page) * items_per_page]
    primary_lines = []
    if frozen_character.dragonsouls:
        primary_lines.append(f'Dragon Souls: {frozen_character.dragonsouls}')

    embed = Embed(
        title=f'Shouts of {frozen_character.full_name}',
        description='\n'.join(primary_lines),
        fields_=[],
        footer=EmbedFooter(text=f'Page: {page + 1}/{total_pages}')
    )
    for char_shout in shouts:
        shout: Shout = char_shout.shout
        lines = [
            f'**{" ".join(word.translation for word in shout.words)}**\n*{" - ".join(word.name for word in shout.words)}*',
            shout.description_for_character(frozen_character),
        ]
        if shout.cost(frozen_character):
            lines.append('Cost:')
            lines.extend(f'- {attribute.name}: **{value}**' for attribute, value in shout.cost(frozen_character).items())
        if shout.divinity(frozen_character):
            lines.append(f'Divinity: {shout.divinity(frozen_character)}')
        embed.fields_.append(EmbedField(
            name=shout.type_and_name,
            value='\n'.join(lines)
        ))
    return {'embed': embed}


@verify_open_channel
async def abilities_(bookmark: str, character: Character, *args, filter_: Optional[str]):
    """
    Shows all abilities that belong to a character.
    :param bookmark: Bookmark number
    :param character: Character to inspect
    :param args: Page number, if more than 10 exist. Pages of the same character are as consistent as the filter.
    :param filter_: Filter by name.
    """
    frozen_character: FrozenCharacter = session.query(FrozenCharacter).filter(FrozenCharacter.character == character, FrozenCharacter.identifier == bookmark).one()
    del character
    page = int(args[0])-1 if args else 0
    items_per_page = 10
    abilities = list(frozen_character.abilities)
    num_abil = len(abilities)
    total_pages = max((num_abil // items_per_page) + ((num_abil % items_per_page) != 0), 1)  # Don't need to add 1 if it divides exactly.
    if 0 > page or page >= total_pages:
        return {'content': f'Invalid page ID. Please use 1 - {total_pages}'}

    if filter_:
        abilities = [ability for ability in abilities if filter_ in ability.name]

    abilities = abilities[page * items_per_page:(1 + page) * items_per_page]
    embed = Embed(
        title=f'Abilities of {frozen_character.full_name}',
        description='',
        fields_=[],
        footer=EmbedFooter(text=f'Page: {page + 1}/{total_pages}')
    )
    for frozen_ability in abilities:
        ability: Ability = frozen_ability.ability
        lines = [ability.description_for_character(frozen_character)]
        if ability.costs:
            lines.append('Cost:')
            lines.extend(f'- {attribute.name}: **{int(value)}**' for attribute, value in ability.cost(frozen_character).items())
        embed.fields_.append(EmbedField(
            name=ability.name if ability.classification is None else f'{ability.classification.name}: {ability.name}',
            value='\n'.join(lines)
        ))
    return {'embed': embed}


@verify_open_channel
async def attributes_(bookmark: str, character: Character, *args, filter_: Optional[str]):
    """
    Shows the attributes a given character has.
    :param bookmark: Bookmark number
    :param character: Character to inspect
    :param args: Page number, if more than 10 exist. Pages of the same character are as consistent as the filter.
    :param filter_: Filter by name.
    """
    frozen_character: FrozenCharacter = session.query(FrozenCharacter).filter(FrozenCharacter.character == character, FrozenCharacter.identifier == bookmark).one()
    del character
    page = int(args[0])-1 if args else 0
    items_per_page = 20
    attributes = sorted(frozen_character.attributes.attributes.items(), key=lambda item: item[1], reverse=True)
    attributes = [attr for attr in attributes if attr[1] != 0]  # 0-valued isn't interesting.
    num_attr = len(attributes)
    total_pages = max((num_attr // items_per_page) + ((num_attr % items_per_page) != 0), 1)  # Don't need to add 1 if it divides exactly.
    if 0 > page or page >= total_pages:
        return {'content': f'Invalid page ID. Please use 1 - {total_pages}'}

    if filter_:
        attributes = [(attribute, value) for attribute, value in attributes if filter_ in attribute.name]

    attributes = attributes[page * items_per_page:(1 + page) * items_per_page]
    embed = Embed(
        title=f'Attributes of {frozen_character.name}',
        description='\n'.join(f'**{attribute.name}**: {int(value)}' for attribute, value in attributes),
        footer=EmbedFooter(text=f'Page: {page + 1}/{total_pages}')
    )
    return {'embed': embed}


@verify_open_channel
async def perks_(bookmark: str, character: Character, *args, filter_: Optional[str]):
    """
    Shows the perks of a given Character.
    :param bookmark: Bookmark number
    :param character: Character to inspect
    :param args: Page number, if more than 10 exist. Pages of the same character are as consistent as the filter.
    :param filter_: Filter by name.
    """
    frozen_character: FrozenCharacter = session.query(FrozenCharacter).filter(FrozenCharacter.character == character, FrozenCharacter.identifier == bookmark).one()
    del character
    page = int(args[0])-1 if args else 0
    items_per_page = 20
    perks = [frozen_perk.perk for frozen_perk in frozen_character.perks]
    num_perks = len(perks)
    total_pages = max((num_perks // items_per_page) + ((num_perks % items_per_page) != 0), 1)  # Don't need to add 1 if it divides exactly.
    if 0 > page or page >= total_pages:
        return {'content': f'Invalid page ID. Please use 1 - {total_pages}'}

    if filter_:
        perks = [perk.perk for perk in perks if filter_ in perk.perk.name or filter_ in perk.perk.description]

    perks = perks[page * items_per_page:(1 + page) * items_per_page]
    embed = Embed(
        title=f'Perks of {frozen_character.full_name}',
        description='\n'.join(f'**{perk.name}**: {perk.description or ""}' for perk in perks),
        footer=EmbedFooter(text=f'Page: {page + 1}/{total_pages}')
    )
    return {'embed': embed}


@verify_open_channel
async def weapons_(bookmark: str, character: Character, *args, filter_: Optional[str], equipped: bool):
    """
    Shows the weapons of a given Character.
    :param bookmark: Bookmark number
    :param character: Character to inspect
    :param args: Page number, if more than 20 exist. Pages of the same character are as consistent as the filter.
    :param filter_: Filter by name.
    :param equipped: Only display equipped weapons.
    """
    frozen_character: FrozenCharacter = session.query(FrozenCharacter).filter(FrozenCharacter.character == character, FrozenCharacter.identifier == bookmark).one()
    del character
    page = int(args[0])-1 if args else 0
    items_per_page = 20
    num_weapons = session.query(FrozenWeapon).filter(FrozenWeapon.name.like(f'%{filter_}%'), FrozenWeapon.owner == frozen_character).count() if filter_ else \
        session.query(FrozenWeapon).filter(FrozenWeapon.owner == frozen_character).count()
    total_pages = max((num_weapons // items_per_page) + ((num_weapons % items_per_page) != 0), 1)  # Don't need to add 1 if it divides exactly.
    if 0 > page or page >= total_pages:
        return {'content': f'Invalid page ID. Please use 1 - {total_pages}'}

    query = session.query(FrozenWeapon).filter(FrozenWeapon.owner == frozen_character)
    if filter_:
        query = query.filter(FrozenWeapon.name.like(f'%{filter_}%'))

    if equipped:
        query = query.filter(FrozenWeapon.equipped == True)

    query = query.order_by(desc(FrozenWeapon.equipped))

    weapons: List[FrozenWeapon] = query.offset(page * items_per_page).limit(items_per_page).all()
    embed = Embed(
        title=f'{frozen_character.full_name}\'s Weapons',
        description=f'Weapons of {frozen_character.full_name}',
        fields=[],
        footer=EmbedFooter(text=f'Page: {page + 1}/{total_pages}')
    )
    for weapon in weapons:
        lines = [weapon.description]
        for ability in weapon.enchanted_abilities():
            lines.append(f'{ability.name}\n- {ability.description_for_character(frozen_character)}')
        embed.fields_.append(EmbedField(
            name=f'{weapon.name} (Equipped)' if weapon.equipped else weapon.name,
            value='\n'.join(lines)
        ))

    return {'embed': embed}


@verify_open_channel
async def list_(*args, character: Optional[Character]):
    """
    Shows a list of all characters.
    :param args: Page number, if more than 10 exist. Pages of the same character are as consistent as the filter.
    :param character: Filter by Character.
    """
    page = int(args[0])-1 if args else 0
    items_per_page = 20
    num_chars = session.query(FrozenCharacter).filter(FrozenCharacter.character == character).count() if character is not None else \
        session.query(FrozenCharacter).count()
    total_pages = max((num_chars // items_per_page) + ((num_chars % items_per_page) != 0), 1)  # Don't need to add 1 if it divides exactly.
    if 0 > page or page >= total_pages:
        return {'content': f'Invalid page ID. Please use 1 - {total_pages}'}

    query = session.query(FrozenCharacter)
    if character is not None:
        query = query.filter(FrozenCharacter.character == character)

    characters: List[FrozenCharacter] = query.offset(page * items_per_page).limit(items_per_page).all()
    embed = Embed(
        title='Character List',
        description='\n'.join(f' - **{char.full_name}**: Level {char.level or ""}' for char in characters),
        footer=EmbedFooter(text=f'Page: {page + 1}/{total_pages}')
    )
    return {'embed': embed}


@verify_open_channel
async def view(bookmark: str, character: Character):
    """
    View details about a specific character
    :param bookmark: Bookmark number
    :param character: Character to view
    """
    frozen_character: FrozenCharacter = session.query(FrozenCharacter).filter(FrozenCharacter.character == character, FrozenCharacter.identifier == bookmark).one()
    del character
    embed = Embed(
        title=frozen_character.full_name,
        fields_=[],
    )
    level = frozen_character.level
    primary_field = [f'Level: **{level}**']
    if frozen_character.divinity:
        primary_field.append(f' - Divinity: **{frozen_character.divinity}**')

    if frozen_character.dragonsouls:
        primary_field.append(f' - Dragon Souls: **{frozen_character.dragonsouls}**')

    embed.fields_.append(EmbedField(name='General', value='\n'.join(primary_field)))

    classes: List[FrozenClass] = sorted(frozen_character.classes, key=lambda item: item.level, reverse=True)
    lines = []
    if len(classes) > 10:
        lines.append(f'To see all {frozen_character.full_name}\'s classes, use `character classes {frozen_character.name}`')
        classes = classes[:9]
    lines.extend(f' - *{char_class.title}* : {char_class.level}' for char_class in classes)

    if lines:
        embed.fields_.append(EmbedField(
            name='Classes',
            value='\n'.join(lines)
        ))

    skills: List[FrozenSkill] = sorted(frozen_character.skills, key=lambda item: item.level, reverse=True)
    lines.clear()
    if len(skills) > 10:
        skills = skills[:9]
        lines.append(f'To see all {frozen_character.full_name}\'s skills, use `character skills {frozen_character.name}`')

    for frozen_skill in skills:
        if frozen_skill.skill.class_ in [frozen_class.class_ for frozen_class in frozen_character.classes]:
            char_class = [char_class for char_class in frozen_character.classes if char_class.class_ is frozen_skill.skill.class_][0]
            class_line = f'({char_class.title}) '
        else:
            class_line = ''
        lines.append(f' - {class_line}*{frozen_skill.skill.name}* : {frozen_skill.level}')

    if lines:
        embed.fields_.append(EmbedField(
            name='Skills',
            value='\n'.join(lines)
        ))

    perks = list(frozen_character.perks)
    lines.clear()
    if len(perks) > 10:
        lines.append(f'To see all {frozen_character.full_name}\'s perks, use `character perks "{frozen_character.name}"`')
        perks = perks[:9]

    lines.extend(f' - {frozen_perk.perk.name}' for frozen_perk in perks)

    if lines:
        embed.fields_.append(EmbedField(
            name='Perks',
            value='\n'.join(lines)
        ))

    attributes: List[FrozenAttribute] = sorted(frozen_character.attribute_objects, key=lambda item: item.max_value, reverse=True)  # Sort by value, descending
    lines.clear()
    if len(attributes) > 10:
        lines.append(f'To see all of {frozen_character.name}\'s attributes, use `character attributes "{frozen_character.name}"`')
        attributes = attributes[:9]

    for attribute in attributes:
        if attribute.value == 0:
            continue
        if attribute.value == attribute.max_value:
            lines.append(f'*{attribute.name}* : {attribute.value}')
        else:
            lines.append(f'*{attribute.name}* : {attribute.value}/{attribute.max_value}')

    if lines:
        embed.fields_.append(EmbedField(
            name='Attributes',
            value='\n'.join(lines)
        ))

    abilities = list(frozen_character.abilities)
    lines.clear()
    if len(abilities) > 5:
        lines.append(f'To see all {frozen_character.name}\'s abilities, use `character abilities "{frozen_character.name}"`')
        abilities = abilities[:4]

    lines.extend(f'*{ability.name}* : {ability.description_for_character(frozen_character.attributes)}' for ability in abilities)
    if lines:
        embed.fields_.append(EmbedField(
            name='Abilities',
            value='\n'.join(lines)
        ))

    shouts = list(frozen_character.shouts)
    lines.clear()
    if len(shouts) > 5:
        lines.append(f'To see all {frozen_character.name}\'s shouts, use `character shouts "{frozen_character.name}"`')
        shouts = shouts[:4]

    lines.extend(f'**{shout.shout.name}** '
                 f'*{" ".join(word.translation for word in sorted(shout.shout.words, key=lambda word: word.word_id)[:shout.max_words])}* : '
                 f'{shout.shout.description_for_character(frozen_character.attributes)}' for shout in shouts)
    if lines:
        embed.fields_.append(EmbedField(
            name='**Shouts**',
            value='\n'.join(lines)
        ))

    weapons: List[FrozenWeapon] = sorted(frozen_character.weapons, key=lambda weapon__: weapon__.equipped)
    lines.clear()
    if len(weapons) > 1:
        lines.append(f'To view all weapons, use `character weapons "{frozen_character.name}"`')
        weapons = [weapons[0]]

    for weapon in weapons:
        lines.append(f' - **{f"{weapon.name} (Equipped)" if weapon.equipped else weapon.name}:** {weapon.description}')
        for ability in weapon.enchanted_abilities():
            lines.append(f'*{ability.name}*: {ability.description_for_character(frozen_character.attributes)}')

    if lines:
        embed.fields_.append(EmbedField(
            name='Weapons',
            value='\n'.join(lines)
        ))

    armor: List[Armor] = sorted(frozen_character.armor, key=lambda armor__: armor__.equipped)
    lines.clear()
    lines.append('Note that attributes and perks from the armor are shown in their respective categories, not under armor.')
    if len(armor) > 5:
        lines.append(f'To view all armor, use `character armor {frozen_character.name}`')
        armor = [armor__ for armor__ in armor if armor__.equipped]

    for armor_item in armor:
        lines.append(f'- {armor_item.name} {"(Equipped) " if armor_item.equipped else ""}{f"Divinity: {armor_item.divinity}" if armor_item.divinity else ""}')

    if lines:
        embed.fields_.append(EmbedField(name='Armor', value='\n'.join(lines)))

    items: List[Item] = sorted(frozen_character.items, key=lambda item: item.amount)
    lines.clear()
    if len(items) > 10:
        lines.append(f'To view a complete inventory (Potentially through pagination), use `character inventory {frozen_character.name}`')
        items = items[:9]

    lines.extend(f'{item.name}: {item.amount}' for item in items)
    if lines:
        embed.fields_.append(EmbedField(name='Inventory', value='\n'.join(lines)))

    titles = [title.title for title in frozen_character.titles]
    if titles:
        embed.fields_.append(EmbedField(name='Titles', value=' '.join(titles)))

    return {'embed': embed}


@verify_open_channel
async def armor_(bookmark: str, character: Character, *args, equipped: Optional[int], filter_: Optional[str]):
    """
    Adds Armor to a character, or changes existent armor.
    :param character: Character whose armor it's about
    :param equipped: Whether it is equipped or not.
    :param bookmark: Bookmark number
    :param filter_: Filter by armor name.
    """
    frozen_character: FrozenCharacter = session.query(FrozenCharacter).filter(FrozenCharacter.character == character, FrozenCharacter.identifier == bookmark).one()
    del character
    page = int(args[0]) - 1 if args else 0
    items_per_page = 5
    armor = list(frozen_character.armor)
    num_armor_pieces = len(armor)
    total_pages = max((num_armor_pieces // items_per_page) + ((num_armor_pieces % items_per_page) != 0), 1)  # Don't need to add 1 if it divides exactly.
    if 0 > page or page >= total_pages:
        return {'content': f'Invalid page ID. Please use 1 - {total_pages}'}

    if filter_:
        armor = [armor__ for armor__ in armor if filter_ in armor__.name]

    if equipped is not None:
        armor = [armor__ for armor__ in armor if armor__.equipped == bool(equipped)]

    armor = armor[page * items_per_page:(1 + page) * items_per_page]
    lines = ['Please note that unlike with "live" characters, bookmarks don\'t show perks and attributes on the armor.']
    embed = Embed(
        title=f'Armor of {frozen_character.full_name}',
        fields_=[],
        footer=EmbedFooter(text=f'Page: {page + 1}/{total_pages}')
    )

    for armor_item in armor:
        embed.fields_.append(EmbedField(
            name=f'{armor_item.name} {"(Equipped) " if armor_item.equipped else ""}{f"Divinity: {armor_item.divinity}" if armor_item.divinity else ""}',
            value='\n'.join(lines) or 'No bonus.'
        ))

    return {'embed': embed}

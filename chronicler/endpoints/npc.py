"""Manage NPCs"""

from logging import getLogger
from typing import Optional, List

from chronicler.discord.embed import Embed, EmbedField, EmbedFooter
from chronicler.models.attribute import Attribute
from chronicler.models.author import Author
from chronicler.models.character import NPC, NPCAttr, NPCPerk
from chronicler.models.perk import Perk
from chronicler.parser import SubParser, FromMessage
from chronicler.models.base import session
from chronicler.parser import verify_author_and_channel, verify_open_channel

logger = getLogger(__name__)


def add_npc_parsers(subparsers: SubParser):
    parser = subparsers.add_parser('npc', description='Functions for managing NPC\'s.',
                                   aliases=['NPC', 'npcs', 'NPCs', 'NPCS'])
    sub: SubParser = parser.add_subparsers()
    for func in [create, edit, attribute_, perk_, view, list_, perks_, attributes_, abilities_]:
        sub.add_parser_from_function(func)


@verify_open_channel
async def abilities_(npc: NPC, *args, filter_: Optional[str], level: Optional[int]):
    """
    Shows all abilities that belong to an NPC.
    :param filter_: Exclude results not like %this%.
    :param npc: NPC whose abilities to show.
    :param level: Pretend NPC has this level.
    """
    page = int(args[0])-1 if args else 0
    if level is not None:
        npc.level = level
    items_per_page = 20
    abilities = list(npc.abilities)
    num_abil = len(abilities)
    total_pages = max((num_abil // items_per_page) + ((num_abil % items_per_page) != 0), 1)  # Don't need to add 1 if it divides exactly.
    if 0 > page or page >= total_pages:
        return {'content': f'Invalid page ID. Please use 1 - {total_pages}'}

    if filter_:
        abilities = [ability for ability in abilities if filter_ in ability.name]

    abilities = abilities[page * items_per_page:(1 + page) * items_per_page]
    embed = Embed(
        title=f'Abilities of {npc.name}' if level is None else f'Abilities of {npc.name} (Level {level})',
        description='',
        fields_=[],
        footer=EmbedFooter(text=f'Page: {page + 1}/{total_pages}')
    )
    for ability in abilities:
        lines = [ability.description_for_character(npc)]
        if ability.costs:
            lines.append('Cost:')
            lines.extend(f'- {attribute.name}: **{value}**' for attribute, value in ability.cost(npc).items())
        embed.fields_.append(EmbedField(
            name=ability.name,
            value='\n'.join(lines)
        ))
    session.rollback()
    return {'embed': embed}


@verify_open_channel
async def attributes_(npc: NPC, *args, filter_: Optional[str], level: Optional[int]):
    """
    Shows the attributes a given NPC has.
    :param filter_: Exclude results not like %this%.
    :param npc: NPC whose attributes to show.
    :param level: Pretend NPC has this level.
    """
    page = int(args[0])-1 if args else 0
    if level is not None:
        npc.level = level
    items_per_page = 20
    num_attr = len(npc.attributes)
    total_pages = max((num_attr // items_per_page) + ((num_attr % items_per_page) != 0), 1)  # Don't need to add 1 if it divides exactly.
    if 0 > page or page >= total_pages:
        return {'content': f'Invalid page ID. Please use 1 - {total_pages}'}

    attributes = list(npc.attributes.items())

    if filter_:
        attributes = [(attribute, value) for attribute, value in attributes if filter_ in attribute.name]

    attributes = attributes[page * items_per_page:(1 + page) * items_per_page]
    embed = Embed(
        title=f'Attributes of {npc.name}' if level is None else f'Attributes of {npc.name} (Level {level})',
        description='\n'.join(f'- **{attribute.name}**: {value}' for attribute, value in attributes),
        footer=EmbedFooter(text=f'Page: {page + 1}/{total_pages}')
    )
    session.rollback()
    return {'embed': embed}


@verify_open_channel
async def perks_(npc: NPC, *args, filter_: Optional[str], level: Optional[int]):
    """
    Shows the perks of a given NPC.
    :param filter_: Exclude results not like %this%.
    :param npc: NPC whose perks to show.
    :param level: Pretend NPC has this level.
    """
    if level is not None:
        npc.level = level
    page = int(args[0])-1 if args else 0
    items_per_page = 20
    num_npc = len(npc.perks)
    total_pages = max((num_npc // items_per_page) + ((num_npc % items_per_page) != 0), 1)  # Don't need to add 1 if it divides exactly.
    if 0 > page or page >= total_pages:
        return {'content': f'Invalid page ID. Please use 1 - {total_pages}'}

    perks = [npc_perk.perk for npc_perk in npc.perks]

    if filter_:
        perks = [perk for perk in perks if filter_ in perk.name or filter_ in perk.description]

    perks = perks[page * items_per_page:(1 + page) * items_per_page]
    embed = Embed(
        title=f'Perks of {npc.name}',
        description='\n'.join(f'- **{perk.name}**: {perk.description or ""}' for perk in perks),
        footer=EmbedFooter(text=f'Page: {page + 1}/{total_pages}')
    )
    session.rollback()
    return {'embed': embed}


@verify_open_channel
async def list_(*args, filter_: Optional[str]):
    """
    Shows a list of all npc's.
    :param filter_: Exclude results not like %this%.
    """
    page = int(args[0])-1 if args else 0
    items_per_page = 20
    num_npc = session.query(NPC).filter(NPC.name.like(f'%{filter_}%')).count() if filter_ else session.query(NPC).count()
    total_pages = max((num_npc // items_per_page) + ((num_npc % items_per_page) != 0), 1)  # Don't need to add 1 if it divides exactly.
    if 0 > page or page >= total_pages:
        return {'content': f'Invalid page ID. Please use 1 - {total_pages}'}

    query = session.query(NPC)
    if filter_:
        query = query.filter(NPC.name.like(f'%{filter_}%'))

    npcs = query.offset(page * items_per_page).limit(items_per_page).all()
    embed = Embed(
        title='NPC List',
        description='\n'.join(f'- **{npc.name}**: {npc.description or ""}' for npc in npcs),
        footer=EmbedFooter(text=f'Page: {page + 1}/{total_pages}')
    )
    return {'embed': embed}


@verify_open_channel
async def view(
        npc: NPC,
        level: Optional[int],
):
    """
    View details about a specific NPC
    :param npc: NPC to view
    :param level: View as if the npc had this level.
    """
    if level is not None:  # We rollback at the end to make sure this isn't preserved.
        npc.level = level

    embed = Embed(
        title=npc.name if npc.title is None else f'{npc.name} {npc.title}',
        description=npc.description,
        fields_=[],
    )
    primary_field = [f'Level: {npc.level}']
    if npc.divinity:
        primary_field.append(f'Divinity: {npc.divinity}')

    if npc.dragonsouls:
        primary_field.append(f'Dragon Souls: {npc.dragonsouls}')

    embed.fields_.append(EmbedField(name='General', value='\n'.join(primary_field)))

    if len(npc.perks) > 10:
        # Render partial list only, and tell user where to find the full list.
        perks: List[NPCPerk] = npc.perks[:9]
        embed.fields_.append(EmbedField(
            name='Perks',
            value=f'To see all {npc.name}\'s perks, use `npc perks "{npc.name}"`\n' +
                  '\n'.join(f'- {npc_perk.perk.name}' for npc_perk in perks if npc.level >= npc_perk.level)
        ))
    elif npc.perks:
        # Render full list
        embed.fields_.append(EmbedField(
            name='Perks',
            value='\n'.join(f'- {npc_perk.perk.name}' for npc_perk in npc.perks if npc.level >= npc_perk.level)
        ))

    if len(npc.attributes) > 10:
        attributes = sorted(npc.attributes.items(), key=lambda pair: pair[1], reverse=True)[:9]
        lines = [f'To see all of {npc.name}\'s attributes, use `npc attributes "{npc.name}"`']
        for attribute, value in attributes:
            lines.append(f'- {attribute.name}: {value}')
        embed.fields_.append(EmbedField(
            name='Attributes',
            value='\n'.join(lines)
        ))
    elif npc.attributes:
        embed.fields_.append(EmbedField(
            name='Attributes',
            value='\n'.join(f'- {attribute.name}: {value}' for attribute, value in sorted(npc.attributes.items(), key=lambda pair: pair[1], reverse=True))
        ))

    abilities = list(npc.abilities)
    if len(abilities) > 10:
        abilities = abilities[:9]
        lines = [f'To see all {npc.name}\'s abilities, use `npc abilities "{npc.name}"`']
        for ability in abilities:
            lines.append(f'- {ability.name}: {ability.description_for_character(npc)}')
        embed.fields_.append(EmbedField(
            name='Abilities',
            value='\n'.join(lines)
        ))
    elif abilities:
        embed.fields_.append(EmbedField(
            name='Abilities',
            value='\n'.join(f'- {ability.name}: {ability.description_for_character(npc)}' for ability in abilities)
        ))

    return {'embed': embed}


@verify_author_and_channel
async def perk_(
        npc: NPC,
        perk: Perk,
        remove: bool,
        level: Optional[int],
):
    """
    Adds a perk to a NPC.
    :param npc: Which NPC is to have a perk or not.
    :param perk: Which perk it is about.
    :param level: What level the perk is unlocked.
    :param remove: Remove instead of add the perk.
    """
    npc_perk = session.query(NPCPerk).filter(NPCPerk.npc == npc, NPCPerk.perk == perk).one_or_none()
    if remove:
        if npc_perk is None:
            return {'content': f'Error, NPC {npc.name} does not have perk {perk.name}'}

        session.delete(npc_perk)
        session.commit()
        return {'content': f'Perk {perk.name} removed from {npc.name}'}

    if npc_perk is None:
        session.add(NPCPerk(npc=npc, perk=perk, level=level or 0))
        session.commit()
        return {'content': f'Successfully added {perk.name} to {npc.name}'}

    npc_perk.level = level
    return {'content': f'{npc.name}\'s {perk.name} perk is now unlocked at level {level}'}


@verify_author_and_channel
async def attribute_(
        npc: NPC,
        attribute: Attribute,
        value: float,
        per_level: bool
):
    """
    Adds an attribute to an NPC, optionally for every level it has.
    :param npc: NPC name, eg "Flame Atronach"
    :param attribute: Attribute name, eg "Fire Resistance"
    :param value: How much of the Attribute
    :param per_level: Whether the bonus should be applied for each level, or only once.
    """
    npc_attr = session.query(NPCAttr).filter(NPCAttr.npc == npc, NPCAttr.attribute == attribute, NPCAttr.per_level == per_level).one_or_none()
    new = npc_attr is None
    if new:
        npc_attr = NPCAttr(npc=npc, attribute=attribute, value=value, per_level=per_level)
        session.add(npc_attr)
        session.commit()
        return {'content': f'Successfully added {attribute.name} to {npc.name} with strength {value} {"for every level" if per_level else "once"}.'}

    npc_attr.value = value
    npc_attr.per_level = per_level
    session.commit()
    return {'content': f'Successfully edited {attribute.name} of {npc.name} to have strength {value} {"for every level" if per_level else "once"}.'}


@verify_author_and_channel
async def edit(
        npc: NPC,
        title: Optional[str],
        description: Optional[str],
        divinity: Optional[int],
        dragonsouls: Optional[int],
        level: Optional[int],
        name: Optional[str],
        author: int = FromMessage.Author,
):
    """
    Edits an NPC.
    :param npc: Name of the NPC
    :param title: Title of the NPC
    :param description: Short description.
    :param divinity: Divinity rating.
    :param author: RL person managing this NPC.
    :param dragonsouls: Number of dragon souls the NPC possesses.
    :param level: NPC's level. Should probably be somewhere around ((sum(STR, END, DEX, CHA, INT, WIS) - 60) / 10)
    :param name: New name for the NPC.
    """
    if name is not None:
        npc.name = name

    return await create(name=npc.name, title=title, description=description, divinity=divinity, author=author, dragonsouls=dragonsouls, level=level)


@verify_author_and_channel
async def create(
        name: str,
        description: str,
        title: Optional[str],
        divinity: int = 0,
        author_id: int = FromMessage.Author,
        guild_id: int = FromMessage.Guild,
        dragonsouls: int = 0,
        level: int = 0,
):
    """
    Creates a new NPC, creature, or other actor.
    :param name: Name of the NPC
    :param title: Title of the NPC
    :param description: Short description.
    :param divinity: Divinity rating.
    :param author_id: RL person managing this NPC.
    :param guild_id: Discord Guild we're working in.
    :param dragonsouls: Number of dragon souls the NPC possesses.
    :param level: NPC's level. Should probably be somewhere around ((sum(STR, END, DEX, CHA, INT, WIS) - 60) / 10)
    """
    npc: NPC = session.query(NPC).filter(NPC.name == name).one_or_none()
    author = session.query(Author).filter(Author.user_id == author_id, Author.guild_id == guild_id).one()
    new = npc is None
    if new:
        npc = NPC(name=name, description=description, author_id=author.entity_id)
        session.add(npc)

    if title is not None:
        npc.title = title

    if divinity is not None:
        npc.divinity = divinity

    if dragonsouls is not None:
        npc.dragonsouls = dragonsouls

    if level is not None:
        npc.level = level

    session.commit()
    if new:
        return {'content': f'Successfully created {npc.name} as new NPC.'}

    return {'content': f'Updated {npc.name}'}

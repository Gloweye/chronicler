"""Manage Attributes"""

from logging import getLogger
from typing import Optional

from chronicler.discord.embed import Embed, EmbedField, EmbedFooter
from chronicler.discord.emoji import Emoji
from chronicler.parser import SubParser
from chronicler.models.base import session
from chronicler.models.attribute import Attribute, SubAttribute
from chronicler.parser import verify_author_and_channel, verify_open_channel

logger = getLogger(__name__)


def add_attribute_parsers(subparsers: SubParser):
    parser = subparsers.add_parser('attribute', description='Functions for managing attributes.', aliases=['attributes', 'Attributes', 'Attribute'])
    sub: SubParser = parser.add_subparsers()
    for func in [create, boost_, list_, view, edit]:
        sub.add_parser_from_function(func)


@verify_author_and_channel
async def create(name: str, description: str = '', max_value: Optional[int] = None, dragon_soul_mod: float = 0.0, emoji: Optional[Emoji] = None):
    """
    Creates an Attribute, which governs all that actually happens to the character.
    :param name: Name, for example: Strength, Health, or Fire Resistance.
    :param description: Short description of what the attribute does.
    :short description: d
    :param max_value: Maximum value, if applicable.
    :param dragon_soul_mod: How much this attribute is affected by dragon souls. 0 = no effect. 1 = doubled with even a single dragon soul.
    :param emoji: Optionally a custom Emoji that belongs to the attribute and may sometimes be used in conjunction with/instead of the attribute name.
    """
    session.add(Attribute(
        name=name,
        max_value=max_value,
        description=description,
        dragon_soul_mod=dragon_soul_mod,
        emoji=emoji,
    ))
    session.commit()
    return {'content': f'Successfully created attribute "{name}"'}


@verify_author_and_channel
async def edit(attribute: Attribute, description: Optional[str], max_value: Optional[int], dragon_soul_mod: Optional[float], clear_boost: bool,
               emoji: Optional[Emoji], clear_emoji: bool, name: Optional[str]):
    """
    Edits a part of the attribute.
    :param attribute: Attribute to edit
    :param name: New name.
    :param description: New description. Omit to keep the old one.
    :param max_value: New Max Value. Omit to keep the old one.
    :param dragon_soul_mod: New Dragon Soul Modifier. Omit to keep the old one.
    :param clear_boost: Removes all boosts to other attributes if given.
    :param emoji: Assign another emoji to this attribute. Note that only custom Emoji's are supported.
    :param clear_emoji: Ignore the emoji, and instead
    """
    resp = ['Changes saved.']
    if name is not None:
        resp.append(f'Renaming {attribute.name} to {name}')
        attribute.name = name

    if description is not None:
        attribute.description = description
        resp.append(f'Attribute description set to "{description}"')

    if max_value is not None:
        attribute.max_value = max_value
        resp.append(f'Attribute maximum set to {max_value}')

    if dragon_soul_mod is not None:
        attribute.dragon_soul_mod = dragon_soul_mod
        resp.append(f'Attribute Dragon Soul modifier set to {dragon_soul_mod}')

    if emoji is not None and not clear_emoji:
        attribute.emoji = emoji
        resp.append(f'Attribute emoji modifier set to {emoji}')

    if clear_emoji:
        attribute.emoji = None
        resp.append(f'Emoji cleared, if there was any.')

    if clear_boost:
        for sub_attr in attribute.attributes:
            session.delete(sub_attr)
        resp.append(f'All Boosts to other attributes cleared.')

    session.commit()
    return {'content': '\n'.join(resp)}


@verify_author_and_channel
async def boost_(parent: Attribute, child: Attribute, mult: float):
    """
    Having a certain value of attribute `parent` will increase attribute `child` by a factor of mult.
    If this combination already exists, overwrite it.
    :param parent: Attribute to boost another.
    :param child: Attribute being boosted.
    :param mult: How much the attribute is being boosted.
    """

    boost = session.query(SubAttribute).filter(SubAttribute.parent_id == parent.entity_id, SubAttribute.child_id == child.entity_id).one_or_none()
    if boost is None:
        boost = SubAttribute(parent_id=parent.entity_id, child_id=child.entity_id, multiplier=mult)
        session.add(boost)
    else:  # If we already have this combination, overwrite it.
        boost.multiplier = mult

    session.commit()
    resp = f'Attribute {parent.name} now boosts {child.name} by a factor {mult}'
    if child.attributes:
        resp += f'\nChild Attribute {child} itself boosts attributes. Note that chaining does not propagate.'

    return {'content': resp}


@verify_open_channel
async def view(attribute: Attribute):
    """
    Shows details about the given attribute. Note that multi-word names need quotes, like "Fire Resistance"
    :param attribute: Attribute to inspect
    """
    embed = Embed(
        title=f'{attribute.emoji} {attribute.name}' if attribute.emoji is not None else attribute.name,
        description=attribute.description,
        fields_=[]
    )
    if attribute.max_value is not None:
        embed.fields_.append(EmbedField(name='Maximum value', value=str(attribute.max_value)))

    if attribute.dragon_soul_mod:
        embed.fields_.append(EmbedField(
            name='Dragon Soul Boost',
            value=f'Dragon Souls make this attribute stronger by {attribute.dragon_soul_mod * 100}% each.'
        ))

    if attribute.attributes:
        embed.fields_.append(EmbedField(
            name='Attribute Boosts',
            value='This attribute strengthens the following other attributes:\n\n' +
            '\n'.join(f'- {sub_attr.child.name}: {sub_attr.multiplier:.2f}' for sub_attr in attribute.attributes)
        ))

    return {'embed': embed}


@verify_open_channel
async def list_(*args, filter_: str = ''):
    """
    Shows a list of all attributes.
    :param filter_: Exclude results not like %this%.
    """
    page = int(args[0])-1 if args else 0
    items_per_page = 20
    num_attributes = session.query(Attribute).filter(Attribute.name.like(f'%{filter_}%')).count() if filter_ else session.query(Attribute).count()
    total_pages = max((num_attributes // items_per_page) + ((num_attributes % items_per_page) != 0), 1)  # Don't need to add 1 if it divides exactly.
    if 0 > page or page >= total_pages:
        return {'content': f'Invalid page ID. Please use 1 - {total_pages}'}

    query = session.query(Attribute)
    if filter_:
        query = query.filter(Attribute.name.like(f'%{filter_}%'))

    attributes = query.offset(page * items_per_page).limit(items_per_page).all()
    embed = Embed(
        title='Attributes List',
        description='\n'.join(f'**{attr.name}**: {attr.description}' for attr in attributes),
        footer=EmbedFooter(text=f'Page: {page + 1}/{total_pages}')
    )
    return {'embed': embed}

"""Manage Abilities"""

from logging import getLogger
from typing import Optional, List

from chronicler.discord.embed import Embed, EmbedField, EmbedFooter
from chronicler.models.attribute import Attribute
from chronicler.parser import SubParser
from chronicler.models.base import session
from chronicler.models.ability import Ability, AbilityCost, Classification
from chronicler.parser import verify_author_and_channel, verify_open_channel

logger = getLogger(__name__)


def add_ability_parsers(subparsers: SubParser):
    parser = subparsers.add_parser('ability', description='Functions for managing abilities.', aliases=['abilities', 'Ability', 'Abilities'])
    sub: SubParser = parser.add_subparsers()
    for func in [create, view, cost, list_, edit]:
        sub.add_parser_from_function(func)


@verify_author_and_channel
async def cost(ability: Ability, attribute: Attribute, magnitude: int):
    """
    Adds a cost to an ability.
    :param ability: What ability should get a cost
    :param attribute: What attribute the cost should be paid in.
    :param magnitude: How much of that attribute does the ability cost
    """
    cost_: AbilityCost = session.query(AbilityCost).filter(AbilityCost.ability == ability, AbilityCost.attribute == attribute).one_or_none()
    if cost_ is None:
        session.add(AbilityCost(ability=ability, attribute=attribute, magnitude=magnitude))
    else:
        cost_.magnitude = magnitude

    session.commit()
    return {'content': f'Successfully set the {attribute.name} cost of {ability.name} to {magnitude}'}


@verify_open_channel
async def list_(*args, filter_: Optional[str] = None):
    """
    Lists all abilities
    :param filter_: Exclude results not like %this%.
    """
    page = int(args[0]) - 1 if args else 0
    items_per_page = 20
    num_abilities = session.query(Ability).filter(Ability.name.like(f'%{filter_}%')).count() if filter_ else session.query(Ability).count()
    total_pages = max((num_abilities // items_per_page) + ((num_abilities % items_per_page) != 0), 1)  # Don't need to add 1 if it divides exactly.
    if 0 > page or page >= total_pages:
        return {'content': f'Invalid page ID. Please use 1 - {total_pages}'}

    query = session.query(Ability)
    if filter_:
        query = query.filter(Ability.name.like(f'%{filter_}%'))

    abilities: List[Ability] = query.offset(page * items_per_page).limit(items_per_page).all()

    embed = Embed(
        title='Abilities',
        footer=EmbedFooter(text=f'Page: {page + 1}/{total_pages + 1}'),
        fields_=[]
    )
    for ability in abilities:
        embed.fields_.append(EmbedField(
            name=ability.render_name,
            value=ability.render_description,
        ))

    return {'embed': embed}


@verify_open_channel
async def view(ability: Ability):
    """
    Views details of an ability
    :param ability: Ability to view
    """
    embed = Embed(
        title=ability.render_name,
        description=ability.render_description,
        fields_=[EmbedField(
            name='Cost',
            value='\n'.join(f'{c_attr.magnitude}  {c_attr.attribute.name}' for c_attr in ability.costs) if ability.costs else 'Free'
        )],
    )

    if ability.divinity:
        embed.fields_.append(EmbedField(name='Divinity', value=ability.divinity))

    embed.footer = EmbedFooter(  # @TODO: Give the exact command to use when calculating how a character uses an ability.
        text='To view how abilities look when used by a specific character, use `character abilities [name]`'
    )

    return {'embed': embed}


@verify_author_and_channel
async def edit(
        ability: Ability,
        new_name: Optional[str],
        description: Optional[str],
        radius: Optional[float],
        magnitude: Optional[int],
        magnitude_scale: Optional[float],
        scale_attribute: Optional[Attribute],
        remove_scale_attribute: bool,
        duration: Optional[int],
        classification: Optional[Classification],
        remove_classification: bool,
        swing_duration: Optional[float],
        divinity: Optional[int]
):
    """
    Edits an ability.
    :param ability: Ability to edit.
    :param new_name: New name.
    :param description: New Description.
    :param radius: How wide the radius of the ability is, if it's AoE.
    :param magnitude: New base magnitude.
    :param magnitude_scale: New Magnitude Scale factor
    :param scale_attribute: New Attribute to Scale with
    :param remove_scale_attribute: Remove magnitude scaling, ignoring their parameters if given.
    :param duration: Duration of the effect
    :param classification: Classification to attach.
    :param remove_classification: Remove Classification, ignoring its argument if given.
    :param swing_duration: How long it takes to execute the ability.
    :param divinity: Divinity Rating.
    """
    lines = []
    if new_name is not None:
        lines.append(f'{ability.name} renamed to {new_name}.')
        ability.name = new_name

    if description is not None:
        lines.append(f'Description changed from {ability.description} to {description}.')
        ability.description = description

    if magnitude is not None:
        lines.append(f'Magnitude changed from {ability.magnitude} to {magnitude}.')
        ability.magnitude = magnitude

    if magnitude_scale is not None and not remove_scale_attribute:
        lines.append(f'Magnitude scaling changed from {ability.magnitude_scale} to {magnitude_scale}.')
        ability.magnitude_scale = magnitude_scale

    if scale_attribute is not None and not remove_scale_attribute:
        lines.append(f'Magnitude scaling attribute changed to {scale_attribute.name}')
        ability.scale_attribute = scale_attribute

    if duration is not None:
        lines.append(f'Duration changed from {ability.duration} to {duration}')
        ability.duration = duration

    if radius is not None:
        lines.append(f'Radius changed from {ability.radius} to {radius}')
        ability.radius = radius

    if classification is not None and not remove_classification:
        lines.append(f'Classification changed to {classification.name}')
        ability.classification = classification

    if remove_classification:
        lines.append(f'Classification removed.')
        ability.classification = None

    if remove_scale_attribute:
        lines.append(f'Attribute scaling removed.')
        ability.scale_attribute = None
        ability.magnitude_scale = 0.0

    if swing_duration is not None:
        lines.append(f'Swing duration changed from {ability.swing_duration} to {swing_duration}')
        ability.swing_duration = swing_duration

    if divinity is not None:
        lines.append(f'Divinity changed from {ability.divinity} to {divinity}')
        ability.divinity = divinity

    session.commit()
    return {'content': '\n'.join(lines)}


@verify_author_and_channel
async def create(
        name: str,
        description: str,
        duration: Optional[int] = None,
        radius: Optional[float] = None,
        magnitude: int = 0,
        magnitude_scale: float = 0.0,
        scale_attribute: Optional[Attribute] = None,
        classification: Optional[Classification] = None,
        swing_duration: float = 1.0,
        divinity: int = 0
):
    """
    Creates a new ability.
    :param name: Name of the Ability
    :param description: Description. Supports {}-enclosed attributes magnitude, attack_speed, duration.
    :param duration: How long the spell effect lasts, or None if there is no duration.
    :param radius: For AoE, how wide the AoE's radius is.
    :param magnitude: Base Magnitude of the effect
    :param magnitude_scale: How much scale_attribute affects magnitude. Processed *before* classification effects.
    :param scale_attribute: Which attribute affects the base power. Tends to be INT for magical and STR for physical offensive abilities.
    :param classification: What classification the ability has, which affects which attributes affect damage, resistance, cost, duration and casting time.
    :param swing_duration: AKA Casting Time. How long it takes to execute the ability. This is the inverse of attack speed.
    :param divinity: Divinity ranking added to the character for the sake of executing the ability. Determines which "absolute" effect wins. Between an unstoppable force and an immovable object, this one decides which wins.
    """
    if session.query(Ability).filter(Ability.name == name).one_or_none() is not None:
        return {'content': f'Ability {name} already exists.'}
    session.add(Ability(
        name=name,
        description=description,
        duration=duration,
        radius=radius,
        magnitude=magnitude,
        magnitude_scale=magnitude_scale,
        scale_attribute=scale_attribute,
        classification=classification,
        swing_duration=swing_duration,
        divinity=divinity
    ))
    session.commit()
    return {'content': f'Created ability {name} successfully.'}

"""
CLIParser is a class that can take a function and create an ArgumentParser tailored to it.
"""
import shlex
from contextlib import contextmanager
from argparse import ArgumentParser, Namespace, _SubParsersAction
from enum import Enum, auto
from inspect import signature, Parameter, Signature
import logging
from typing import Callable, TypeVar, Any, Union, List, Tuple, Iterable, _GenericAlias, Optional
import sys

from sqlalchemy.orm.exc import MultipleResultsFound, NoResultFound

from chronicler.discord.cache import cache
from chronicler.discord.channel import Channel
from chronicler.discord.embed import EmbedField, Embed
from chronicler.discord.emoji import Emoji
from chronicler.discord.message import Message
from chronicler.discord.role import Role
from chronicler.discord.snowflake import Snowflake
from chronicler.discord.guild import Guild
from chronicler.discord.user import User
from chronicler.models.ability import Ability, Classification, Shout, Word
from chronicler.models.attribute import Attribute
from chronicler.models.author import Author, OpenChannel
from chronicler.models.base import session
from chronicler.models.character import NPC, Character
from chronicler.models.class_ import Class
from chronicler.models.info import Infobox
from chronicler.models.perk import Perk
from chronicler.models.skill import Skill
from chronicler.tools import get_server_prefix, attr_by_name_or_id, perk_by_name_or_id, class_by_name_or_id, skill_by_name_or_id, ability_by_name_or_id, \
    classification_by_name_or_id, shout_by_name_or_id, npc_by_name_or_id, infobox_by_name_or_id

ReturnValue = TypeVar('ReturnValue')
logger = logging.getLogger(__name__)  # pylint: disable=invalid-name
RESERVED_SHORT = {
    # 'gamer': 'g',
    # 'class': 'c',
    # 'skill': 's',
}
TYPES = {
    'int': int,
    'str': str,
    'bool': bool,
    'list': list,
    'tuple': tuple,
    'None': type(None),
}
TYPE_FACTORY_MAP = {
    # Models
    Attribute: attr_by_name_or_id,
    Perk: perk_by_name_or_id,
    Class: class_by_name_or_id,
    Skill: skill_by_name_or_id,
    Ability: ability_by_name_or_id,
    Classification: classification_by_name_or_id,
    Shout: shout_by_name_or_id,
    NPC: npc_by_name_or_id,
    Character: Character.get_by_name,
    # Parsed from strings/mentions
    Emoji: Emoji.from_string,
    User: User.from_string,
    Channel: Channel.from_string,
    Role: Role.from_string,
    Infobox: infobox_by_name_or_id,
}


class MessageVerificationFailed(Exception):
    pass


class IgnoreEnum(Enum):
    """Subclasses of this Enum are not added as arguments, instead calculated after parsing."""


class FromMessage(IgnoreEnum):
    Guild = auto()
    Author = auto()
    Channel = auto()
    User_Mentions = auto()
    Channel_Mentions = auto()
    Role_Mentions = auto()

    def get(self, message, default=None):
        return {
            FromMessage.Guild: message.guild_id,
            FromMessage.Author: getattr(message.author, 'id', None),  # message.author may be None, and None has no id.
            FromMessage.Channel: message.channel_id,
            FromMessage.User_Mentions: message.mentions,
            FromMessage.Role_Mentions: message.mention_roles,
            FromMessage.Channel_Mentions: message.mention_channels
        }.get(self, default)


class Choices(str, Enum):
    """Superclass for all Enumerations of which members are requested as arguments."""

    def __str__(self):
        return self.value


class TupleType:
    """
    Creates a type converter for Tuples.
    For any inst = TupleType(Tuple[str, int, float]),
    the first inst(var) -> str(var), the second inst(var) -> int(var), and the third inst(var) -> float(var).
    Subsequent calls will raise an IndexError.
    """

    def __init__(self, types: Tuple):
        self.types = types
        self.index = 0

    def __call__(self, var: str):
        ret_val = self.types[self.index](var)
        self.index += 1
        return ret_val

    def __repr__(self):
        return f'{self.__class__.__name__}{tuple(type_.__name__ for type_ in self.types)}'


def check_author_and_channel(message: 'Message', args: Namespace):
    """ Also returns True on every open channel. """
    channel_id = FromMessage.Channel.get(message)
    author_id = FromMessage.Author.get(message)
    if author_id is None:  # Not all messages have an author
        return False

    try:
        author = session.query(Author).filter(Author.user_id == author_id).one()
    except (MultipleResultsFound, NoResultFound):
        return False

    return bool(session.query(OpenChannel).filter(OpenChannel.channel_id == channel_id).one()) \
        or any(channel_id == auth_chan.channel_id for auth_chan in author.channels)


def check_author_owns_character(message: 'Message', args: Namespace):
    author_id = FromMessage.Author.get(message)
    '''
    # Note: The original idea was that people could build their own characters. But that means that literally *everything* needs an author_id, and I just 
    # can't be fucked to implement all that shit. 
    # If you wanna write a character or even just mess around, go ahead, clone the repo, and run your own version of the bot. 
    # Note that the availability of support may be dependent on my mood, the time of day, the weather, whether I'm at work, 
    # or the position of jupiter in the sky, and whether or not I finally found a GF, and the birthsign of the last Oblivion char you played.
    if author_id is None:  # Not all messages have an author
        return False

    for param in signature(func).parameters.values():
        if param.annotation == Character.get_by_name:
            character = getattr(args, param.name)
            break
    else:
        return False

    return character.author_id == author_id
    '''
    return author_id == 410886004677804042


def check_channel_open(message: 'Message', args: Namespace):
    return True
    channel_id = FromMessage.Channel.get(message)
    return bool(session.query(OpenChannel).filter(OpenChannel.channel_id == channel_id).one_or_none())


def check_server_owner(message: 'Message', args: Namespace):
    sender = FromMessage.Author.get(message)
    guild_id = FromMessage.Guild.get(message)
    guild = cache[Guild, guild_id]
    return guild.owner_id == sender or sender == 410886004677804042


def verify(*funcs):
    """
    Adds a number of checks to a function. Before the function is executed, the Parser below will execute these checks. Example:
    >>> def check(message: 'Message', args: 'Namespace'):
    >>>     # Implementation, message being the Message object from discord, and args being the namespace containing the parsed arguments.
    >>>
    >>> @verify(check)
    >>> def function(...):
    >>>     ...
    """
    def decorator(func):
        if hasattr(func, 'checks'):
            func.checks.extend(funcs)
        else:
            func.checks = list(funcs)
        return func
    return decorator


verify_author_and_channel = verify(check_author_and_channel)
verify_author_owns_character = verify(check_author_owns_character)
verify_open_channel = verify(check_channel_open)
verify_server_owner = verify(check_server_owner)


class SubParser(_SubParsersAction):
    parent: 'Parser'

    @contextmanager
    def parser_constructor(self, parser):
        """Use a different parser constructor, like, say, a classmethod."""
        old = self._parser_class
        self._parser_class = parser
        yield
        self._parser_class = old

    def add_parser(self, *args, **kwargs):
        parser = super().add_parser(*args, **kwargs)
        self.parent.subparsers.append(parser)
        parser.parent = self.parent
        return parser

    def add_parser_from_function(self, func, name='', **kwargs) -> 'Parser':
        with self.parser_constructor(Parser.from_function):
            if func.__doc__ is not None:  # We need a help description, so use the first non-empty line from the docstring, unless it already has a help.
                for line in func.__doc__.split('\n'):
                    line = line.strip()
                    if line:
                        kwargs.setdefault('help', line)
                        break

            name = name or func.__name__.strip('_').replace('_', '-')
            parser = super().add_parser(name=name, func=func, **kwargs)
            self.parent.subparsers.append(parser)
            parser.parent = self.parent
            return parser


class Parser(ArgumentParser):
    """
    ArgumentParser that creates parsers tailored to functions and executes them.
    The arguments requested will be converted to the appropriate type.
    Arguments without default values will be considered required.
    """
    _loggers = (__name__, )
    func = None
    active_parser = None
    parent: 'Parser' = None
    message = None

    @staticmethod
    def _parse_type(type_name: str, source_module=sys.modules['__main__']) -> type:
        # Minimum effort solution for str(type) provided annotations that's private to a module.
        type_ = source_module
        for layer in type_name.split('.'):
            type_ = getattr(type_, layer)
        return type_

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.register('action', 'parsers', SubParser)
        self.tuples: List[str] = []
        self.set_defaults(active=self)
        self.subparsers: List[Parser] = []
        self.name: str = kwargs.get('prog').split()[-1] if 'prog' in kwargs else ''
        self.description: str = kwargs.get('description', '') or kwargs.get('help', '')
        self.arguments: List[Tuple[str, str]] = []

    def add_subparsers(self, *args, **kwargs) -> SubParser:
        # kwargs.setdefault('dest', 'option')
        subs: SubParser = super().add_subparsers(*args, **kwargs, parser_class=Parser)
        subs.parent = self
        subs.add_parser_from_function(self.help)
        return subs

    def error(self, message):
        if logger.isEnabledFor(logging.DEBUG):
            self.get_default('active').print_help()

        raise RuntimeError(f'Parsing error: {message}\n')

    def exit(self, status=0, message=None):
        if message:
            logger.critical(message)

    @staticmethod  # noqa: C901
    def _preparse_signature(func, sig: Signature) -> dict:
        arg_kwargs = {}
        for name, parameter in sig.parameters.items():
            # Preparation only, true parsing after we've added docstring data.
            arg_kwargs[name] = kwargs = {'dest': name}
            kwargs['type'] = type_ = parameter.annotation if parameter.annotation is not Parameter.empty else None

            if type_ is FromMessage or isinstance(parameter.default, FromMessage):
                arg_kwargs.pop(name)
                continue

            if parameter.kind is Parameter.POSITIONAL_ONLY:
                raise TypeError(f'Cannot parse argument {name}, positional only arguments are not supported.')
            elif parameter.kind is Parameter.VAR_KEYWORD:
                raise TypeError(f'Cannot parse argument {name}, **kwargs is not supported.')
            elif parameter.kind is Parameter.VAR_POSITIONAL:
                if getattr(type_, '__args__', None) is None and type_ not in (list, tuple):
                    kwargs['nargs'] = '?'
                    kwargs['type'] = type_ = str  # Can't annotate *args.
                    # kwargs['default'] = ()
                else:
                    raise TypeError(f'Cannot parse argument {name}, *args is not supported. '
                                    f'If you want to collect a variable length List[{type_ and type_.__name__}], use that.')

            if type(None) in getattr(type_, '__args__', ()):  # pylint: disable=unidiomatic-typecheck  # Bullshit, isinstance() can't do this without iterating.
                # Annotated type is Optional[type]
                kwargs['default'] = None
                types = set(type_.__args__)
                types.discard(type(None))
                optional = True
                if len(types) == 1:
                    kwargs['type'] = type_ = types.pop()  # Replace type with the real type
                else:
                    raise ValueError(f'Cannot parse multiple allowed types aside from None for {name}. Provided was {type_}.')
            else:
                optional = False

            if parameter.default is not Parameter.empty:
                kwargs['default'] = parameter.default
                if type_ is None:
                    kwargs['type'] = type_ = type(parameter.default)
                    logger.warning(f'Type for \'{name}\' not provided. Assuming {type_.__name__} based on default ({parameter.default!r})')

                if isinstance(type_, type) and not isinstance(parameter.default, type_) and not optional and 'group' not in kwargs:
                    # If we have a default/type mismatch, inform the user about their mistake, but continue.
                    if parameter.default is None:
                        logger.warning(f'None is not an instance of {func.__module__}.{func.__name__}({type_.__name__}). '
                                       f'Did you mean to annotate {name} with Optional[{type_.__name__}]?')
                    else:
                        logger.warning(f'Parameter {name} has default {parameter.default!r}, which is not an instance of {type_.__name__}.')

            kwargs['required'] = 'default' not in kwargs

        return arg_kwargs

    @classmethod  # noqa: C901
    def _parse_docstring(cls, docstring: str, arg_kwargs: dict, func_module: str) -> Tuple[str, str, dict, dict]:
        """Parses docstring and returns, in order, description, epilog, groups, and short names."""
        description = ''
        epilog = ''
        groups = {}
        hard_shorts = {}
        is_epilog = False
        # Grab docstring and extract argument lines.
        for line in docstring.split('\n'):
            line: str
            if not line.strip().startswith(':'):
                if is_epilog:
                    epilog += f'{line} '
                else:
                    description += f'{line} '

                continue

            _, primary, info = line.split(':', maxsplit=2)  # Line needs form :data_type name: info/value
            data_type, name = primary.split()
            is_epilog = True  # Descriptions after/between arguments is considered epilog
            info = info.strip()
            if name not in arg_kwargs and data_type != 'group':
                continue

            if data_type == 'param':
                if info:  # 0-length info isn't info and doesn't help.
                    arg_kwargs[name]['help'] = info
            elif data_type == 'short':  # Force assignment of a short letter, even if other options share the first letter.
                if len(info) > 1:
                    raise ValueError(f'Cannot assign single letter alias "{info}", because it is to long.')
                hard_shorts[name] = info
            elif data_type == 'type':  # Type annotations in docstrings like this is officially OK, so lets try to support it.
                if arg_kwargs[name].get('type', None) is not None:
                    logger.info('Type annotation found both as annotation and docstring. Docstring will be ignored.')
                    continue

                try:
                    # Not using TYPES.get() here because cls._parse_type is likely to raise errors.
                    arg_kwargs[name]['type'] = TYPES[info] if info in TYPES else cls._parse_type(info, sys.modules[func_module])
                except AttributeError:
                    logger.info(f'Cannot parse type {info} for {name} in module {func_module}')

            elif data_type == 'group':  # Invented our own syntax for mutually exclusive groups support
                if name not in arg_kwargs:  # New group definition.
                    if info == 'True':
                        groups[name] = True
                    elif info == 'False':
                        groups[name] = False
                    else:
                        logger.warning(f'Expected "True" or "False" as argument after a group declaration, got "{info}"')

                else:
                    arg_kwargs[name]['group'] = info

            else:
                logger.warning(f'Cannot parse Docstring annotation "{data_type}" in line "{line.strip()}"')

        return description, epilog, groups, hard_shorts

    def _type_details(self, name: str, kwargs: dict):  # noqa: C901
        type_ = kwargs['type']
        # Handle valid Iterator[type_]'s.
        if getattr(type_, '_name', None) == 'List':  # handle List[type].
            kwargs['nargs'] = '*'
            if type_.__args__[0].__name__ == 'T':
                logger.warning(f'No type selected for List contents for argument {name}. Assuming str.')
                type_ = kwargs['type'] = str
            else:
                type_ = kwargs['type'] = type_.__args__[0]

        elif getattr(type_, '_name', None) == 'Tuple':  # Handle Tuple[*Types].
            self.tuples.append(name)
            if type_.__args__:
                kwargs['nargs'] = len(type_.__args__)
                type_ = kwargs['type'] = TupleType(type_.__args__)
            else:
                kwargs['nargs'] = '*'
                logger.warning(f'No type selected for Tuple contents for argument {name}. Assuming str and allowing variable length.')
                type_ = kwargs['type'] = str

        # Type-dependent actions.
        if isinstance(type_, _GenericAlias):
            # This hits basically everything in the typing module. Everything it hits would otherwise cause an error during parsing.
            raise ValueError(f'Cannot handle type hint {type_!r}, because it is abstract.')
        elif not isinstance(type_, type):
            pass
            # Only types are checked below, and issubclass() errors out on non-types.
            # Valid non-types present here could be functions instantiating more complex objects from the CLI string argument.
            # Examples are argparse.FileType, from_manager(QueryableObject) or TupleType.
        elif type_ is bool:
            if 'nargs' in kwargs:
                raise ValueError(f'Cannot parse an iterable of booleans for {name}')

            del kwargs['type']  # type is not used for Booleans
            kwargs['required'] = False  # Booleans can't be required, since then there's no way to get the opposite value.
            kwargs['action'] = 'store_false' if kwargs.get('default', False) is True else 'store_true'
            if 'default' in kwargs:
                del kwargs['default']

        elif issubclass(type_, Choices):  # Enum argument
            kwargs['choices'] = type_
        elif type_ in (list, tuple):  # When a simple list/tuple is supplied.
            if 'nargs' in kwargs:
                raise ValueError(f'Cannot parse an iterable of iterables for {name}.')

            logger.info(f'No typing for list/tuple contents provided, assuming str. Use typing.List or typing.Tuple to provide these to {name}.')
            kwargs['nargs'] = '*'
            kwargs['type'] = str
            if type_ is tuple:
                self.tuples.append(name)
        elif type_ in TYPE_FACTORY_MAP:
            kwargs['type'] = TYPE_FACTORY_MAP[type_]

    @classmethod  # noqa: C901
    def from_function(
            cls,
            func: Callable[[Any], ReturnValue],
            parse: Union[Iterable, bool] = False,
            keyword_only: bool = False,
            parse_args: Tuple[Optional[Iterable], Any] = (),
            log: int = logging.INFO,
            **kwargs
    ) -> Union[Callable[[Any], ReturnValue], ReturnValue]:
        """
        Transforms a function in a complete argparse.ArgumentParser.
        This function also accepts all valid arguments for argparse.ArgumentParser, when passed as **kwargs. These will be forwarded and overwrite inferred
        values.
        :param func: The function to create an ArgumentParser for.
        :param parse: If False, return the parser. If truthy, call the function with parsed arguments, and return it's result.
        :param keyword_only: Don't generate positional parameters - you'll need to supply --name switches for all arguments.
        :param parse_args: If parse is true, holds the arguments for parser.parse_args(). Use None to skip one.
        :param log: Pass False to hide your mistakes when CLIParser would normally complain about them. Otherwise pass a log level like logging.DEBUG.
        Supported annotations include everything that has a __name__, and which can be called with a str argument to yield a single value.
        # Mutually exclusive groups:
        Groups can be created by a line in the docstring:
        `:group <groupname>: <required>`
        Where the groupname can be any string that is NOT currently use as argument name (keyword or otherwise).
        <required> should be the string 'True' or 'False' and signifies whether at least one group member needs to be supplied.
        Group members are defined as:
        `:group <name>: <groupname>`
        Where <name> is the variable name in the same was as in a :param... line. <groupname> is the name of the group.
        # Abbreviated command line options
        Abbreviated command line options can be supplied in a docstring in the following format:
        `:short <name>: <letter>`
        Where <name> is the parameter name and <letter> is the short letter to use.
        # On top of that, there is special handling for the following types
        ## list/tuple
        This allows any number of arguments to be taken to be provided. These are unified into a list/tuple and returned.
        ## typing.List[type]
        On top of the above for list/tuple, enacts type conversion on the lists' contents.
        ## typing.Tuple[*types]
        Demands exactly len(types) arguments. Type conversion in the tuple is enacted.
        ## utils.cliparser.Choices
        Subclasses of this StrEnum may be used for typing, unlike other Enums. Their string values are used to restrict input choices. Other Enums work for the
        default value and type conversion, but not for restricting input choices, and may therefore raise errors.
        ## bool
        Bool arguments are converted to argument-less switches so that not supplying the switch results in the default value, if any. If there is no default,
        then not supplying the switch results in False.
        ## utils.classes.base.queryableobject.QueryableObject
        User is requested to supply the name, and the object is retrieved from the database if it exists. Throws an error if it does not or the database is
        not available. This uses manager.get(), and as such is subject to several defaults you may or may not appreciate.
        ## Optional[type] or Union[None, type]
        These are literally both the exact same thing. Argument will never be required, None is assigned as default if there isn't any, and the value is
        converted to type.
        ## Utils
        Will not result in a command line option. If a Utils object has been passed, it will be the argument. Otherwise, a new one will be created and provided.
        """
        docstring = func.__doc__ or ''

        # Prepare dictionaries for all the arguments. Everything in here has higher priority than docstring values.
        sig = signature(func, follow_wrapped=True)
        arg_kwargs = cls._preparse_signature(func, sig)

        # Docstring parsing. Returns some info for the Parser kwargs, and some for the arg_kwargs.
        description, epilog, groups, hard_shorts = cls._parse_docstring(docstring, arg_kwargs, func.__module__)

        # Using setdefault leaves current versions intact. Since those are explicitly given, they should trump inferred values.
        kwargs.setdefault('description', description)
        kwargs.setdefault('epilog', epilog)
        if func.__name__ != 'main':  # In it were, it's better to let argparse use its default of sys.argv[1].
            kwargs.setdefault('prog', func.__name__)

        # Instantiate Parser
        self = cls(add_help=False, **kwargs)
        # We retrieve this in parse_args(). The extra step is necessary if we have subparsers, in order to get the right func attribute on the primary parser.
        self.set_defaults(func=func)
        # Setup groups
        for name, required in groups.items():
            groups[name] = self.add_mutually_exclusive_group(required=required)

        # Determine unambiguous short forms we can use. (for example, -h is the short form of --help)
        shorts = set()
        forbidden = set(RESERVED_SHORT.values()) | set(hard_shorts.values())
        forbidden.add('h')  # Otherwise conflicts with the -h, --help option.
        for name, kwargs in arg_kwargs.items():
            if not keyword_only and 'default' not in kwargs and kwargs['type'] != bool:  # All bools are optional, therefore not positional.
                continue  # Positional arguments don't conflict.
            elif name in hard_shorts:
                continue  # We already have a short name hard assigned, no need to conflict with others.
            elif name[0] in shorts:  # Conflict detected
                forbidden.add(name[0])
                shorts.remove(name[0])
            elif name[0] not in forbidden:  # Doesn't seem to conflict.
                shorts.add(name[0])

        # Parse and add parameters
        for name, parameter in sig.parameters.items():
            if name not in arg_kwargs:
                continue  # We skipped this option by removing it from arg_kwargs
            kwargs = arg_kwargs[name]

            # Warn if we haven't found type from annotation/docstring/default
            if kwargs['type'] is None:
                logger.warning(f'Argument {name} has no type specified. Assuming str for now, but annotate the function please.')
                kwargs['type'] = str

            # Warn if we don't have a help text set. It should be in the docstring.
            if 'help' not in kwargs:
                if parameter.kind != parameter.VAR_POSITIONAL:
                    logger.warning(f'No help info set for argument {name} of function {func.__module__}.{func.__name__}')
                kwargs['help'] = 'Help information missing; type: {type}'.format_map(kwargs)
                if parameter.default is not Parameter.empty:
                    kwargs['help'] += f' default: {parameter.default}'

            self._type_details(name, kwargs)  # pylint: disable=protected-access  # Nope, it's really an instance of our own cls. Also, *evil laughter*
            # Remove leading/trailing underscores. Their only use should be disambiguation with builtins/keywords. Then replace _ of python with - of CLI args.
            arg_name = name.strip('_').replace('_', '-')
            required = (kwargs.get if keyword_only else kwargs.pop)('required')

            if required and not keyword_only:
                # Order of positional arguments is accurate because Signature.parameters is a sorted mapping.
                names = (name,)  # Don't add prefixes for required arguments, and keep underscores.
                if 'dest' in kwargs:
                    del kwargs['dest']  # The name is used as dest for positional required arguments.

            elif name in hard_shorts:  # Short names as assigned in docstrings.
                names = (f'-{hard_shorts[name]}', f'--{arg_name}')
            elif arg_name in RESERVED_SHORT:  # Reserved short names.
                names = (f'-{RESERVED_SHORT[arg_name]}', f'--{arg_name}')
            elif name[0] in shorts:  # Non-conflicting short names
                names = (f'-{name[0]}', f'--{arg_name}')
            else:  # Short names would conflict, so let's not use them.
                names = (f'--{arg_name}',)

            self.arguments.append((','.join(names), kwargs['help']))
            # logger.debug(f'Adding parameter {name} of function {func.__module__}.{func.__qualname__} with *{names} and **{kwargs}')
            groups.get(kwargs.pop('group', None), self).add_argument(*names, **kwargs)

        if parse:
            return func(**vars(self.parse_args(*parse_args)))

        return self

    @verify_open_channel
    async def help(self, *args, guild_id: Snowflake = FromMessage.Guild):
        """
        Lists the available commands with basic information, or provides details about a specific command as listed below.
        :param args: What to show help for. Defaults to this same parser.
        :param guild_id: ID of the current server.
        """
        subject = (args[0] if args else None) if isinstance(args, tuple) else args
        for parser in self.subparsers:
            if parser.prog.split()[-1] == subject:
                subject = parser
                break
        else:
            subject = self

        embed = Embed(
            title=subject.name.strip('_'),
            description=subject.description,
            fields_=[]
        )
        for sub in subject.subparsers:
            embed.fields_.append(EmbedField(
                name=f'Command: {sub.name.strip("_")}',
                value=f'`{get_server_prefix(int(guild_id))}{subject.prog} {sub.name}`\n{sub.description}'
            ))

        for name, desc in subject.arguments:
            embed.fields_.append(EmbedField(name=f'Argument: {name.strip("_")}', value=desc))
            
        return {'embed': embed}

    @staticmethod
    def _parse_positional_args(func, args: Namespace):
        positionals = []
        for name, parameter in signature(func, follow_wrapped=True).parameters.items():
            positionals.append(name)
            if parameter.kind == parameter.VAR_POSITIONAL:
                pos = []
                for param in positionals:
                    if getattr(args, param, None) is not None:
                        pos.append(vars(args).pop(param))
                    elif param == name:
                        vars(args).pop(name, None)

                args.POSITIONALS = tuple(pos)
                return
        # If there is no VAR_POSITIONAL (*args group), then we don't process any positionals, since keywords can handle everything.
        args.POSITIONALS = ()

    def parse_args(self, args=None, namespace=None, message: Optional['Message'] = None, skip_checks=False) -> Namespace:
        if args is None and message is not None:
            args = shlex.split(message.content)[1:]  # Ignore our prefix.
            self.message = message

        args = super().parse_args(args, namespace)

        for var in self.tuples:  # Transform into tuples where tuples are requested.
            setattr(args, var, tuple(getattr(args, var)))

        self.active_parser = vars(args).pop('active')
        if 'func' in vars(args):  # Retrieve func if we're constructed that way. (we're typically not if we have subparsers.)
            self.func = vars(args).pop('func')
            func = self.func
        else:
            func = self.help
        if message is not None:
            self.add_args_from_message(args, message, signature(func, follow_wrapped=True))

        if not skip_checks and hasattr(self.func, 'checks'):
            for check in self.func.checks:
                if not check(message=message, args=args):
                    raise MessageVerificationFailed(check.__name__)

        self._parse_positional_args(func, args)

        return args

    @staticmethod
    def add_args_from_message(target, message: 'Message', sig: Signature):
        for param in sig.parameters.values():
            if isinstance(param.default, FromMessage):  # Processing values of the FromMessage enum
                setattr(target, param.name, param.default.get(message))
            elif param.annotation is Message and param.default == Parameter.empty:  # Entire message object, if requested.
                setattr(target, param.name, message)

    @contextmanager
    def with_prog(self, prog: str):
        old = self.prog
        self.prog = prog
        yield self
        self.prog = old

    @property
    def can_run(self):
        return self.func is not None

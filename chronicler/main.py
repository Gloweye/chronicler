
import asyncio
import sys
import logging

from sqlalchemy.orm.exc import NoResultFound

from chronicler.config import config
from chronicler.models.author import ChannelRenderer

sys.path.append('.')

from chronicler import renderers
from chronicler.parser import MessageVerificationFailed
from chronicler.discord.client import Discord
from chronicler.discord.message import Message
from chronicler.discord.api import api
from chronicler.endpoints.parser import create_parser, Parser
from chronicler.models.base import finalize, session
from chronicler.tools import get_server_prefix

logger = logging.getLogger(__name__)
client = Discord()
parser: Parser
finalize()


async def echo(message: Message):
    logger.debug(f'Sending Message in response to {message.author.name}')
    await api.send_message(message.content)


async def default_renderer(message: Message, result: dict):
    """
    Default renderer, which sends the message to the discord server unmodified.
    result['embed']: chronicler.discord.embed.Embed is supplied -> Creates embed message on the server.
    result['content']: str -> plain text message.
    :param message: Message object being replied to.
    :param result: Dict containing the return data. There is at least one truthy value in this dict.
    :return: requests.Response object returned by the API.
    """
    return await message.reply(**result)


@client('MESSAGE_CREATE')
async def handle_message(message: Message):
    logger.debug(f'Got a message from {message.author} !')

    if client.bot_id != message.author.id:  # Checks if it's us.

        if message.content.lower().startswith(get_server_prefix(message.guild_id)):
            try:
                args = parser.parse_args(message=message)
                pos_args = vars(args).pop('POSITIONALS')
                # if pos_args:
                #     pos_args = (pos_args, )
                if parser.can_run:
                    logger.debug(f'Parsed args {vars(args)}, to be fed to function {parser.func}')
                    result = await parser.func(*pos_args, **vars(args))
                    parser.func = None  # This should probably be somewhere in the parser or something, but I can't be arsed right now.
                else:
                    logger.debug(f'Parsed args {vars(args)}, calling help function...')
                    result = await parser.active_parser.help(*pos_args, **{k: v for k, v in vars(args).items() if k in ('subject', 'guild_id')})

            except MessageVerificationFailed as e:
                result = {'content': f'Check failed: {e}'}
            except NoResultFound:
                if logger.isEnabledFor(logging.DEBUG):
                    result = {'content': f'At least one provided instance was misspelled.'}
                else:
                    return
            except RuntimeError as e:  # Probably a parsing error
                logger.error(e, exc_info=logger.isEnabledFor(logging.DEBUG))
                return

            if result and any(val for val in result.values()):
                renderer = session.query(ChannelRenderer).filter(ChannelRenderer.channel_id == message.channel_id).one_or_none()
                if renderer is None:
                    await default_renderer(message, result)
                else:
                    await renderers.get(renderer.renderer)(message, result)


def exception_handler(loop, context):
    exc = context['exception']
    logger.critical(exc, exc_info=exc if logger.isEnabledFor(logging.DEBUG) else False)
    if logger.isEnabledFor(logging.DEBUG):
        loop.stop()


async def main():
    logging.basicConfig(level=config['main']['logging'])
    logging.getLogger('websockets').setLevel(logging.INFO)
    for handler in logging.getLogger().handlers:
        handler.setFormatter(logging.Formatter('{levelname: <8}:{asctime}:{name: <30}:{lineno: <4}:{message}', style='{'))
    asyncio.get_running_loop().set_exception_handler(exception_handler)
    global parser
    parser = create_parser()

    await client.run()
    return 0


if __name__ == '__main__':
    sys.exit(asyncio.run(main()))

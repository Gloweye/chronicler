from typing import Generator, Union

import mistune
from mistune.renderers import BaseRenderer

from chronicler import renderers
from chronicler.config import config
from chronicler.discord.channel import Channel
from chronicler.discord.message import Message
from chronicler.discord.snowflake import Snowflake
from chronicler.discord.cache import cache

from chronicler.models.base import session
from chronicler.models.author import ServerGlobal
from chronicler.models.attribute import Attribute
from chronicler.models.character import NPC
from chronicler.models.info import Infobox
from chronicler.models.perk import Perk
from chronicler.models.class_ import Class
from chronicler.models.skill import Skill
from chronicler.models.ability import Ability, Classification, Shout, Word


async def get_channel(channel_id: Snowflake) -> Channel:
    if cache.exists(Channel, channel_id):
        return cache[Channel, channel_id]
    else:
        return await Channel.get_by_id(channel_id)


def get_server_prefix(guild_id: int) -> str:
    var = session.query(ServerGlobal).filter(ServerGlobal.guild_id == guild_id, ServerGlobal.setting == 'prefix').one_or_none()
    if var is None:
        return config['main']['server_prefix']

    return var.value


def attr_by_name_or_id(name: str) -> Attribute:
    if name.isdecimal():
        return session.query(Attribute).filter(Attribute.entity_id == int(name)).one()
    return session.query(Attribute).filter(Attribute.name == name).one()


def perk_by_name_or_id(name: str) -> Attribute:
    if name.isdecimal():
        return session.query(Perk).filter(Perk.entity_id == int(name)).one()
    return session.query(Perk).filter(Perk.name == name).one()


def class_by_name_or_id(name: str) -> Class:
    if name.isdecimal():
        return session.query(Class).filter(Class.entity_id == int(name)).one()
    return Class.get_by_name(name)


def skill_by_name_or_id(name: str) -> Skill:
    if name.isdecimal():
        return session.query(Skill).filter(Skill.entity_id == int(name)).one()
    return session.query(Skill).filter(Skill.name == name).one()


def ability_by_name_or_id(name: str) -> Ability:
    if name.isdecimal():
        return session.query(Ability).filter(Ability.entity_id == int(name)).one()
    return session.query(Ability).filter(Ability.name == name).one()


def classification_by_name_or_id(name: str) -> Classification:
    if name.isdecimal():
        return session.query(Classification).filter(Classification.entity_id == int(name)).one()
    return session.query(Classification).filter(Classification.name == name).one()


def shout_by_name_or_id(name: str) -> Shout:
    if name.isdecimal():
        return session.query(Shout).filter(Shout.entity_id == int(name)).one()
    return session.query(Shout).filter(Shout.name == name).one()


def npc_by_name_or_id(name: str) -> NPC:
    if name.isdecimal():
        return session.query(NPC).filter(NPC.entity_id == int(name)).one()
    return session.query(NPC).filter(NPC.name == name).one()


def infobox_by_name_or_id(name: str) -> Infobox:
    if name.isdecimal():
        return session.query(Infobox).filter(Infobox.entity_id == int(name)).one()
    return session.query(Infobox).filter(Infobox.topic == name).one()


class BBCodeRenderer(BaseRenderer):
    """
    Subclassed and based on https://github.com/lepture/mistune/blob/master/mistune/renderers.py
    Pull request if we get it running well?
    Regardless, sourcing bbcode here: https://forums.spacebattles.com/help/bb-codes/
    """
    NAME = 'bbcode'
    IS_TREE = False

    def link(self, link, text=None, title=None):
        return f'[URL={link}]{text or link}[/URL]'

    def image(self, src, alt="", title=None):
        return f'[IMG]{src}[/IMG]'

    def emphasis(self, text):
        return f'[I]{text}[/I]'

    def strong(self, text):
        return f'[B]{text}[/B]'

    def codespan(self, text):
        return f'[ICODE]{text}[/ICODE]'

    def linebreak(self):
        return '\n'

    def inline_html(self, html):
        return html

    def paragraph(self, text):
        return f'{text}'

    def heading(self, text, level):
        return f'{text}[hr{["", "=1", "=2", "=3", "=4", "=5"][level-1]}]'

    def newline(self):
        return ''

    def thematic_break(self):
        return '\n'

    def block_text(self, text):
        return text

    def block_code(self, code, info=None):
        return f'[CODE={info.split(None, 1)[0]}]{code}[/CODE]'

    def block_quote(self, text):
        return f'[QUOTE]{text}[/QUOTE]'

    def block_html(self, html):
        return f'[QUOTE]{html}[/QUOTE]'

    def list(self, text, ordered, level, start=None):
        if ordered:
            return f'[LIST={start or 1}]{text}[/LIST]'
        return f'[LIST]{text}[/LIST]'

    def list_item(self, text, level):
        return f'[*]{text}'

    def text(self, text):
        return text


bbcode = mistune.create_markdown(
    escape=False,
    renderer=BBCodeRenderer(),
)


def chunks(entity: Union[str, list], chunk_size: int = 1900) -> Generator[Union[str, list], None, None]:
    location = 0
    while location < len(entity):
        yield entity[location:location + chunk_size]
        location += chunk_size


async def bbcode_renderer(message: Message, result: dict):
    """
    Renderer for Ctrl+C/Ctrl+V use on the spacebattles forums.
    """
    if result.get('content', None):
        reply = bbcode(result.get('content'))
    else:
        embed = result.pop('embed')
        fields = '\n'.join(f'[fieldset={bbcode(field.name)}]{bbcode(field.value)}[/fieldset]' for field in embed.fields_)
        reply = f"""\n[spoiler={embed.title.strip("*")}][fieldset={bbcode(embed.title)}]{bbcode(embed.description)}\n{fields}[/fieldset][/spoiler]\n"""

    for chunk in chunks(reply):
        await message.reply(**{'content': f'```{chunk}```'})


async def ffnet_renderer(message: Message, result: dict):
    """
    Renderer for copy/pasting towards https://www.fanfiction.net.
    """
    if result.get('content', None):
        result['content'] = mistune.html(result.get('content'))
    else:
        embed = result.pop('embed')
        fields = '\n'.join(f'{mistune.html(field.name)}\n{mistune.html(field.value)}' for field in embed.fields_)
        text = '\n'.join((mistune.html(embed.title), mistune.html(embed.description), fields))

        result['content'] = f"```\n{text}\n```"
    return await message.reply(**result)


# Yes, this is very bad code practice. No, I've run out of fucks to give.
renderers['bbcode'] = bbcode_renderer
renderers['ffnet'] = ffnet_renderer

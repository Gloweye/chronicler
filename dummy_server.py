
import asyncio
import random


async def random_message(writer):
    await asyncio.sleep(random.randint(5, 10))
    print('Done Sleeping, now sending some message.')
    writer.write(b'Some Message')


async def handle_echo(reader, writer):
    message = ''
    while message != 'quit':
        try:
            data = await reader.read(100)
        except ConnectionResetError:
            print(f'Someone rudely aborted our connection.')
            break
        if not data:
            continue

        message = data.decode()
        addr = writer.get_extra_info('peername')

        print(f"Received {message!r} from {addr!r}")
        if message == 'ping':
            print("Sending pong")
            writer.write(b'pong')
            asyncio.create_task(random_message(writer))
        else:
            print(f"Send: {message!r}")
            writer.write(data)
        await writer.drain()

    print("Close the connection")
    writer.close()


async def main():
    server = await asyncio.start_server(
        handle_echo, '127.0.0.1', 8888)

    addr = server.sockets[0].getsockname()
    print(f'Serving on {addr}')

    async with server:
        await server.serve_forever()

asyncio.run(main())
